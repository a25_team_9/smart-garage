USE `mechanicum`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table mechanicum.currencies: ~4 rows (approximately)
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` (`currency_id`, `name`) VALUES
	(1, 'BGN'),
	(2, 'USD'),
	(3, 'EUR'),
	(4, 'GBP');
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;

-- Dumping data for table mechanicum.manufacturers: ~18 rows (approximately)
/*!40000 ALTER TABLE `manufacturers` DISABLE KEYS */;
INSERT INTO `manufacturers` (`manufacturer_id`, `name`) VALUES
	(1, 'Alfa Romeo'),
	(2, 'Audi'),
	(3, 'BMW'),
	(4, 'Dacia'),
	(5, 'Fiat'),
	(6, 'Ford'),
	(7, 'Honda'),
	(8, 'Hyundai'),
	(9, 'Jeep'),
	(10, 'Mazda'),
	(11, 'Mercedes-Benz'),
	(12, 'Mitsubishi'),
	(13, 'Nissan'),
	(14, 'Opel'),
	(15, 'Skoda'),
	(18, 'Trabant4e'),
	(16, 'Volkswagen'),
	(17, 'Volvo');
/*!40000 ALTER TABLE `manufacturers` ENABLE KEYS */;

-- Dumping data for table mechanicum.roles: ~3 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `name`) VALUES
	(3, 'Administrator'),
	(2, 'Employee'),
	(1, 'User');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table mechanicum.services: ~48 rows (approximately)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`service_id`, `start_date`, `end_date`, `quantity`, `status_id`, `type_id`, `visit_id`) VALUES
	(1, '2021-03-30', '2021-04-07', 1, 3, 2, 1),
	(2, '2021-04-05', '2021-04-06', 1, 3, 7, 2),
	(3, '2021-04-04', '2021-04-07', 4, 3, 9, 3),
	(4, '2021-04-01', '2021-04-05', 3, 3, 11, 4),
	(6, '2021-04-02', '2021-04-05', 2, 3, 10, 5),
	(7, '2021-04-03', '2021-04-07', 1, 3, 18, 6),
	(8, '2021-04-03', '2021-04-08', 1, 3, 4, 7),
	(9, '2021-04-08', '2021-04-11', 2, 2, 5, 12),
	(10, '2021-04-08', '2021-04-13', 1, 2, 4, 12),
	(11, '2021-04-12', '2021-04-15', 1, 1, 3, 13),
	(12, '2021-04-20', '2021-04-23', 4, 1, 6, 13),
	(14, '2021-04-20', '2021-04-22', 1, 1, 1, 8),
	(15, '2021-04-30', '2021-05-05', 2, 1, 13, 9),
	(16, '2021-04-19', '2021-04-22', 2, 1, 17, 10),
	(17, '2021-04-29', '2021-04-30', 1, 1, 2, 11),
	(18, '2021-04-18', '2021-04-20', 2, 1, 15, 14),
	(19, '2021-04-14', '2021-04-17', 2, 1, 4, 15),
	(20, '2021-04-14', '2021-04-17', 2, 1, 5, 15),
	(21, '2021-04-14', '2021-04-17', 1, 1, 17, 15),
	(22, '2021-04-14', '2021-04-17', 1, 1, 17, 15),
	(23, '2021-04-14', '2021-04-21', 1, 1, 18, 16),
	(24, '2021-04-09', '2021-04-20', 1, 1, 10, 17),
	(25, '2021-04-08', '2021-04-20', 1, 2, 9, 17),
	(26, '2021-04-08', '2021-04-20', 1, 2, 6, 17),
	(27, '2021-04-08', '2021-04-15', 1, 2, 5, 18),
	(28, '2021-04-08', '2021-04-15', 2, 2, 3, 18),
	(29, '2021-04-11', '2021-04-15', 2, 1, 22, 19),
	(30, '2021-04-11', '2021-04-15', 6, 1, 21, 19),
	(31, '2021-04-12', '2021-04-17', 4, 1, 9, 20),
	(32, '2021-04-12', '2021-04-17', 2, 1, 18, 20),
	(33, '2021-04-15', '2021-05-01', 2, 1, 18, 21),
	(34, '2021-04-15', '2021-05-01', 1, 1, 15, 21),
	(35, '2021-04-15', '2021-05-01', 1, 1, 20, 21),
	(36, '2021-04-15', '2021-05-06', 1, 1, 1, 22),
	(37, '2021-04-15', '2021-05-07', 1, 1, 3, 22),
	(38, '2021-04-15', '2021-05-02', 1, 1, 7, 23),
	(39, '2021-04-15', '2021-05-02', 4, 1, 19, 23),
	(40, '2021-04-15', '2021-05-02', 4, 1, 11, 23),
	(41, '2021-04-14', '2021-05-15', 4, 1, 11, 24),
	(42, '2021-04-14', '2021-05-15', 1, 1, 8, 16),
	(43, '2021-04-14', '2021-05-15', 1, 1, 15, 16),
	(48, '2021-05-09', '2021-05-18', 1, 1, 1, 25),
	(49, '2021-05-09', '2021-05-18', 1, 1, 10, 25),
	(50, '2021-05-09', '2021-05-18', 3, 1, 18, 25),
	(51, '2021-05-09', '2021-05-18', 4, 1, 9, 25),
	(52, '2021-04-18', '2021-04-20', 1, 2, 5, 26),
	(53, '2021-04-18', '2021-04-18', 1, 2, 11, 26),
	(54, '2021-04-18', '2021-04-19', 1, 2, 20, 26);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

-- Dumping data for table mechanicum.service_statuses: ~3 rows (approximately)
/*!40000 ALTER TABLE `service_statuses` DISABLE KEYS */;
INSERT INTO `service_statuses` (`status_id`, `name`) VALUES
	(2, 'In progress'),
	(1, 'Not started'),
	(3, 'Ready for pickup');
/*!40000 ALTER TABLE `service_statuses` ENABLE KEYS */;

-- Dumping data for table mechanicum.service_types: ~23 rows (approximately)
/*!40000 ALTER TABLE `service_types` DISABLE KEYS */;
INSERT INTO `service_types` (`type_id`, `name`, `single_price`) VALUES
	(1, 'General checkup', 350),
	(2, 'Change oil car', 240),
	(3, 'Change oil SUV', 460),
	(4, 'Change air filter', 40),
	(5, 'Change fuel filter', 40),
	(6, 'Change lights & lenses', 80),
	(7, 'Replace brake fluid', 80),
	(8, 'Change cooling system', 50),
	(9, 'Change brakes', 140),
	(10, 'Engine fix', 800),
	(11, 'Change wheel', 45),
	(12, 'Check handbrake cable', 70),
	(13, 'Check fuel system', 80),
	(14, 'Check braking system', 70),
	(15, 'Check windows for damage', 40),
	(16, 'Check wheel bearing', 120),
	(17, 'Check antifreeze', 35),
	(18, 'Paint whole car', 2500),
	(19, 'Change interior part', 450),
	(20, 'Road test', 150),
	(21, 'Change gear oil', 280),
	(22, 'Change light bulb', 40),
	(23, 'Paint one detail', 180);
/*!40000 ALTER TABLE `service_types` ENABLE KEYS */;

-- Dumping data for table mechanicum.users_authorization: ~16 rows (approximately)
/*!40000 ALTER TABLE `users_authorization` DISABLE KEYS */;
INSERT INTO `users_authorization` (`email`, `password`, `reset_token`) VALUES
	('aplivougen@autocare.bg', 'password1', NULL),
	('cky3k@abv.bg', 'password1', NULL),
	('dewsag@owlymail.com', 'password1', NULL),
	('fuanot@owlymail.com', 'password1', NULL),
	('genbelight@autocare.bg', 'password1', NULL),
	('ggbg@gmail.com', 'password1', NULL),
	('hipiandsta@autocare.bg', 'password1', NULL),
	('iperchemliev@abv.bg', 'password1', NULL),
	('ishobsenel@autocare.bg', 'password1', NULL),
	('laibab@owlymail.com', 'password1', NULL),
	('proshlqk@sev.bg', 'bLC6FBzx353dZ$5', NULL),
	('puwpie@owlymail.com', 'password1', NULL),
	('ragsie@owlymail.com', 'password1', NULL),
	('sisdac@owlymail.com', 'password1', NULL),
	('toneyerney@autocare.bg', 'password1', NULL),
	('ushumouste@autocare.bg', 'password1', NULL);
/*!40000 ALTER TABLE `users_authorization` ENABLE KEYS */;

-- Dumping data for table mechanicum.users_roles: ~18 rows (approximately)
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` (`email`, `role_id`) VALUES
	('ggbg@gmail.com', 2),
	('ggbg@gmail.com', 3),
	('cky3k@abv.bg', 2),
	('iperchemliev@abv.bg', 2),
	('puwpie@owlymail.com', 1),
	('laibab@owlymail.com', 1),
	('ragsie@owlymail.com', 1),
	('dewsag@owlymail.com', 1),
	('sisdac@owlymail.com', 1),
	('fuanot@owlymail.com', 1),
	('ushumouste@autocare.bg', 1),
	('toneyerney@autocare.bg', 1),
	('aplivougen@autocare.bg', 1),
	('genbelight@autocare.bg', 1),
	('hipiandsta@autocare.bg', 1),
	('ishobsenel@autocare.bg', 1),
	('ggbg@gmail.com', 1),
	('proshlqk@sev.bg', 1);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;

-- Dumping data for table mechanicum.user_info: ~16 rows (approximately)
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` (`user_id`, `email`, `phone`, `first_name`, `last_name`, `address`) VALUES
	(1, 'ggbg@gmail.com', '0888567890', 'Georgi', 'Georgiev', 'Gondor'),
	(2, 'cky3k@abv.bg', '0878567890', 'Ivan', 'Ivanov', 'Mordor'),
	(3, 'iperchemliev@abv.bg', '0887635234', 'Ivan', 'Perchemliev', 'Praga'),
	(4, 'puwpie@owlymail.com', '0887635235', 'William', 'Hampton ', 'Wendover'),
	(5, 'laibab@owlymail.com', '0878567894', 'Daniel', 'Brandt', 'Morpeth'),
	(6, 'ragsie@owlymail.com', '0878567895', 'Jacob', 'Mcloughlin', 'Richmond'),
	(7, 'dewsag@owlymail.com', '0878567896', 'Joseph', 'Boyer', 'Crookston'),
	(8, 'sisdac@owlymail.com', '0878567897', 'Ethan', 'Snyder', 'Dudley'),
	(9, 'fuanot@owlymail.com', '0878567891', 'Edwin ', 'Howard', 'Bronx '),
	(10, 'ushumouste@autocare.bg', '0878267897', 'Ioan', 'Carter', 'Dubuque '),
	(11, 'toneyerney@autocare.bg', '0878562332', 'Layton', 'Woods', 'Looe '),
	(12, 'aplivougen@autocare.bg', '0878567895', 'Xander', 'Jones', 'Martins Ferry'),
	(13, 'genbelight@autocare.bg', '0878567232', 'Mark', 'Mason', 'Uckfield'),
	(14, 'hipiandsta@autocare.bg', '0878568545', 'Oliver ', 'O\'Quinn', 'Silverton '),
	(15, 'ishobsenel@autocare.bg', '087859148', 'Tony', 'Patterson', 'Boulder '),
	(18, 'proshlqk@sev.bg', '0123456798', 'Nomad', 'Selski', NULL);
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;

-- Dumping data for table mechanicum.vehicles: ~20 rows (approximately)
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` (`vehicle_id`, `model_id`, `year`, `vin`, `licence_plate`, `owner`, `vehicle_url`) VALUES
	(1, 4, 2017, '12345678901234567', 'C6666PB', 2, 'https://s1.cdn.autoevolution.com/images/news/2016-alfa-romeo-giulia-tipo-952-quadrifoglio-verde-is-the-rebirth-of-alfa-romeo-photo-gallery-97073_1.jpg'),
	(2, 2, 2011, '1ZVHT80N385187895', 'A6462CD', 5, 'https://live.staticflickr.com/1902/45396560182_523e165935_b.jpg'),
	(3, 53, 2009, '1N4AL21E59N587416', 'PB1132KZ', 7, 'https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/12q2/450434/2012-mercedes-benz-c350-4matic-coupe-instrumented-test-review-car-and-driver-photo-452049-s-original.jpg'),
	(4, 42, 2011, '3FA6P0LU9ER385897', 'CB4326KH', 4, 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2021-honda-hr-v-mmp-1-1595869461.jpg?crop=0.889xw:1.00xh;0.0561xw,0&resize=640:*'),
	(5, 9, 2005, '2CNBJ18U4R6985163', 'CA3754PT', 6, 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2021-audi-a4-45-tfsi-quattro-106-1607927016.jpg?crop=0.587xw:0.661xh;0.179xw,0.276xh&resize=640:*'),
	(6, 11, 2008, '1GCEG15X861178691', 'CB6037KC', 7, 'https://www.slashgear.com/wp-content/uploads/2020/04/A202548_large-1-1280x720.jpg'),
	(7, 13, 2014, '5GZCZ63464S826932', 'EB4572BH', 8, 'https://images.hgmsites.net/hug/2020-audi-a8_100708460_h.jpg'),
	(8, 21, 2005, '2XP5DB0X85M865682', 'EH7285AH', 9, 'https://img.drivemag.net/media/default/0001/43/2017-BMW-M550i-xDrive-65-1824-default-large.jpeg'),
	(9, 27, 2013, '1FTNX21S1XEB46692', 'CB4181KC', 10, 'https://www.auto-data.net/images/f85/BMW-M5-F90-LCI-facelift-2020.jpg'),
	(10, 79, 2008, '3N1CB51D05L565271', 'PB2892AT', 11, 'https://cdn.motor1.com/images/mgl/BwL46/s3/vw-passat-facelift-2019.jpg'),
	(11, 39, 2015, '1GDHC24U86E209643', 'CH5583AP', 12, 'https://www.motortrend.com/uploads/sites/5/2018/03/2019-Ford-Fusion-front-three-quarter-in-motion-01-1.jpg'),
	(12, 50, 2013, '6FTZX1769WKB63004', 'CB5394AH', 13, 'https://media.ed.edmunds-media.com/mazda/cx-5/2021/oem/2021_mazda_cx-5_4dr-suv_signature_fq_oem_4_815.jpg'),
	(13, 55, 2006, '5TDZK22C58S235104', 'X5284HH', 5, 'https://media.autoexpress.co.uk/image/private/s--MBp-XaC4--/v1590491167/autoexpress/2020/05/Mercedes-AMG%20E%2053%20Coupe%202020-2.jpg'),
	(14, 64, 2008, 'KNAFB161735103589', 'CB4582KC', 8, 'https://cdn.motor1.com/images/mgl/PEEP8/s1/nissan-qashqai-2021.jpg'),
	(15, 72, 2013, 'WDDLJ9BB9DA034828', 'B8542KE', 2, 'https://cdn.skoda-storyboard.com/2017/05/RaS_00002-1440x960.jpg'),
	(16, 83, 2008, 'JTDBT923471140603', 'BT7463TK', 3, 'https://res.cloudinary.com/autofile-communications-inc/image/upload/s--hCmxmd-v--/q_jpegmini,w_1366/v1567570718/article/article18959/k8tlk0pcpbfmheadh5v8.jpg'),
	(17, 45, 2006, '2XP5DB0X85M856372', 'CB3428KK', 14, 'https://assets-eu-01.kc-usercontent.com/bb5aba31-d98c-0160-8548-418b3723c58e/0d5a7341-70cf-4464-af0b-c540e6decb10/Hyundai%20Ioniq%20(22).jpg'),
	(18, 67, 2021, '1GD2D24U86E209543', 'PB6666CA', 1, 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPTAMrqVpXPZ8Dl8xMTTfXgUtSODdiCmczdUIZ6K0Z33HIHXn7-1tyXyLyu7a_A9Gv-Ec&usqp=CAU'),
	(19, 61, 2001, '2XP5CB0X85M953372', 'PK8954CO', 15, 'https://media.caradvice.com.au/image/private/c_fill,q_auto,f_auto,w_1200,h_900/70e85e38984279b5ab42f1a1cc5a5211.jpg'),
	(21, 84, 2020, 'OMGWTFDAFUQ123456', 'PK6666PK', 18, 'https://i.ytimg.com/vi/j3w0u-Ka7eY/maxresdefault.jpg');
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;

-- Dumping data for table mechanicum.vehicle_models: ~84 rows (approximately)
/*!40000 ALTER TABLE `vehicle_models` DISABLE KEYS */;
INSERT INTO `vehicle_models` (`model_id`, `model`, `manufacturer_id`) VALUES
	(1, 'Alfa 156', 1),
	(2, 'Alfa 159', 1),
	(3, 'Crosswagon', 1),
	(4, 'Giulia', 1),
	(5, 'Giulietta', 1),
	(6, 'GT', 1),
	(7, 'Stelvio', 1),
	(8, 'A3', 2),
	(9, 'A4', 2),
	(10, 'A5', 2),
	(11, 'A6', 2),
	(12, 'A7', 2),
	(13, 'A8', 2),
	(14, 'Q5', 2),
	(15, 'Q7', 2),
	(16, 'RS4', 2),
	(17, 'RS6', 2),
	(18, '320', 3),
	(19, '330', 3),
	(20, '530', 3),
	(21, '550', 3),
	(22, '640', 3),
	(23, '650', 3),
	(24, '745', 3),
	(25, '750', 3),
	(26, 'M3', 3),
	(27, 'M5', 3),
	(28, 'X5', 3),
	(29, 'X7', 3),
	(30, 'Logan', 4),
	(31, 'Duster', 4),
	(32, 'Dokker', 4),
	(33, '500', 5),
	(34, 'Brava', 5),
	(35, 'Chroma', 5),
	(36, 'Panda', 5),
	(37, 'Focus', 6),
	(38, 'Fiesta', 6),
	(39, 'Fusion', 6),
	(40, 'Civic', 7),
	(41, 'CR-V', 7),
	(42, 'HR-V', 7),
	(43, 'Prelude', 7),
	(44, 'Santa Fe', 8),
	(45, 'IONIQ', 8),
	(46, 'Kona', 8),
	(47, 'Grand Cherokee', 9),
	(48, '323', 10),
	(49, '625', 10),
	(50, 'CX-5', 10),
	(51, 'CX-7', 10),
	(52, 'C230', 11),
	(53, 'C350', 11),
	(54, 'E230', 11),
	(55, 'E350', 11),
	(56, 'ML500', 11),
	(57, 'GL500', 11),
	(58, 'G500', 11),
	(59, 'Lancer', 12),
	(60, 'Outlander', 12),
	(61, 'Pajero', 12),
	(62, 'Micra', 13),
	(63, 'Navara', 13),
	(64, 'Qashqai', 13),
	(65, 'Primiera', 13),
	(66, 'Astra', 14),
	(67, 'Insignia', 14),
	(68, 'Meriva', 14),
	(69, 'Moka', 14),
	(70, 'Fabia', 15),
	(71, 'Octavia', 15),
	(72, 'Rapid', 15),
	(73, 'Superb', 15),
	(74, 'Yeti', 15),
	(75, 'Bora', 16),
	(76, 'Caddy', 16),
	(77, 'Golf', 16),
	(78, 'Jetta', 16),
	(79, 'Passat', 16),
	(80, 'Polo', 16),
	(81, 'S60', 17),
	(82, 'V60', 17),
	(83, 'XC90', 17),
	(84, 'dolno', 18);
/*!40000 ALTER TABLE `vehicle_models` ENABLE KEYS */;

-- Dumping data for table mechanicum.visits: ~26 rows (approximately)
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
INSERT INTO `visits` (`visit_id`, `vehicle_id`, `arrival_date`, `completion_date`, `paid`, `total_price`, `currency_id`) VALUES
	(1, 1, '2021-03-29', '2021-04-04', 1, 240, 1),
	(2, 2, '2021-03-30', '2021-04-06', 1, 80, 1),
	(3, 16, '2021-03-30', '2021-04-07', 1, 560, 1),
	(4, 1, '2021-04-01', '2021-04-05', 1, 135, 1),
	(5, 9, '2021-04-07', '2021-04-05', 1, 1600, 1),
	(6, 5, '2021-04-07', '2021-04-07', 1, 2500, 1),
	(7, 12, '2021-04-07', '2021-04-08', 1, 40, 1),
	(8, 1, '2021-04-20', NULL, 0, 350, 1),
	(9, 2, '2021-04-30', NULL, 0, 160, 1),
	(10, 5, '2021-04-18', NULL, 0, 70, 1),
	(11, 15, '2021-03-29', NULL, 0, 106.28, 4),
	(12, 3, '2021-04-08', NULL, 0, 120, 1),
	(13, 4, '2021-04-09', NULL, 0, 780.01, 1),
	(14, 6, '2021-04-18', NULL, 0, 80, 2),
	(15, 7, '2021-04-13', NULL, 0, 230, 1),
	(16, 8, '2021-04-14', NULL, 0, 2590, 1),
	(17, 10, '2021-04-06', NULL, 0, 1020, 1),
	(18, 11, '2021-04-07', NULL, 0, 960, 1),
	(19, 13, '2021-04-07', NULL, 0, 1760, 1),
	(20, 14, '2021-04-12', NULL, 0, 5560, 1),
	(21, 16, '2021-04-06', NULL, 0, 5190, 1),
	(22, 17, '2021-04-14', NULL, 0, 810, 1),
	(23, 18, '2021-04-14', NULL, 0, 2060, 1),
	(24, 19, '2021-04-14', NULL, 0, 180, 1),
	(25, 21, '2021-04-18', NULL, 0, 5640.94, 2),
	(26, 21, '2021-04-18', NULL, 0, 235, 1);
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
