USE `mechanicum`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for mechanicum
CREATE DATABASE IF NOT EXISTS `mechanicum` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mechanicum`;

-- Dumping structure for table mechanicum.currencies
CREATE TABLE IF NOT EXISTS `currencies`
(
    `currency_id` int(11)    NOT NULL,
    `name`        varchar(3) NOT NULL,
    PRIMARY KEY (`currency_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

-- Dumping data for table mechanicum.currencies: ~4 rows (approximately)
/*!40000 ALTER TABLE `currencies`
    DISABLE KEYS */;
INSERT INTO `currencies` (`currency_id`, `name`)
VALUES (1, 'BGN'),
       (2, 'USD'),
       (3, 'EUR'),
       (4, 'GBP');
/*!40000 ALTER TABLE `currencies`
    ENABLE KEYS */;

-- Dumping structure for table mechanicum.manufacturers
CREATE TABLE IF NOT EXISTS `manufacturers`
(
    `manufacturer_id` int(11)     NOT NULL AUTO_INCREMENT,
    `name`            varchar(30) NOT NULL,
    PRIMARY KEY (`manufacturer_id`),
    UNIQUE KEY `manufacturers_name_uindex` (`name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 18
  DEFAULT CHARSET = latin1;

-- Dumping data for table mechanicum.manufacturers: ~17 rows (approximately)
/*!40000 ALTER TABLE `manufacturers`
    DISABLE KEYS */;
INSERT INTO `manufacturers` (`manufacturer_id`, `name`)
VALUES (1, 'Alfa Romeo'),
       (2, 'Audi'),
       (3, 'BMW'),
       (4, 'Dacia'),
       (5, 'Fiat'),
       (6, 'Ford'),
       (7, 'Honda'),
       (8, 'Hyundai'),
       (9, 'Jeep'),
       (10, 'Mazda'),
       (11, 'Mercedes-Benz'),
       (12, 'Mitsubishi'),
       (13, 'Nissan'),
       (14, 'Opel'),
       (15, 'Skoda'),
       (16, 'Volkswagen'),
       (17, 'Volvo');
/*!40000 ALTER TABLE `manufacturers`
    ENABLE KEYS */;

-- Dumping structure for table mechanicum.roles
CREATE TABLE IF NOT EXISTS `roles`
(
    `role_id` int(11)     NOT NULL AUTO_INCREMENT,
    `name`    varchar(20) NOT NULL,
    PRIMARY KEY (`role_id`),
    UNIQUE KEY `roles_name_uindex` (`name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = latin1;

-- Dumping data for table mechanicum.roles: ~3 rows (approximately)
/*!40000 ALTER TABLE `roles`
    DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `name`)
VALUES (3, 'Administrator'),
       (2, 'Employee'),
       (1, 'User');
/*!40000 ALTER TABLE `roles`
    ENABLE KEYS */;

-- Dumping structure for table mechanicum.services
CREATE TABLE IF NOT EXISTS `services`
(
    `service_id` int(11) NOT NULL AUTO_INCREMENT,
    `start_date` date    NOT NULL,
    `end_date`   date    NOT NULL,
    `quantity`   int(11) NOT NULL,
    `vehicle_id` int(11) NOT NULL,
    `status_id`  int(11) NOT NULL,
    `type_id`    int(11) NOT NULL,
    `visit_id`   int(11) NOT NULL,
    PRIMARY KEY (`service_id`),
    KEY `services_service_statuses_fk` (`status_id`),
    KEY `services_service_types_fk` (`type_id`),
    KEY `services_vehicles_fk` (`vehicle_id`),
    KEY `services_visits_fk` (`visit_id`),
    CONSTRAINT `services_service_statuses_fk` FOREIGN KEY (`status_id`) REFERENCES `service_statuses` (`status_id`),
    CONSTRAINT `services_service_types_fk` FOREIGN KEY (`type_id`) REFERENCES `service_types` (`type_id`),
    CONSTRAINT `services_vehicles_fk` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`vehicle_id`),
    CONSTRAINT `services_visits_fk` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`visit_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 9
  DEFAULT CHARSET = latin1;

-- Dumping data for table mechanicum.services: ~7 rows (approximately)
/*!40000 ALTER TABLE `services`
    DISABLE KEYS */;
INSERT INTO `services` (`service_id`, `start_date`, `end_date`, `quantity`, `vehicle_id`, `status_id`, `type_id`,
                        `visit_id`)
VALUES (1, '2021-03-30', '2021-04-04', 1, 1, 2, 2, 1),
       (2, '2021-04-05', '2021-04-06', 1, 2, 2, 7, 2),
       (3, '2021-04-04', '2021-04-07', 4, 16, 1, 9, 3),
       (4, '2021-04-01', '2021-04-05', 3, 1, 1, 11, 4),
       (6, '2021-04-02', '2021-04-05', 2, 9, 1, 10, 5),
       (7, '2021-04-03', '2021-04-07', 1, 5, 1, 18, 6),
       (8, '2021-04-03', '2021-04-08', 1, 12, 1, 4, 7);
/*!40000 ALTER TABLE `services`
    ENABLE KEYS */;

-- Dumping structure for table mechanicum.service_statuses
CREATE TABLE IF NOT EXISTS `service_statuses`
(
    `status_id` int(11)     NOT NULL AUTO_INCREMENT,
    `name`      varchar(20) NOT NULL,
    PRIMARY KEY (`status_id`),
    UNIQUE KEY `service_statuses_name_uindex` (`name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = latin1;

-- Dumping data for table mechanicum.service_statuses: ~3 rows (approximately)
/*!40000 ALTER TABLE `service_statuses`
    DISABLE KEYS */;
INSERT INTO `service_statuses` (`status_id`, `name`)
VALUES (2, 'In progress'),
       (1, 'Not started'),
       (3, 'Ready for pickup');
/*!40000 ALTER TABLE `service_statuses`
    ENABLE KEYS */;

-- Dumping structure for table mechanicum.service_types
CREATE TABLE IF NOT EXISTS `service_types`
(
    `type_id`      int(11) NOT NULL AUTO_INCREMENT,
    `name`         varchar(200) DEFAULT NULL,
    `single_price` double  NOT NULL,
    PRIMARY KEY (`type_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 24
  DEFAULT CHARSET = latin1;

-- Dumping data for table mechanicum.service_types: ~23 rows (approximately)
/*!40000 ALTER TABLE `service_types`
    DISABLE KEYS */;
INSERT INTO `service_types` (`type_id`, `name`, `single_price`)
VALUES (1, 'General checkup', 350),
       (2, 'Change oil car', 240),
       (3, 'Change oil SUV', 460),
       (4, 'Change air filter', 40),
       (5, 'Change fuel filter', 40),
       (6, 'Change lights & lenses', 80),
       (7, 'Replace brake fluid', 80),
       (8, 'Change cooling system', 50),
       (9, 'Change brakes', 140),
       (10, 'Engine fix', 800),
       (11, 'Change wheel', 45),
       (12, 'Check handbrake cable', 70),
       (13, 'Check fuel system', 80),
       (14, 'Check braking system', 70),
       (15, 'Check windows for damage', 40),
       (16, 'Check wheel bearing', 120),
       (17, 'Check antifreeze', 35),
       (18, 'Paint whole car', 2500),
       (19, 'Change interior part', 450),
       (20, 'Road test', 150),
       (21, 'Change gear oil', 280),
       (22, 'Change light bulb', 40),
       (23, 'Paint one detail', 180);
/*!40000 ALTER TABLE `service_types`
    ENABLE KEYS */;

-- Dumping structure for table mechanicum.users_authorization
CREATE TABLE IF NOT EXISTS `users_authorization`
(
    `email`    varchar(200) NOT NULL,
    `password` varchar(60)  NOT NULL,
    PRIMARY KEY (`email`),
    UNIQUE KEY `user_info_email_uindex` (`email`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

-- Dumping data for table mechanicum.users_authorization: ~15 rows (approximately)
/*!40000 ALTER TABLE `users_authorization`
    DISABLE KEYS */;
INSERT INTO `users_authorization` (`email`, `password`)
VALUES ('aplivougen@autocare.bg', 'password1'),
       ('cky3k@abv.bg', 'password1'),
       ('dewsag@owlymail.com', 'password1'),
       ('fuanot@owlymail.com', 'password1'),
       ('genbelight@autocare.bg', 'password1'),
       ('ggbg@gmail.com', 'password1'),
       ('hipiandsta@autocare.bg', 'password1'),
       ('iperchemliev@abv.bg', 'password1'),
       ('ishobsenel@autocare.bg', 'password1'),
       ('laibab@owlymail.com', 'password1'),
       ('puwpie@owlymail.com', 'password1'),
       ('ragsie@owlymail.com', 'password1'),
       ('sisdac@owlymail.com', 'password1'),
       ('toneyerney@autocare.bg', 'password1'),
       ('ushumouste@autocare.bg', 'password1');
/*!40000 ALTER TABLE `users_authorization`
    ENABLE KEYS */;

-- Dumping structure for table mechanicum.users_roles
CREATE TABLE IF NOT EXISTS `users_roles`
(
    `email`   varchar(200) NOT NULL,
    `role_id` int(11)      NOT NULL,
    KEY `user_authorisations_roles_roles_fk` (`role_id`),
    KEY `user_authorisations_roles_user_authorisation_fk` (`email`),
    CONSTRAINT `user_authorisations_roles_roles_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
    CONSTRAINT `user_authorisations_roles_user_authorisation_fk` FOREIGN KEY (`email`) REFERENCES `users_authorization` (`email`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

-- Dumping data for table mechanicum.users_roles: ~9 rows (approximately)
/*!40000 ALTER TABLE `users_roles`
    DISABLE KEYS */;
INSERT INTO `users_roles` (`email`, `role_id`)
VALUES ('ggbg@gmail.com', 2),
       ('ggbg@gmail.com', 3),
       ('cky3k@abv.bg', 2),
       ('iperchemliev@abv.bg', 2),
       ('puwpie@owlymail.com', 1),
       ('laibab@owlymail.com', 1),
       ('ragsie@owlymail.com', 1),
       ('dewsag@owlymail.com', 1),
       ('sisdac@owlymail.com', 1);
/*!40000 ALTER TABLE `users_roles`
    ENABLE KEYS */;

-- Dumping structure for table mechanicum.user_info
CREATE TABLE IF NOT EXISTS `user_info`
(
    `user_id`    int(11)      NOT NULL AUTO_INCREMENT,
    `email`      varchar(200) NOT NULL,
    `phone`      varchar(10) DEFAULT NULL,
    `first_name` varchar(30)  NOT NULL,
    `last_name`  varchar(30)  NOT NULL,
    `address`    varchar(40) DEFAULT NULL,
    PRIMARY KEY (`user_id`),
    KEY `customers_user_fk` (`email`),
    CONSTRAINT `customers_user_fk` FOREIGN KEY (`email`) REFERENCES `users_authorization` (`email`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 16
  DEFAULT CHARSET = latin1;

-- Dumping data for table mechanicum.user_info: ~15 rows (approximately)
/*!40000 ALTER TABLE `user_info`
    DISABLE KEYS */;
INSERT INTO `user_info` (`user_id`, `email`, `phone`, `first_name`, `last_name`, `address`)
VALUES (1, 'ggbg@gmail.com', '0888567890', 'Georgi', 'Georgiev', 'Gondor'),
       (2, 'cky3k@abv.bg', '0878567890', 'Ivan', 'Ivanov', 'Mordor'),
       (3, 'iperchemliev@abv.bg', '0887635234', 'Ivan', 'Perchemliev', 'Praga'),
       (4, 'puwpie@owlymail.com', '0887635235', 'William', 'Hampton ', 'Wendover'),
       (5, 'laibab@owlymail.com', '0878567894', 'Daniel', 'Brandt', 'Morpeth'),
       (6, 'ragsie@owlymail.com', '0878567895', 'Jacob', 'Mcloughlin', 'Richmond'),
       (7, 'dewsag@owlymail.com', '0878567896', 'Joseph', 'Boyer', 'Crookston'),
       (8, 'sisdac@owlymail.com', '0878567897', 'Ethan', 'Snyder', 'Dudley'),
       (9, 'fuanot@owlymail.com', '0878567891', 'Edwin ', 'Howard', 'Bronx '),
       (10, 'ushumouste@autocare.bg', '0878267897', 'Ioan', 'Carter', 'Dubuque '),
       (11, 'toneyerney@autocare.bg', '0878562332', 'Layton', 'Woods', 'Looe '),
       (12, 'aplivougen@autocare.bg', '0878567895', 'Xander', 'Jones', 'Martins Ferry'),
       (13, 'genbelight@autocare.bg', '0878567232', 'Mark', 'Mason', 'Uckfield'),
       (14, 'hipiandsta@autocare.bg', '0878568545', 'Oliver ', 'O\'Quinn', 'Silverton '),
       (15, 'ishobsenel@autocare.bg', '087859148', 'Tony', 'Patterson', 'Boulder ');
/*!40000 ALTER TABLE `user_info`
    ENABLE KEYS */;

-- Dumping structure for table mechanicum.vehicles
CREATE TABLE IF NOT EXISTS `vehicles`
(
    `vehicle_id`    int(11)    NOT NULL AUTO_INCREMENT,
    `model_id`      int(11)    NOT NULL,
    `year`          int(11)    NOT NULL,
    `vin`           varchar(17) DEFAULT NULL,
    `licence_plate` varchar(8) NOT NULL,
    `owner`         int(11)    NOT NULL,
    PRIMARY KEY (`vehicle_id`),
    UNIQUE KEY `vehicles_licence_plate_uindex` (`licence_plate`),
    KEY `vehicles_customers_fk` (`owner`),
    KEY `vehicles_vehicle_models_fk` (`model_id`),
    CONSTRAINT `vehicles_customers_fk` FOREIGN KEY (`owner`) REFERENCES `user_info` (`user_id`),
    CONSTRAINT `vehicles_vehicle_models_fk` FOREIGN KEY (`model_id`) REFERENCES `vehicle_models` (`model_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 18
  DEFAULT CHARSET = latin1;

-- Dumping data for table mechanicum.vehicles: ~16 rows (approximately)
/*!40000 ALTER TABLE `vehicles`
    DISABLE KEYS */;
INSERT INTO `vehicles` (`vehicle_id`, `model_id`, `year`, `vin`, `licence_plate`, `owner`)
VALUES (1, 1, 2017, '12345678901234567', 'C6666PB', 2),
       (2, 3, 2011, '1ZVHT80N385187895', 'A6462CD', 5),
       (3, 53, 2009, '1N4AL21E59N587416', 'PB1132KZ', 7),
       (4, 42, 2011, '3FA6P0LU9ER385897', 'CB4326KH', 4),
       (5, 9, 2005, '2CNBJ18U4R6985163', 'CA3754PT', 6),
       (6, 11, 2008, '1GCEG15X861178691', 'CB6037KC', 7),
       (7, 13, 2014, '5GZCZ63464S826932', 'EB4572BH', 8),
       (8, 21, 2005, '2XP5DB0X85M865682', 'EH7285AH', 9),
       (9, 27, 2013, '1FTNX21S1XEB46692', 'CB4181KC', 10),
       (10, 34, 2008, '3N1CB51D05L565271', 'PB2892AT', 11),
       (11, 39, 2015, '1GDHC24U86E209643', 'CH5583AP', 12),
       (12, 51, 2013, '6FTZX1769WKB63004', 'CB5394AH', 13),
       (13, 55, 2006, '5TDZK22C58S235104', 'X5284HH', 5),
       (14, 64, 2008, 'KNAFB161735103589', 'CB4582KC', 8),
       (15, 72, 2013, 'WDDLJ9BB9DA034828', 'B8542KE', 2),
       (16, 83, 2008, 'JTDBT923471140603', 'BT7463TK', 3),
       (17, 45, 2006, '2XP5DB0X85M856372', 'CB3428KK', 14);
/*!40000 ALTER TABLE `vehicles`
    ENABLE KEYS */;

-- Dumping structure for table mechanicum.vehicle_models
CREATE TABLE IF NOT EXISTS `vehicle_models`
(
    `model_id`        int(11)     NOT NULL AUTO_INCREMENT,
    `model`           varchar(30) NOT NULL,
    `manufacturer_id` int(11)     NOT NULL,
    PRIMARY KEY (`model_id`),
    UNIQUE KEY `vehicle_models_model_uindex` (`model`),
    KEY `vehicle_models_manufacturers_fk` (`manufacturer_id`),
    CONSTRAINT `vehicle_models_manufacturers_fk` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`manufacturer_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 84
  DEFAULT CHARSET = latin1;

-- Dumping data for table mechanicum.vehicle_models: ~83 rows (approximately)
/*!40000 ALTER TABLE `vehicle_models`
    DISABLE KEYS */;
INSERT INTO `vehicle_models` (`model_id`, `model`, `manufacturer_id`)
VALUES (1, 'Alfa 156', 1),
       (2, 'Alfa 159', 1),
       (3, 'Crosswagon', 1),
       (4, 'Giulia', 1),
       (5, 'Giulietta', 1),
       (6, 'GT', 1),
       (7, 'Stelvio', 1),
       (8, 'A3', 2),
       (9, 'A4', 2),
       (10, 'A5', 2),
       (11, 'A6', 2),
       (12, 'A7', 2),
       (13, 'A8', 2),
       (14, 'Q5', 2),
       (15, 'Q7', 2),
       (16, 'RS4', 2),
       (17, 'RS6', 2),
       (18, '320', 3),
       (19, '330', 3),
       (20, '530', 3),
       (21, '550', 3),
       (22, '640', 3),
       (23, '650', 3),
       (24, '745', 3),
       (25, '750', 3),
       (26, 'M3', 3),
       (27, 'M5', 3),
       (28, 'X5', 3),
       (29, 'X7', 3),
       (30, 'Logan', 4),
       (31, 'Duster', 4),
       (32, 'Dokker', 4),
       (33, '500', 5),
       (34, 'Brava', 5),
       (35, 'Chroma', 5),
       (36, 'Panda', 5),
       (37, 'Focus', 6),
       (38, 'Fiesta', 6),
       (39, 'C-Max', 6),
       (40, 'Civic', 7),
       (41, 'CR-V', 7),
       (42, 'HR-V', 7),
       (43, 'Prelude', 7),
       (44, 'Santa Fe', 8),
       (45, 'IONIQ', 8),
       (46, 'Kona', 8),
       (47, 'Grand Cherokee', 9),
       (48, '323', 10),
       (49, '625', 10),
       (50, 'CX-5', 10),
       (51, 'CX-7', 10),
       (52, 'C230', 11),
       (53, 'C350', 11),
       (54, 'E230', 11),
       (55, 'E350', 11),
       (56, 'ML500', 11),
       (57, 'GL500', 11),
       (58, 'G500', 11),
       (59, 'Lancer', 12),
       (60, 'Outlander', 12),
       (61, 'Pajero', 12),
       (62, 'Micra', 13),
       (63, 'Navara', 13),
       (64, 'Qashqai', 13),
       (65, 'Primiera', 13),
       (66, 'Astra', 14),
       (67, 'Insignia', 14),
       (68, 'Meriva', 14),
       (69, 'Moka', 14),
       (70, 'Fabia', 15),
       (71, 'Octavia', 15),
       (72, 'Rapid', 15),
       (73, 'Superb', 15),
       (74, 'Yeti', 15),
       (75, 'Bora', 16),
       (76, 'Caddy', 16),
       (77, 'Golf', 16),
       (78, 'Jetta', 16),
       (79, 'Passat', 16),
       (80, 'Polo', 16),
       (81, 'S60', 17),
       (82, 'V60', 17),
       (83, 'XC90', 17);
/*!40000 ALTER TABLE `vehicle_models`
    ENABLE KEYS */;

-- Dumping structure for table mechanicum.visits
CREATE TABLE IF NOT EXISTS `visits`
(
    `visit_id`    int(11)    NOT NULL AUTO_INCREMENT,
    `date`        date       NOT NULL,
    `paid`        tinyint(1) NOT NULL DEFAULT 0,
    `total_price` double              DEFAULT NULL,
    `currency_id` int(11)    NOT NULL,
    PRIMARY KEY (`visit_id`),
    KEY `visits_currencies_fk` (`currency_id`),
    CONSTRAINT `visits_currencies_fk` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`currency_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 8
  DEFAULT CHARSET = latin1;

-- Dumping data for table mechanicum.visits: ~7 rows (approximately)
/*!40000 ALTER TABLE `visits`
    DISABLE KEYS */;
INSERT INTO `visits` (`visit_id`, arrival_date, `paid`, `total_price`, `currency_id`)
VALUES (1, '2021-03-29', 1, 240, 4),
       (2, '2021-03-30', 0, NULL, 1),
       (3, '2021-03-30', 0, NULL, 2),
       (4, '2021-04-01', 0, NULL, 3),
       (5, '2021-04-07', 0, NULL, 1),
       (6, '2021-04-07', 0, NULL, 1),
       (7, '2021-04-07', 0, NULL, 1);
/*!40000 ALTER TABLE `visits`
    ENABLE KEYS */;

/*!40101 SET SQL_MODE = IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS = IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
