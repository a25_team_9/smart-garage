DROP DATABASE IF EXISTS `mechanicum`;
CREATE DATABASE  IF NOT EXISTS `mechanicum`;
USE `mechanicum`;

create table currencies
(
    currency_id int        not null
        primary key,
    name        varchar(3) not null
);

create table manufacturers
(
    manufacturer_id int auto_increment
        primary key,
    name            varchar(30) not null,
    constraint manufacturers_name_uindex
        unique (name)
);

create table roles
(
    role_id int auto_increment
        primary key,
    name    varchar(20) not null,
    constraint roles_name_uindex
        unique (name)
);

create table service_statuses
(
    status_id int auto_increment
        primary key,
    name      varchar(20) not null,
    constraint service_statuses_name_uindex
        unique (name)
);

create table service_types
(
    type_id      int auto_increment
        primary key,
    name         varchar(200) null,
    single_price double       not null,
    constraint service_types_name_uindex
        unique (name)
);

create table users_authorization
(
    email    varchar(200) not null,
    password varchar(60)  not null,
    constraint user_info_email_uindex
        unique (email)
);

alter table users_authorization
    add primary key (email);

create table user_info
(
    user_id    int auto_increment
        primary key,
    email      varchar(200) not null,
    phone      varchar(10)  null,
    first_name varchar(30)  not null,
    last_name  varchar(30)  not null,
    address    varchar(40)  null,
    constraint customers_user_fk
        foreign key (email) references users_authorization (email)
);

create table users_roles
(
    email   varchar(200) not null,
    role_id int          not null,
    constraint user_authorisations_roles_roles_fk
        foreign key (role_id) references roles (role_id),
    constraint user_authorisations_roles_user_authorisation_fk
        foreign key (email) references users_authorization (email)
);

create table vehicle_models
(
    model_id        int auto_increment
        primary key,
    model           varchar(30) not null,
    manufacturer_id int         not null,
    constraint vehicle_models_model_uindex
        unique (model),
    constraint vehicle_models_manufacturers_fk
        foreign key (manufacturer_id) references manufacturers (manufacturer_id)
);

create table vehicles
(
    vehicle_id    int auto_increment
        primary key,
    model_id      int         not null,
    year          int         not null,
    vin           varchar(17) null,
    licence_plate varchar(8)  not null,
    owner         int         not null,
    constraint vehicles_licence_plate_uindex
        unique (licence_plate),
    constraint vehicles_customers_fk
        foreign key (owner) references user_info (user_id),
    constraint vehicles_vehicle_models_fk
        foreign key (model_id) references vehicle_models (model_id)
);

create table visits
(
    visit_id    int auto_increment
        primary key,
    date        date                 not null,
    paid        tinyint(1) default 0 not null,
    total_price double               null,
    currency_id int                  not null,
    constraint visits_currencies_fk
        foreign key (currency_id) references currencies (currency_id)
);

create table services
(
    service_id int auto_increment
        primary key,
    start_date date not null,
    end_date   date not null,
    quantity   int  not null,
    vehicle_id int  not null,
    status_id  int  not null,
    type_id    int  not null,
    visit_id   int  not null,
    constraint services_service_statuses_fk
        foreign key (status_id) references service_statuses (status_id),
    constraint services_service_types_fk
        foreign key (type_id) references service_types (type_id),
    constraint services_vehicles_fk
        foreign key (vehicle_id) references vehicles (vehicle_id),
    constraint services_visits_fk
        foreign key (visit_id) references visits (visit_id)
);

