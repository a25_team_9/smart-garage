CREATE USER IF NOT EXISTS 'mechanicum'@'%.%.%.%' IDENTIFIED BY 'mechanicum';
GRANT ALL PRIVILEGES ON mechanicum.* TO 'mechanicum'@'%.%.%.%' IDENTIFIED BY 'mechanicum';
FLUSH PRIVILEGES;

