package com.telerikacademy.mechanicum;

import com.telerikacademy.mechanicum.models.*;
import com.telerikacademy.mechanicum.models.converter.Converter;
import com.telerikacademy.mechanicum.models.converter.Result;
import com.telerikacademy.mechanicum.models.converter.ResultFromAndTo;
import com.telerikacademy.mechanicum.models.converter.ResultRate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ModelHelper {

    public static Manufacturer createMockManufacturer() {
        var mockManufacturer = new Manufacturer();
        mockManufacturer.setId(1);
        mockManufacturer.setName("MockMake");
        return mockManufacturer;
    }

    public static VehicleModel createMockModel() {
        var mockModel = new VehicleModel();
        mockModel.setId(1);
        mockModel.setModel("MockModel");
        mockModel.setManufacturer(createMockManufacturer());
        return mockModel;
    }

    public static Vehicle createMockVehicle() {
        var mockVehicle = new Vehicle();
        mockVehicle.setId(1);
        mockVehicle.setModel(createMockModel());
        mockVehicle.setYear(2000);
        mockVehicle.setVin("1FTNX21S1XEB46692");
        mockVehicle.setLicencePlate("C6666PB");
        mockVehicle.setOwner(createMockUser());
        return mockVehicle;
    }

    public static UserInfo createMockEmployee() {
        var mockEmployee = new UserInfo();
        mockEmployee.setUserAuthorization(createMockUserAuthorization());
        mockEmployee.setUserID(1);
        mockEmployee.setPhone("0888123456");
        mockEmployee.setFirstName("John");
        mockEmployee.setLastName("Doe");
        mockEmployee.setAddress("test address");
        Set<Vehicle> vehicleList = new HashSet<>();
        vehicleList.add(createMockVehicle());
        mockEmployee.setVehicleList(vehicleList);
        return mockEmployee;
    }

    public static UserInfo createMockUser() {
        var mockUser = new UserInfo();
        var mockUserAuthorization = new UserAuthorization();
        mockUserAuthorization.setEmail("mock_user@test.com");
        mockUserAuthorization.setPassword("5YpSn2Xn3N");
        Role userRole = new Role(2, "User");
        Set<Role> roles = new HashSet<>();
        roles.add(userRole);
        mockUserAuthorization.setRoles(roles);
        mockUser.setUserID(1);
        mockUser.setUserAuthorization(mockUserAuthorization);
        mockUser.setPhone("0888123456");
        mockUser.setFirstName("Boo");
        mockUser.setLastName("Foo");
        mockUser.setAddress("test address");
        return mockUser;
    }

    public static UserAuthorization createMockUserAuthorization() {
        var mockUserAuthorization = new UserAuthorization();
        mockUserAuthorization.setEmail("mock@test.com");
        mockUserAuthorization.setPassword("5YpSn2Xn3N");
        Set<Role> roles = new HashSet<>();
        roles.add(createMockRoleEmployee());
        mockUserAuthorization.setRoles(roles);
        return mockUserAuthorization;
    }

    public static Role createMockRoleEmployee() {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setName("Employee");
        return mockRole;
    }

    public static Role createMockRoleUser() {
        var mockRole = new Role();
        mockRole.setId(2);
        mockRole.setName("User");
        return mockRole;
    }

    public static Currency createMockCurrency(){
        var mockCurrency = new Currency();
        mockCurrency.setId(1);
        mockCurrency.setName("mockCurrency");
        return mockCurrency;
    }

    public static Visit createMockVisit() {
        var mockVisit = new Visit();
        mockVisit.setId(1);
        mockVisit.setPaid(true);
        mockVisit.setTotalPrice(100.00);
        mockVisit.setVehicle(createMockVehicle());
        mockVisit.setCurrency(createMockCurrency());
        mockVisit.setServiceList(new ArrayList<>());
        return mockVisit;
    }

    public static ServiceStatus createMockServiceStatus() {
        var mockServiceStatus = new ServiceStatus();
        mockServiceStatus.setId(1);
        mockServiceStatus.setName("mockReady");
        return mockServiceStatus;
    }

    public static ServiceType createMockServiceType() {
        var mockServiceType = new ServiceType();
        mockServiceType.setId(1);
        mockServiceType.setName("mockServiceType");
        mockServiceType.setSinglePrice(100.00);
        return mockServiceType;
    }

    public static Service createMockService() {
        var mockService = new Service();
        mockService.setId(1);
        mockService.setStartDate(LocalDate.now());
        mockService.setEndDate(LocalDate.now().plusDays(10));
        mockService.setQuantity(1);
        mockService.setServiceStatus(createMockServiceStatus());
        mockService.setServiceType(createMockServiceType());
        mockService.setVisit(createMockVisit());
        return mockService;
    }

    public static ResultRate createMockResultRate() {
        var mockResultRate = new ResultRate();
        mockResultRate.setValue(2.0);
        mockResultRate.setFormat("mockGBP");
        return mockResultRate;
    }

    public static ResultFromAndTo createMockResultFrom() {
        var mockResultFromAndTo = new ResultFromAndTo();
        mockResultFromAndTo.setPrice_usd(1.0);
        mockResultFromAndTo.setCode("mockCodeFrom");
        mockResultFromAndTo.setName("mockNameFrom");
        mockResultFromAndTo.setUpdated("mockUpdateFrom");
        return mockResultFromAndTo;
    }

    public static ResultFromAndTo createMockResultTo() {
        var mockResultFromAndTo = new ResultFromAndTo();
        mockResultFromAndTo.setPrice_usd(2.0);
        mockResultFromAndTo.setCode("mockCodeTo");
        mockResultFromAndTo.setName("mockNameTo");
        mockResultFromAndTo.setUpdated("mockUpdateTo");
        return mockResultFromAndTo;
    }

    public static Result createMockResult() {
        var mockResult = new Result();
        mockResult.setFrom(createMockResultFrom());
        mockResult.setTo(createMockResultTo());
        mockResult.setResult(createMockResultRate());
        return mockResult;
    }

    public static Converter createMockConverter() {
        var mockConverter = new Converter();
        mockConverter.setSuccess(true);
        mockConverter.setResult(createMockResult());
        return mockConverter;
    }
}
