package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Currency;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.repositories.contracts.CurrencyRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.mechanicum.ModelHelper.*;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceImplTests {

    @Mock
    CurrencyRepository mockCurrencyRepository;

    @InjectMocks
    CurrencyServiceImpl mockCurrencyService;

    UserInfo mockEmployee;
    UserInfo mockUser;
    Currency mockCurrency;

    @BeforeEach
    public void initialized() {
        mockEmployee = createMockEmployee();
        mockUser = createMockUser();
        mockCurrency = createMockCurrency();
    }

    @Test
    public void getAll_Should_CallRepository_When_Called() {
        //Arrange, Act
        mockCurrencyService.getAll();

        //Assert
        Mockito.verify(mockCurrencyRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_Should_CallRepository_When_Called() {
        //Arrange, Act
        mockCurrencyService.getById(mockCurrency.getId());

        //Assert
        Mockito.verify(mockCurrencyRepository, Mockito.times(1))
                .getById(mockCurrency.getId());
    }

    @Test
    public void create_Should_ThrowException_When_UserNotEmployee() {
        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> mockCurrencyService.create(mockCurrency,mockUser));
    }

 @Test
 public void create_Should_ThrowException_When_CurrencyNameExists() {
     //Arrange
     Mockito.when(mockCurrencyRepository.getByName(mockCurrency.getName()))
             .thenReturn(mockCurrency);

     //Act, Assert
     Assertions.assertThrows(DuplicateEntityException.class,
             ()-> mockCurrencyService.create(mockCurrency,mockEmployee));
 }

    @Test
    public void create_Should_CallRepository_When_UserEmployee() {
        //Arrange
        Mockito.when(mockCurrencyRepository.getByName(mockCurrency.getName()))
                .thenThrow(new EntityNotFoundException("Currency", "name", mockCurrency.getName()));

        //Act
        mockCurrencyService.create(mockCurrency,mockEmployee);

        //Assert
        Mockito.verify(mockCurrencyRepository, Mockito.times(1))
                .create(mockCurrency);
    }

    @Test
    public void update_Should_ThrowException_When_UserNotEmployee() {
        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> mockCurrencyService.update(mockCurrency,mockUser));
    }

    @Test
    public void update_Should_CallRepository_When_UserEmployee() {
        //Arrange, Act
        mockCurrencyService.update(mockCurrency,mockEmployee);

        //Assert
        Mockito.verify(mockCurrencyRepository,Mockito.times(1))
                .update(mockCurrency);
    }

    @Test
    public void delete_Should_ThrowException_When_UserNotEmployee() {
        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> mockCurrencyService.delete(mockCurrency.getId(),mockUser));
    }

    @Test
    public void delete_Should_CallRepository_When_UserEmployee() {
        //Arrange,
        Mockito.when(mockCurrencyRepository.getById(mockCurrency.getId())).thenReturn(mockCurrency);

        // Act
        mockCurrencyService.delete(mockCurrency.getId(),mockEmployee);

        //Assert
        Mockito.verify(mockCurrencyRepository,Mockito.times(1))
                .delete(mockCurrency);
    }
}
