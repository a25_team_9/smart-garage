package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Vehicle;
import com.telerikacademy.mechanicum.repositories.contracts.VehicleRepository;
import com.telerikacademy.mechanicum.repositories.contracts.VisitRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.telerikacademy.mechanicum.ModelHelper.*;

@ExtendWith(MockitoExtension.class)
public class VehicleServiceImplTests {

    @Mock
    VehicleRepository mockVehicleRepository;

    @Mock
    VisitRepository mockVisitRepository;

    @InjectMocks
    VehicleServiceImpl mockService;

    UserInfo mockEmployee;
    UserInfo mockUser;
    Vehicle mockVehicle;

    @BeforeEach
    public void initialized() {
        mockEmployee = createMockEmployee();
        mockUser = createMockUser();
        mockVehicle = createMockVehicle();
    }

    @Test
    public void getAll_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.getAll(mockUser));
    }

    @Test
    public void getAll_Should_CallService_When_UserIsEmployee() {
        // Act
        mockService.getAll(mockEmployee);
        // Assert
        Mockito.verify(mockVehicleRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ThrowException_When_userIsNotEmployeeOrOwner() {
        // Arrange
        var anotherMockUser = createMockUser();
        anotherMockUser.setUserID(2);
        Mockito.when(mockVehicleRepository.getById(mockVehicle.getId()))
                .thenReturn(mockVehicle);
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.getById(mockVehicle.getId(), anotherMockUser));
    }

    @Test
    public void getById_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        Mockito.when(mockVehicleRepository.getById(mockVehicle.getId()))
                .thenReturn(mockVehicle);
        // Act
        mockService.getById(mockVehicle.getId(), mockEmployee);
        // Assert
        Mockito.verify(mockVehicleRepository, Mockito.times(2)).getById(mockVehicle.getId());
    }

    @Test
    public void getById_Should_CallRepository_When_UserIsOwner() {
        // Arrange
        Mockito.when(mockVehicleRepository.getById(mockVehicle.getId()))
                .thenReturn(mockVehicle);
        // Act
        mockService.getById(mockVehicle.getId(), mockUser);
        // Assert
        Mockito.verify(mockVehicleRepository, Mockito.times(2)).getById(mockVehicle.getId());
    }

    @Test
    public void create_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.create(mockVehicle, mockUser));
    }

    @Test
    public void create_Should_ThrowException_When_VinExist() {
        // Arrange
        Mockito.when(mockVehicleRepository.getByVin(mockVehicle.getVin()))
                .thenReturn(mockVehicle);
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(mockVehicle, mockEmployee));
    }

    @Test
    public void create_Should_ThrowException_When_LicencePlateExist() {
        // Arrange
        Mockito.when(mockVehicleRepository.getByVin(mockVehicle.getVin()))
                .thenThrow(new EntityNotFoundException("Vehicle", "vin", mockVehicle.getVin()));
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(mockVehicle, mockEmployee));
    }


    @Test
    public void create_Should_CallRepository_When_VinAndLicencePlateNotExist() {
        // Arrange
        Mockito.when(mockVehicleRepository.getByVin(mockVehicle.getVin()))
                .thenThrow(new EntityNotFoundException("Vehicle", "vin", mockVehicle.getVin()));
        Mockito.when(mockVehicleRepository.getByLicencePlate(mockVehicle.getLicencePlate()))
                .thenThrow(new EntityNotFoundException("Vehicle", "licence plate", mockVehicle.getLicencePlate()));
        // Act
        mockService.create(mockVehicle, mockEmployee);
        // Assert
        Mockito.verify(mockVehicleRepository, Mockito.times(1)).create(mockVehicle);
    }

    @Test
    public void update_Should_ThrowException_When_userIsNotEmployeeOrOwner() {
        // Arrange
        var anotherMockUser = createMockUser();
        anotherMockUser.setUserID(2);
        Mockito.when(mockVehicleRepository.getById(mockVehicle.getId()))
                .thenReturn(mockVehicle);
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.update(mockVehicle, anotherMockUser));
    }

    @Test
    public void update_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        Mockito.when(mockVehicleRepository.getById(mockVehicle.getId()))
                .thenReturn(mockVehicle);
        // Act
        mockService.update(mockVehicle, mockEmployee);
        // Assert
        Mockito.verify(mockVehicleRepository, Mockito.times(1)).update(mockVehicle);
    }

    @Test
    public void update_Should_CallRepository_When_UserIsOwner() {
        // Arrange
        Mockito.when(mockVehicleRepository.getById(mockVehicle.getId()))
                .thenReturn(mockVehicle);
        // Act
        mockService.update(mockVehicle, mockUser);
        // Assert
        Mockito.verify(mockVehicleRepository, Mockito.times(1)).update(mockVehicle);
    }

    @Test
    public void delete_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.delete(mockVehicle.getId(), mockUser));
    }

    @Test
    public void delete_Should_CallRepository_When_userIsEmployee() {
        // Arrange
        Mockito.when(mockVehicleRepository.getById(Mockito.anyInt()))
                .thenReturn(mockVehicle);
        Mockito.when(mockVisitRepository.getCustomerLinkedServices(mockUser.getUserID(),
                Optional.of(mockVehicle.getId()),
                Optional.empty()))
                .thenThrow(EntityNotFoundException.class);
        // Act
        mockService.delete(mockVehicle.getId(), mockEmployee);
        // Assert
        Mockito.verify(mockVehicleRepository, Mockito.times(1)).delete(mockVehicle);
    }

    @Test
    public void delete_Should_ThrowException_When_VehicleHasVisits() {
        // Arrange
        Mockito.when(mockVehicleRepository.getById(Mockito.anyInt()))
                .thenReturn(mockVehicle);
        Mockito.when(mockVisitRepository.getCustomerLinkedServices(mockUser.getUserID(),
                Optional.of(mockVehicle.getId()),
                Optional.empty()))
                .thenReturn(new ArrayList<>());
        // Act, Assert
        Assertions.assertThrows(InvalidFieldException.class,
                () -> mockService.delete(mockVehicle.getId(), mockEmployee));
    }

    @Test
    public void filter_Should_ThrowException_When_UserNotEmployee() {
        //Arrange
        Map<String, String> mockFilterMap = new HashMap<>();

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.filter(mockFilterMap, mockUser));
    }

    @Test
    public void filter_Should_CallRepository_When_UserEmployee() {
        //Arrange
        Map<String, String> mockFilterMap = new HashMap<>();

        //Act
        mockService.filter(mockFilterMap, mockEmployee);

        //Assert
        Mockito.verify(mockVehicleRepository, Mockito.times(1))
                .filter(mockFilterMap);
    }

    @Test
    public void getServicesByCarId_Should_ThrowException_When_UserNotEmployeeOrOwner() {
        //Arrange
        var mockOwner = createMockUser();
        mockOwner.setUserID(404);
        mockVehicle.setOwner(mockOwner);
        Mockito.when(mockVehicleRepository.getById(mockVehicle.getId())).thenReturn(mockVehicle);
        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.getServicesByCarId(mockVehicle.getId(), mockUser));
    }

    @Test
    public void getServicesByCarId_Should_CallRepository_When_UserOwner() {
        //Arrange
        mockVehicle.setOwner(mockUser);
        Mockito.when(mockVehicleRepository.getById(mockVehicle.getId())).thenReturn(mockVehicle);

        //Act
        mockService.getServicesByCarId(mockVehicle.getId(), mockUser);

        //Assert
        Mockito.verify(mockVehicleRepository, Mockito.times(1))
                .getServicesByCarId(mockVehicle.getId());
    }

    @Test
    public void getAllVehiclesCount_Should_CallRepository_When_Called() {
        //Arrange, Act
        mockService.getAllVehiclesCount();

        //Assert
        Mockito.verify(mockVehicleRepository, Mockito.times(1))
                .getAllVehiclesCount();
    }
}
