package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.ServiceStatus;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceRepository;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceStatusRepository;
import com.telerikacademy.mechanicum.repositories.contracts.VisitRepository;
import com.telerikacademy.mechanicum.utils.ExchangeRateService;
import com.telerikacademy.mechanicum.utils.contracts.EmailService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

import static com.telerikacademy.mechanicum.ModelHelper.*;

@ExtendWith(MockitoExtension.class)
public class ServiceServiceImplTests {

    @Mock
    ServiceRepository mockServiceRepository;

    @Mock
    VisitRepository mockVisitRepository;

    @Mock
    ServiceStatusRepository mockServiceStatusRepository;

    @Mock
    ExchangeRateService mockExchangeRateService;

    @Mock
    EmailService mockEmailService;

    @InjectMocks
    ServiceServiceImpl mockServiceService;


    UserInfo mockEmployee;
    UserInfo mockUser;
    Service mockService;
    Visit mockVisit;
    ServiceStatus mockServiceStatus;

    @BeforeEach
    public void initialized() {
        mockEmployee = createMockEmployee();
        mockUser = createMockUser();
        mockVisit = createMockVisit();
        mockService = createMockService();
        mockServiceStatus = createMockServiceStatus();
    }

    @Test
    public void getAll_Should_CallRepository_When_Called() {
        //Arrange, Act
        mockServiceService.getAll(mockEmployee);

        //Assert
        Mockito.verify(mockServiceRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAll_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockServiceService.getAll(mockUser));
    }

    @Test
    public void getById_Should_CallRepository_When_Called() {
        //Arrange, Act
        mockServiceService.getById(mockService.getId(), mockEmployee);

        //Assert
        Mockito.verify(mockServiceRepository, Mockito.times(1))
                .getById(mockService.getId());
    }

    @Test
    public void getById_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockServiceService.getById(mockService.getId(), mockUser));
    }

    @Test
    public void create_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockServiceService.create(mockService, mockUser));
    }

    @Test
    public void create_Should_CallServiceRepository_When_Called() {
        //Arrange
        mockVisit.getServiceList().add(mockService);
        Mockito.when(mockVisitRepository.getByID(mockService.getVisit().getId()))
                .thenReturn(mockVisit);
        Mockito.when(mockExchangeRateService.getConverter("BGN", mockVisit.getCurrency().getName()))
                .thenReturn(createMockConverter());

        //Act
        mockServiceService.create(mockService, mockEmployee);

        //Assert
        Mockito.verify(mockServiceRepository, Mockito.times(1))
                .create(mockService);
    }

    @Test
    public void create_Should_ThrowException_When_VisitComplete() {
        //Arrange
        mockVisit.setArrivalDate(LocalDate.of(2020,01,01));
        mockVisit.setCompletionDate(LocalDate.of(2020,01,02));
        mockService.setVisit(mockVisit);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> mockServiceService.create(mockService,mockEmployee));
    }

    @Test
    public void create_Should_ThrowException_When_ServiceExistsInVisit() {
        //Arrange
        mockVisit.setServiceList(List.of(mockService));
        mockService.setVisit(mockVisit);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                ()-> mockServiceService.create(mockService,mockEmployee));
    }

    @Test
    public void update_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockServiceService.update(mockService, mockUser));
    }

    @Test
    public void update_Should_SetStatusToCompleted_When_endDateIsInPast() {
        //Arrange
        mockVisit.getServiceList().add(mockService);
        mockService.setVisit(mockVisit);
        mockService.setEndDate(LocalDate.now().minusDays(1));
        Mockito.when(mockServiceStatusRepository.getById(3))
                .thenReturn(mockServiceStatus);
        Mockito.when(mockVisitRepository.getByID(mockService.getVisit().getId()))
                .thenReturn(mockVisit);
        Mockito.when(mockExchangeRateService.getConverter("BGN", mockVisit.getCurrency().getName()))
                .thenReturn(createMockConverter());

        Mockito.when(mockServiceRepository.checkVisitCompletionAllServices(mockService.getVisit().getId()))
                .thenReturn(true);

        //Act
        mockServiceService.update(mockService, mockEmployee);

        //Assert
        Mockito.verify(mockServiceRepository, Mockito.times(1))
                .update(mockService);
    }


    @Test
    public void delete_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockServiceService.delete(mockService.getId(), mockUser));
    }

    @Test
    public void delete_Should_CallRepository_When_Called() {
        //Arrange
        mockVisit.getServiceList().add(mockService);
        mockService.setVisit(mockVisit);
        Mockito.when(mockServiceRepository.getById(mockService.getId()))
                .thenReturn(mockService);
        Mockito.when(mockVisitRepository.getByID(mockVisit.getId()))
                .thenReturn(mockVisit);
        Mockito.when(mockExchangeRateService.getConverter("BGN", mockVisit.getCurrency().getName()))
                .thenReturn(createMockConverter());

        //Act
        mockServiceService.delete(mockService.getId(), mockEmployee);

        //Assert
        Mockito.verify(mockServiceRepository, Mockito.times(1))
                .delete(mockService);
    }
}
