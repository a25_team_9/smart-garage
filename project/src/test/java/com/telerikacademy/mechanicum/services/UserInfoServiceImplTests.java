package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.*;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceRepository;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceStatusRepository;
import com.telerikacademy.mechanicum.repositories.contracts.UserInfoRepository;
import com.telerikacademy.mechanicum.repositories.contracts.VisitRepository;
import com.telerikacademy.mechanicum.services.contracts.VehicleService;
import com.telerikacademy.mechanicum.utils.contracts.EmailService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static com.telerikacademy.mechanicum.ModelHelper.*;

@ExtendWith(MockitoExtension.class)
public class UserInfoServiceImplTests {


    @Mock
    UserInfoRepository mockUserInfoRepository;

    @Mock
    ServiceRepository mockServiceRepository;

    @Mock
    VisitRepository mockVisitRepository;

    @Mock
    ServiceStatusRepository mockServiceStatusRepository;

    @Mock
    EmailService mockEmailService;

    @Mock
    VehicleService mockVehicleService;

    @InjectMocks
    UserInfoServiceImpl mockUserInfoService;

    @InjectMocks
    ServiceStatusServiceImpl mockServiceStatusService;

    UserInfo mockEmployee;
    UserInfo mockUser;
    Service mockService;
    Visit mockVisit;
    ServiceStatus mockServiceStatus;

    @BeforeEach
    public void initialized() {
        mockEmployee = createMockEmployee();
        mockUser = createMockUser();
        mockVisit = createMockVisit();
        mockService = createMockService();
        mockServiceStatus = createMockServiceStatus();
    }

    @Test
    public void getAll_Should_CallRepository_When_Called() {
        //Arrange, Act
        mockUserInfoService.getAll(mockEmployee);

        //Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAll_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserInfoService.getAll(mockUser));
    }

    @Test
    public void getAllCustomers_Should_CallRepository_When_Called() {
        //Arrange, Act
        mockUserInfoService.getAllCustomers(mockEmployee);

        //Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .getAllCustomers();
    }

    @Test
    public void getAllCustomers_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserInfoService.getAllCustomers(mockUser));
    }

    @Test
    public void getById_Should_CallRepository_When_Called() {
        //Arrange, Act
        mockUserInfoService.getByID(mockUser.getUserID(), mockEmployee);

        //Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .getByID(mockUser.getUserID());
    }

    @Test
    public void getById_Should_ThrowException_When_UserNotAuthorized() {
        //Arrange
        UserInfo userInfo = createMockUser();
        userInfo.setUserID(13);
        Mockito.when(mockUserInfoRepository.getByID(mockUser.getUserID()))
                .thenReturn(userInfo);
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserInfoService.getByID(mockUser.getUserID(), mockUser));
    }


    @Test
    public void create_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserInfoService.create(mockUser, mockUser));
    }

    @Test
    public void create_Should_ThrowException_When_EmailExist() {
        // Arrange
        Mockito.when(mockUserInfoRepository.getByEmail(mockUser.getUserAuthorization().getEmail()))
                .thenReturn(mockUser);
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUserInfoService.create(mockUser, mockEmployee));
    }

    @Test
    public void create_Should_CallRepository_When_Called() {
        // Arrange
        Mockito.when(mockUserInfoRepository.getByEmail(mockUser.getUserAuthorization().getEmail()))
                .thenThrow(EntityNotFoundException.class);
        // Act, Assert
        mockUserInfoService.create(mockUser, mockEmployee);

        //Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .create(mockUser);
    }


    @Test
    public void update_Should_ThrowException_When_userIsNotEmployeeOrOwner() {
        // Act, Assert
        UserInfo userInfo = createMockUser();
        userInfo.setUserID(13);
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserInfoService.update(mockUser, userInfo));
    }

    @Test
    public void update_Should_CallRepository_When_Called() {
        // Act, Assert
        mockUserInfoService.update(mockUser, mockEmployee);

        //Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    public void delete_Should_ThrowException_When_UserNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserInfoService.delete(mockUser.getUserID(), mockUser));
    }

    @Test
    public void delete_Should_ThrowException_When_UserHasVisits() {
        //Arrange
        Mockito.when(mockUserInfoRepository.getByID(mockUser.getUserID()))
                .thenReturn(mockUser);
        Mockito.when(mockVisitRepository.getCustomerVisits(mockUser.getUserID(),1))
                .thenReturn(List.of(mockVisit));
        //Act, Assert
        Assertions.assertThrows(InvalidFieldException.class,
                ()-> mockUserInfoService.delete(mockUser.getUserID(),mockEmployee));
    }

    @Test
    public void delete_Should_CallRepository_When_Called() {
        // Arrange
        var mockVehicle = createMockVehicle();
        Set<Vehicle> mockVehicleList = new HashSet<>();
        mockUser.setVehicleList(mockVehicleList);
        List<Visit> mockVisitList = new ArrayList<>();
        mockVehicleList.add(createMockVehicle());
        Mockito.when(mockUserInfoRepository.getByID(mockUser.getUserID()))
                .thenReturn(mockUser);
        Mockito.when(mockVisitRepository.getCustomerVisits(mockUser.getUserID(),1))
                .thenReturn(mockVisitList);
        //Act
        mockUserInfoService.delete(mockUser.getUserID(), mockEmployee);
        //Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .delete(mockUser);
    }

    @Test
    public void filter_Should_ThrowException_When_userIsNotEmployeeOrOwner() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserInfoService.filter(new HashMap<String, String>(), mockUser));
    }

    @Test
    public void filter_Should_CallRepository_When_Called() {
        // Act, Assert
        mockUserInfoService.filter(new HashMap<String, String>(), mockEmployee);

        //Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .filter(new HashMap<>());
    }

    @Test
    public void sortByNameOrVisitDate_Should_ThrowException_When_userIsNotEmployeeOrOwner() {
        // Act, Assert
        UserInfo userInfo = createMockUser();
        userInfo.setUserID(13);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserInfoService.sortByNameOrVisitDate(new HashMap<String, String>(), userInfo));
    }

    @Test
    public void sortByNameOrVisitDate_Should_CallRepository_When_Called() {
        // Act, Assert


        mockUserInfoService.sortByNameOrVisitDate(new HashMap<String, String>(), mockEmployee);
        //Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .sort(new HashMap<>());
    }

    @Test
    public void getByEmail_Should_CallRepository_When_Called() {
        // Act, Assert


        mockUserInfoService.getByEmail("email");
        //Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .getByEmail("email");
    }

    @Test
    public void getAllCustomersCount_Should_CallRepository_When_Called() {
        //Act
        mockUserInfoService.getAllCustomersCount();

        //Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .getAllCustomersCount();
    }

    @Test
    public void getByName_Should_ThrowException_When_UserNotEmployee() {
        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserInfoService.getByName("any", mockUser));
    }

    @Test
    public void getByName_Should_CallRepository_When_UserEmployee() {
        //Arrange, Act
        mockUserInfoService.getByName("any", mockEmployee);

        //Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .getByName("any");
    }
//
//    @Test
//    public void getByResetToken_Should_ThrowException_When_UserNotAuthorized() {
//        //Arrange
//        Mockito.when(mockUserInfoRepository.getByToken("mockToken")).thenReturn(createMockUser());
//        mockUser.setUserID(404);
//        //Act, Assert
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> mockUserInfoService.getByResetToken("mockToken", mockUser));
//    }

    @Test
    public void getByResetToken_Should_CallRepository_When_UserAuthorized() {
        //Arrange
        Mockito.when(mockUserInfoRepository.getByToken("mockToken")).thenReturn(mockUser);

        //Act
        mockUserInfoService.getByResetToken("mockToken",mockEmployee);

        //Assert
        Mockito.verify(mockUserInfoRepository,Mockito.times(1))
                .getByToken("mockToken");
    }
}
