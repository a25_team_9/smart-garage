package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.ServiceStatus;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.repositories.contracts.CurrencyRepository;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceRepository;
import com.telerikacademy.mechanicum.repositories.contracts.UserInfoRepository;
import com.telerikacademy.mechanicum.repositories.contracts.VisitRepository;
import com.telerikacademy.mechanicum.utils.ExchangeRateService;
import com.telerikacademy.mechanicum.utils.contracts.EmailService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.mechanicum.ModelHelper.*;

@ExtendWith(MockitoExtension.class)
public class VisitServiceImplTests {

    @Mock
    VisitRepository mockVisitRepository;

    @Mock
    ServiceRepository mockServiceRepository;

    @Mock
    UserInfoRepository mockUserInfoRepository;

    @Mock
    EmailService mockEmailService;

    @Mock
    CurrencyRepository mockCurrencyRepository;

    @Mock
    ExchangeRateService mockExchangeRateService;

    @InjectMocks
    VisitServiceImpl mockVisitService;

    UserInfo mockEmployee;
    UserInfo mockUser;
    Service mockService;
    Visit mockVisit;
    ServiceStatus mockServiceStatus;

    @BeforeEach
    public void initialized() {
        mockEmployee = createMockEmployee();
        mockUser = createMockUser();
        mockVisit = createMockVisit();
        mockService = createMockService();
        mockServiceStatus = createMockServiceStatus();
    }

    @Test
    public void getAll_Should_CallRepository_When_Called() {
        //Arrange, Act
        mockVisitService.getAll(mockEmployee);

        //Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAll_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockVisitService.getAll(mockUser));
    }

    @Test
    public void getById_Should_CallRepository_When_Called() {

        UserInfo userInfo = createMockUser();
        userInfo.setUserID(13);
        Mockito.when(mockUserInfoRepository.getByID(1))
                .thenReturn(mockEmployee);

        mockVisitService.getById(mockUser.getUserID(), mockEmployee);

        //Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .getByID(mockEmployee.getUserID());
    }

    @Test
    public void getById_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        UserInfo userInfo = createMockUser();
        userInfo.setUserID(13);
        Mockito.when(mockUserInfoRepository.getByID(userInfo.getUserID()))
                .thenReturn(mockUser);
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockVisitService.getById(1, userInfo));
    }


    @Test
    public void create_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockVisitService.create(mockVisit, mockUser));
    }

    @Test
    public void create_Should_CallRepository_When_Called() {
        // Arrange

        mockVisitService.create(mockVisit, mockEmployee);

        //Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .create(mockVisit);
    }

    @Test
    public void update_Should_ThrowException_When_userIsNotEmployeeOrOwner() {
        // Act, Assert
        UserInfo userInfo = createMockUser();
        userInfo.setUserID(13);
        Mockito.when(mockVisitRepository.getByID(mockService.getVisit().getId()))
                .thenReturn(createMockVisit());
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockVisitService.update(mockVisit, userInfo));
    }

    @Test
    public void update_Should_CallRepository_When_Called() {
        // Arrange
        mockVisit.getServiceList().add(mockService);
        var mockNewCurrency = createMockCurrency();
        mockNewCurrency.setId(22);
        mockNewCurrency.setName("USD");
        mockVisit.setCurrency(mockNewCurrency);
        Mockito.when(mockVisitRepository.getByID(mockService.getVisit().getId()))
                .thenReturn(createMockVisit());
        Mockito.when(mockServiceRepository.checkVisitCompletionAllServices(mockVisit.getId()))
                .thenReturn(true);
        Mockito.when(mockExchangeRateService.getConverter("mockCurrency", mockVisit.getCurrency().getName()))
                .thenReturn(createMockConverter());

        // Act
        mockVisitService.update(mockVisit, mockEmployee);

        //Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .update(mockVisit);
    }

    @Test
    public void delete_Should_ThrowException_When_userIsNotEmployeeOrOwner() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockVisitService.delete(mockVisit.getId(), mockUser));
    }

    @Test
    public void delete_Should_ThrowException_When_VisitWithServices() {
        //Arrange
        mockVisit.getServiceList().add(mockService);
        Mockito.when(mockVisitRepository.getByID(mockVisit.getId()))
                .thenReturn(mockVisit);

        //Act, Assert
        Assertions.assertThrows(InvalidFieldException.class,
                ()-> mockVisitService.delete(mockVisit.getId(),mockEmployee));
    }

    @Test
    public void delete_Should_CallRepository_When_Called() {
        // Act, Assert
        Mockito.when(mockVisitRepository.getByID(mockVisit.getId()))
                .thenReturn(mockVisit);
        mockVisitService.delete(mockUser.getUserID(), mockEmployee);
        //Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .delete(mockVisit);
    }

    @Test
    public void getPayment_Should_ThrowException_When_userIsNotEmployeeOrOwner() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockVisitService.getPayment(mockVisit.getId(), mockUser));
    }

    @Test
    public void getPayment_Should_ThrowException_When_visitCompletionDateIsNull() {
        // Act, Assert
        mockVisit.setCompletionDate(null);
        Mockito.when(mockVisitRepository.getByID(mockVisit.getId()))
                .thenReturn(mockVisit);
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockVisitService.getPayment(mockVisit.getId(), mockEmployee));
    }

    @Test
    public void getPayment_Should_CallRepository_When_Called() {
        // Act, Assert
        mockVisit.setCompletionDate(LocalDate.now());
        Mockito.when(mockVisitRepository.getByID(mockVisit.getId()))
                .thenReturn(mockVisit);
        mockVisitService.getPayment(mockUser.getUserID(), mockEmployee);
        //Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .update(mockVisit);
    }


    @Test
    public void getLinkedServices_Should_ThrowException_When_userIsNotEmployeeOrOwner() {
        // Act, Assert
        UserInfo userInfo = createMockUser();
        userInfo.setUserID(13);
        Mockito.when(mockUserInfoRepository.getByID(1))
                .thenReturn(mockEmployee);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockVisitService.getLinkedServices(mockUser.getUserID(), userInfo, Optional.of(1), Optional.of("String")));
    }

    @Test
    public void getLinkedServicesShould_Should_CallRepository_When_Called() {
        // Act, Assert
        var userInfo = createMockUser();
        userInfo.setUserID(13);
        Mockito.when(mockUserInfoRepository.getByID(1))
                .thenReturn(mockEmployee);

        mockVisitService.getLinkedServices(mockUser.getUserID(), mockEmployee, Optional.of(1), Optional.of("String"));
        //Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .getCustomerLinkedServices(mockUser.getUserID(), Optional.of(1), Optional.of("String"));
    }

    @Test
    public void getCustomerVisits_Should_ThrowException_When_UserNotAuthorized() {
        //Arrange
        var userInfo = createMockUser();
        userInfo.setUserID(13);
        Mockito.when(mockUserInfoRepository.getByID(mockUser.getUserID()))
                .thenReturn(mockUser);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> mockVisitService.getCustomerVisits(mockUser.getUserID(),Optional.of(1),Optional.of("BGN"),userInfo));
    }

    @Test
    public void getCustomerVisits_Should_CallRepository_When_UserAuthorized() {
        //Arrange
        List<Visit> mockVisitList = new ArrayList<>();
        mockVisitList.add(mockVisit);
        var mockNewCurrency = createMockCurrency();
        mockNewCurrency.setId(404);
        mockNewCurrency.setName("BGN");
        Mockito.when(mockUserInfoRepository.getByID(mockUser.getUserID()))
                .thenReturn(mockUser);
        Mockito.when(mockVisitRepository.getCustomerVisits(mockUser.getUserID(),1))
                .thenReturn(mockVisitList);
        Mockito.when(mockExchangeRateService.getConverter(mockVisit.getCurrency().getName(), "BGN"))
                .thenReturn(createMockConverter());
        Mockito.when(mockCurrencyRepository.getByName(mockNewCurrency.getName()))
                .thenReturn(createMockCurrency());
        //Act
        mockVisitService.getCustomerVisits(mockUser.getUserID(),
                Optional.of(1),
                Optional.of("BGN"),
                mockEmployee);
        //Assert
        Mockito.verify(mockVisitRepository,Mockito.times(1))
                .getCustomerVisits(mockUser.getUserID(),1);
    }


    @Test
    public void getCustomerVisits_Should_UpdateServices_When_DifferentCurrencySend() {
        //Arrange
        List<Visit> mockVisitList = new ArrayList<>();
        mockVisitList.add(mockVisit);
        mockVisitList.get(0).getServiceList().add(mockService);
        var mockNewCurrency = createMockCurrency();
        mockNewCurrency.setId(2);
        mockNewCurrency.setName("GBP");
        Mockito.when(mockUserInfoRepository.getByID(mockUser.getUserID()))
                .thenReturn(mockUser);
        Mockito.when(mockVisitRepository.getCustomerVisits(mockUser.getUserID(),1))
                .thenReturn(mockVisitList);
        Mockito.when(mockExchangeRateService.getConverter(mockVisit.getCurrency().getName(), mockNewCurrency.getName()))
                .thenReturn(createMockConverter());
        Mockito.when(mockCurrencyRepository.getByName(mockNewCurrency.getName()))
                .thenReturn(mockNewCurrency);
        //Act
        mockVisitService.getCustomerVisits(mockUser.getUserID(),
                Optional.of(1),
                Optional.of("GBP"),
                mockEmployee);
        //Assert
        Mockito.verify(mockVisitRepository,Mockito.times(1))
                .getCustomerVisits(mockUser.getUserID(),1);
    }
}
