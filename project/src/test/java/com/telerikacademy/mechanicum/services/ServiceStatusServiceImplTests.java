package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.ServiceStatus;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceRepository;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceStatusRepository;
import com.telerikacademy.mechanicum.repositories.contracts.VisitRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.mechanicum.ModelHelper.*;

@ExtendWith(MockitoExtension.class)
public class ServiceStatusServiceImplTests {

    @Mock
    ServiceStatusRepository mockServiceStatusRepository;

    @InjectMocks
    ServiceStatusServiceImpl mockServiceStatusService;

    UserInfo mockEmployee;
    UserInfo mockUser;
    ServiceStatus mockServiceStatus;

    @BeforeEach
    public void initialized() {
        mockEmployee = createMockEmployee();
        mockUser = createMockUser();
        mockServiceStatus = createMockServiceStatus();
    }

    @Test
    public void getById_Should_CallRepository_When_Called() {
        //Arrange, Act
        mockServiceStatusService.getById(mockServiceStatus.getId(), mockEmployee);

        //Assert
        Mockito.verify(mockServiceStatusRepository, Mockito.times(1))
                .getById(mockServiceStatus.getId());
    }

    @Test
    public void getById_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockServiceStatusService.getById(mockServiceStatus.getId(), mockUser));
    }
}
