package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Manufacturer;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.repositories.contracts.ManufacturerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.mechanicum.ModelHelper.*;

@ExtendWith(MockitoExtension.class)
public class ManufacturerServiceImplTests {

    @Mock
    ManufacturerRepository mockRepository;

    @InjectMocks
    ManufacturerServiceImpl mockService;

    UserInfo mockEmployee;
    UserInfo mockUser;
    Manufacturer mockManufacturer;

    @BeforeEach
    public void initialized() {
        mockEmployee = createMockEmployee();
        mockUser = createMockUser();
        mockManufacturer = createMockManufacturer();
    }

    @Test
    public void getAll_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.getAll(mockUser));
    }

    @Test
    public void getAll_Should_CallRepository_When_UserIsEmployee() {
        // Act
        mockService.getAll(mockEmployee);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.getById(mockManufacturer.getId(), mockUser));
    }

    @Test
    public void getById_Should_CallRepository_When_UserIsEmployee() {
        // Act
        mockService.getById(Mockito.anyInt(), mockEmployee);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getById(Mockito.anyInt());
    }

    @Test
    public void create_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.create(mockManufacturer, mockUser));
    }

    @Test
    public void create_Should_ThrowException_When_MakeWithSameNameExists() {
        // Arrange
        Mockito.when(mockRepository.getByName(mockManufacturer.getName()))
                .thenReturn(mockManufacturer);
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> mockService.create(mockManufacturer, mockEmployee));
    }

    @Test
    public void create_Should_CallRepository_When_MakeWithSameNameDoesNotExists() {
        // Arrange
        Mockito.when(mockRepository.getByName(mockManufacturer.getName()))
                .thenThrow(new EntityNotFoundException("Manufacturer", "name", mockManufacturer.getName()));
        // Act
        mockService.create(mockManufacturer, mockEmployee);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(Mockito.any(Manufacturer.class));
    }

}
