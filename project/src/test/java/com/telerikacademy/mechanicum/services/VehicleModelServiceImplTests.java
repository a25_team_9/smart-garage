package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Manufacturer;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.VehicleModel;
import com.telerikacademy.mechanicum.repositories.contracts.VehicleModelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.mechanicum.ModelHelper.*;

@ExtendWith(MockitoExtension.class)
public class VehicleModelServiceImplTests {

    @Mock
    VehicleModelRepository mockRepository;

    @InjectMocks
    VehicleModelServiceImpl mockService;

    UserInfo mockEmployee;
    UserInfo mockUser;
    VehicleModel mockModel;

    @BeforeEach
    public void initialized() {
        mockEmployee = createMockEmployee();
        mockUser = createMockUser();
        mockModel = createMockModel();
    }

    @Test
    public void getAll_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.getAll(mockUser));
    }

    @Test
    public void getAll_Should_CallService_When_UserIsEmployee() {
        // Act
        mockService.getAll(mockEmployee);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.getById(mockModel.getId(), mockUser));
    }

    @Test
    public void getById_Should_CallService_When_UserIsEmployee() {
        // Act
        mockService.getById(Mockito.anyInt(), mockEmployee);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getById(Mockito.anyInt());
    }

    @Test
    public void create_Should_ThrowException_When_userIsNotEmployee() {
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockService.create(mockModel, mockUser));
    }

    @Test
    public void create_Should_ThrowException_When_MakeWithSameNameExists() {
        // Arrange
        Mockito.when(mockRepository.getByName(mockModel.getModel()))
                .thenReturn(mockModel);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> mockService.create(mockModel, mockEmployee));
    }

    @Test
    public void create_Should_CallRepository_When_MakeWithSameNameDoesNotExists() {
        // Arrange
        Mockito.when(mockRepository.getByName(mockModel.getModel()))
                .thenThrow(new EntityNotFoundException("Manufacturer", "name", mockModel.getModel()));

        // Act
        mockService.create(mockModel, mockEmployee);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(Mockito.any(VehicleModel.class));
    }

}
