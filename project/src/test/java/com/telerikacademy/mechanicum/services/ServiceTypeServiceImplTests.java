package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.ServiceType;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceRepository;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceTypeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.telerikacademy.mechanicum.ModelHelper.*;

@ExtendWith(MockitoExtension.class)
public class ServiceTypeServiceImplTests {

    @Mock
    ServiceTypeRepository mockServiceTypeRepository;

    @Mock
    ServiceRepository mockServiceRepository;

    @InjectMocks
    ServiceTypeServiceImpl mockServiceTypeService;

    UserInfo mockEmployee;
    UserInfo mockUser;
    ServiceType mockServiceType;

    @BeforeEach
    public void initialized() {
        mockEmployee = createMockEmployee();
        mockUser = createMockUser();
        mockServiceType = createMockServiceType();
    }

    @Test
    public void getAll_Should_CallRepository_When_Called() {
        //Arrange, Act
        mockServiceTypeService.getAll();

        //Assert
        Mockito.verify(mockServiceTypeRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_CallRepository_When_Called() {
        //Arrange, Act
        mockServiceTypeService.getById(mockServiceType.getId());

        //Assert
        Mockito.verify(mockServiceTypeRepository, Mockito.times(1)).getById(mockServiceType.getId());
    }

    @Test
    public void create_Should_ThrowException_When_UserNotEmployee() {
        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockServiceTypeService.create(mockServiceType, mockUser));
    }

    @Test
    public void create_Should_ThrowException_When_ServiceTypeNameNotUnique() {
        //Arrange
        Mockito.when(mockServiceTypeRepository.getByName(mockServiceType.getName()))
                .thenReturn(mockServiceType);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockServiceTypeService.create(mockServiceType, mockEmployee));
    }

    @Test
    public void create_Should_CallRepository_When_ServiceTypeNameUnique() {
        //Arrange
        Mockito.when(mockServiceTypeRepository.getByName(mockServiceType.getName()))
                .thenThrow(new EntityNotFoundException("Service Type", "name", mockServiceType.getName()));

        //Act
        mockServiceTypeService.create(mockServiceType, mockEmployee);

        //Assert
        Mockito.verify(mockServiceTypeRepository, Mockito.times(1))
                .create(mockServiceType);
    }
    
    @Test
    public void update_Should_ThrowException_When_UserNotEmployee() {
        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockServiceTypeService.update(mockServiceType, mockUser));
    }

    @Test
    public void update_Should_CallRepository_When_UserEmployee() {
        //Arrange, Act
        mockServiceTypeService.update(mockServiceType,mockEmployee);

        //Assert
        Mockito.verify(mockServiceTypeRepository,Mockito.times(1))
                .update(mockServiceType);
    }

//    @Test
//    public void filter_Should_ThrowException_When_UserNotEmployee() {
//        //Arrange
//        Map<String, String> mockFilterMap = new HashMap<>();
//
//        //Act, Assert
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> mockServiceTypeService.filter(mockFilterMap));
//    }

    @Test
    public void getByName_Should_CallRepository_When_Called() {
        //Arrange,Act
        mockServiceTypeService.getByName(mockServiceType.getName());

        //Assert
        Mockito.verify(mockServiceTypeRepository,Mockito.times(1))
                .getByName(mockServiceType.getName());
    }

    @Test
    public void filter_Should_CallRepository_When_UserEmployee() {
        //Arrange
        Map<String, String> mockFilterMap = new HashMap<>();

        //Act
        mockServiceTypeService.filter(mockFilterMap);

        //Assert
        Mockito.verify(mockServiceTypeRepository,Mockito.times(1))
                .filter(mockFilterMap);
    }

    @Test
    public void delete_Should_ThrowException_When_UserNotEmployee() {
        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockServiceTypeService.delete(mockServiceType.getId(), mockUser));
    }

    @Test
    public void delete_Should_ThrowException_When_ServiceTypeUsed() {
        //Arrange
        Mockito.when(mockServiceTypeRepository.getById(mockServiceType.getId()))
                .thenReturn(mockServiceType);
        Mockito.when(mockServiceRepository.getAll()).thenReturn(List.of(createMockService()));

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> mockServiceTypeService.delete(mockServiceType.getId(),mockEmployee));
    }

    @Test
    public void delete_Should_CallRepository_When_ServiceTypeNotUsed() {
        //Arrange
        mockServiceType.setName("mockName2");
        Mockito.when(mockServiceTypeRepository.getById(mockServiceType.getId()))
                .thenReturn(mockServiceType);
        Mockito.when(mockServiceRepository.getAll()).thenReturn(List.of(createMockService()));

        //Act
        mockServiceTypeService.delete(mockServiceType.getId(),mockEmployee);

        //Assert
        Mockito.verify(mockServiceTypeRepository,Mockito.times(1))
                .delete(mockServiceType);
    }
}
