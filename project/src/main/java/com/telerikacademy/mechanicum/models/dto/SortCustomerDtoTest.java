package com.telerikacademy.mechanicum.models.dto;

import javax.persistence.*;

public class SortCustomerDtoTest {
    private String date;
    private String firstName;

    public SortCustomerDtoTest() {
    }

    public SortCustomerDtoTest(String date, String firstName) {
        this.date = date;
        this.firstName = firstName;
    }

    public String getDate() {
        return date;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
