package com.telerikacademy.mechanicum.models;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "services")
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_id")
    private int id;

    @Column(name = "start_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate startDate;

    @Column(name = "end_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate endDate;

    @Column(name = "quantity")
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private ServiceStatus serviceStatus;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private ServiceType serviceType;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "visit_id")
    private Visit visit;

    public Service() {
    }

    public Service(int id,
                   LocalDate startDate,
                   LocalDate endDate,
                   int quantity,
                   ServiceStatus serviceStatus,
                   ServiceType serviceType,
                   Visit visit) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.quantity = quantity;
        this.serviceStatus = serviceStatus;
        this.serviceType = serviceType;
        this.visit = visit;
    }

    public int getId() {
        return id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public int getQuantity() {
        return quantity;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public Visit getVisit() {
        return visit;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setServiceStatus(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public void setVisit(Visit visit) {
        this.visit = visit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Service service = (Service) o;
        return getId() == service.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
