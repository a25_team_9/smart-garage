package com.telerikacademy.mechanicum.models.converter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultFromAndTo {
    private double price_usd;
    private String code;
    private String name;
    private String updated;

    public ResultFromAndTo() {
    }

    public ResultFromAndTo(double price_usd, String code, String name, String updated) {
        this.price_usd = price_usd;
        this.code = code;
        this.name = name;
        this.updated = updated;
    }

    public double getPrice_usd() {
        return price_usd;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getUpdated() {
        return updated;
    }

    public void setPrice_usd(double price_usd) {
        this.price_usd = price_usd;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }
}
