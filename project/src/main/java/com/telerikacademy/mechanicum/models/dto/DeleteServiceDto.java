package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.Positive;

public class DeleteServiceDto {
    @Positive
    private Integer id;

    public DeleteServiceDto() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
