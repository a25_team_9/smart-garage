package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.Positive;

public class VehicleSearchParameters {

    @Positive
    private Integer ownerId;

    public VehicleSearchParameters() {
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }
}
