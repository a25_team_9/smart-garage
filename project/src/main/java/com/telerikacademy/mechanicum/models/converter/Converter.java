package com.telerikacademy.mechanicum.models.converter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Converter {
    private boolean success;
    private Result result;

    public Converter() {
    }

    public Converter(boolean success, Result result) {
        this.success = success;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public Result getResult() {
        return result;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public double getConvertionRate() {
        return result.getResult().getValue();
    }
}
