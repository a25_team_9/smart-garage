package com.telerikacademy.mechanicum.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "visits")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "visit_id")
    private int id;

    @Column(name = "arrival_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate arrivalDate;

    @Column(name = "completion_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate completionDate;

    @Column(name = "paid")
    private boolean paid;

    @Column(name = "total_price")
    private Double totalPrice;

    @ManyToOne
    @JoinColumn(name = "currency_id")
    private Currency currency;

    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "visit")
    private List<Service> serviceList;

    public Visit() {
    }

    public Visit(int id,
                 LocalDate arrivalDate,
                 LocalDate completionDate,
                 boolean paid,
                 Double totalPrice,
                 Currency currency,
                 Vehicle vehicle,
                 List<Service> serviceList) {
        this.id = id;
        this.arrivalDate = arrivalDate;
        this.completionDate = completionDate;
        this.paid = paid;
        this.totalPrice = totalPrice;
        this.currency = currency;
        this.vehicle = vehicle;
        this.serviceList = serviceList;
    }

    public int getId() {
        return id;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public LocalDate getCompletionDate() {
        return completionDate;
    }

    public boolean isPaid() {
        return paid;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public List<Service> getServiceList() {
        return serviceList;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public void setCompletionDate(LocalDate completionDate) {
        this.completionDate = completionDate;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Visit visit = (Visit) o;
        return getId() == visit.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

