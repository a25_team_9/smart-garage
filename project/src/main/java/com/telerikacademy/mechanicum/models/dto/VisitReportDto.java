package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class VisitReportDto {
    @NotNull
    private String currency;
    @Positive
    private Integer numberOfVisits;

    public VisitReportDto() {
    }

    public VisitReportDto(String currency, Integer numberOfVisits) {
        this.currency = currency;
        this.numberOfVisits = numberOfVisits;
    }

    public String getCurrency() {
        return currency;
    }

    public Integer getNumberOfVisits() {
        return numberOfVisits;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setNumberOfVisits(Integer numberOfVisits) {
        this.numberOfVisits = numberOfVisits;
    }
}
