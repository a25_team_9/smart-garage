package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.*;

public class VehicleDto {

    @Positive(message = "Model should be positive number")
    private int modelId;

    @Min(value = 2000, message = "We don't do service to car lower than 2000 year")
    @Max(value = 2021, message = "Year should not be greater than 2021")
    private int year;

    @NotBlank(message = "Car must have VIN that is not empty")
    @Size(min = 17, max = 17, message = "Car VIN must be exactly 17 characters long.")
    private String vin;

    @NotBlank
    @Pattern(regexp = "^[A-Z]{1,2}[0-9]{4}[A-Z]{2}", message = "Licence plate not valid in BG")
    private String licencePlate;

    @Positive(message = "Owner should be positive number")
    private int ownerId;

    private String vehicleUrl;

    public VehicleDto() {
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public String getVehicleUrl() {
        return vehicleUrl;
    }

    public void setVehicleUrl(String vehicleUrl) {
        this.vehicleUrl = vehicleUrl;
    }
}
