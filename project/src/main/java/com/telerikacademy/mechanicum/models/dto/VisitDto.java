package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.Positive;

public class VisitDto {

    private boolean paid;

    @Positive
    private int vehicleId;

    @Positive
    private int currencyId;

    public VisitDto() {
    }

    public VisitDto(boolean paid, int vehicleId, int currencyId) {
        this.paid = paid;
        this.vehicleId = vehicleId;
        this.currencyId = currencyId;
    }

    public boolean isPaid() {
        return paid;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }
}
