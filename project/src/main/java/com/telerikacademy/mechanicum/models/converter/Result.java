package com.telerikacademy.mechanicum.models.converter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
    private ResultFromAndTo from;
    private ResultFromAndTo to;
    private ResultRate result;

    public Result() {
    }

    public Result(ResultFromAndTo from, ResultFromAndTo to, ResultRate result) {
        this.from = from;
        this.to = to;
        this.result = result;
    }

    public ResultFromAndTo getFrom() {
        return from;
    }

    public ResultFromAndTo getTo() {
        return to;
    }

    public ResultRate getResult() {
        return result;
    }

    public void setFrom(ResultFromAndTo from) {
        this.from = from;
    }

    public void setTo(ResultFromAndTo to) {
        this.to = to;
    }

    public void setResult(ResultRate result) {
        this.result = result;
    }
}
