package com.telerikacademy.mechanicum.models.converter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultRate {

    private double value;
    private String format;

    public ResultRate() {
    }

    public ResultRate(double value, String format) {
        this.value = value;
        this.format = format;
    }

    public double getValue() {
        return value;
    }

    public String getFormat() {
        return format;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setFormat(String format) {
        this.format = format;
    }
}
