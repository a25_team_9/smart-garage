package com.telerikacademy.mechanicum.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;


@Entity
@Table(name = "user_info")
public class UserInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer userID;

    @OneToOne
    @JoinColumn(name = "email")
    private UserAuthorization userAuthorization;

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "phone")
    private String phone;
    @Column(name = "address")
    private String address;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner")
    Set<Vehicle> vehicleList;

    public UserInfo() {
    }

    public UserInfo(Integer userID,
                    UserAuthorization userAuthorization,
                    String firstName,
                    String lastName,
                    String phone,
                    String address,
                    Set<Vehicle> vehicleList) {
        this.userID = userID;
        this.userAuthorization = userAuthorization;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.address = address;
        this.vehicleList = vehicleList;
    }

    public Integer getUserID() {
        return userID;
    }

    public UserAuthorization getUserAuthorization() {
        return userAuthorization;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public Set<Vehicle> getVehicleList() {
        return vehicleList;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public void setUserAuthorization(UserAuthorization userAuthorization) {
        this.userAuthorization = userAuthorization;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setVehicleList(Set<Vehicle> vehicleList) {
        this.vehicleList = vehicleList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInfo userInfo = (UserInfo) o;
        return Objects.equals(userID, userInfo.userID) && Objects.equals(userAuthorization, userInfo.userAuthorization);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userID, userAuthorization);
    }
}
