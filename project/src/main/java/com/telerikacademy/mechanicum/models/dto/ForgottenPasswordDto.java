package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.Email;

public class ForgottenPasswordDto {
    @Email
    private String email;

    public ForgottenPasswordDto() {
    }

    public ForgottenPasswordDto(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
