package com.telerikacademy.mechanicum.models.dto;


public class UserInfoFilterDto {

    private String email;
    private String firstName;
    private String phone;
    private String modelId;
    private String makeId;

    private String from;
    private String to;

    public UserInfoFilterDto() {
    }

    public UserInfoFilterDto(String email, String firstName, String phone, String modelId, String makeId, String from, String to) {
        this.email = email;
        this.firstName = firstName;
        this.phone = phone;
        this.modelId = modelId;
        this.makeId = makeId;
        this.from = from;
        this.to = to;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPhone() {
        return phone;
    }

    public String getModelId() {
        return modelId;
    }

    public String getMakeId() {
        return makeId;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public void setMakeId(String makeId) {
        this.makeId = makeId;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
