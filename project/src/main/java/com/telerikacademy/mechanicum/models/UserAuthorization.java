package com.telerikacademy.mechanicum.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users_authorization")
public class UserAuthorization {

    @Id
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "email"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    Set<Role> roles;
    @Column(name = "reset_token")
    private String resetToken;

    public UserAuthorization() {
    }

    public UserAuthorization(String email,
                             String password,
                             Set<Role> roles) {
        this.email = email;
        this.password = password;
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    @JsonIgnore
    public String getResetToken() {
        return resetToken;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    @JsonIgnore
    public boolean isEmployee() {
        return roles.stream().anyMatch(r -> r.getName().equalsIgnoreCase("Employee"));
    }

    @JsonIgnore
    public boolean isCustomer() {
        return roles.stream().anyMatch(r -> r.getName().equals("User"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAuthorization that = (UserAuthorization) o;
        return email.equals(that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}
