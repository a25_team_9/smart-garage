package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.Size;

public class ManufacturerDto {

    @Size(min = 3, max = 15, message = "Make name must be between 3 and 15 symbols")
    private String name;

    public ManufacturerDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
