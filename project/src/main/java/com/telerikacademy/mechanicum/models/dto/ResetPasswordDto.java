package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.Size;

public class ResetPasswordDto {

    @Size(min = 8)
    private String password;

    @Size(min = 8)
    private String repeatPassword;

    public ResetPasswordDto() {
    }

    public ResetPasswordDto(String password, String repeatPassword) {
        this.password = password;
        this.repeatPassword = repeatPassword;
    }

    public String getPassword() {
        return password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }
}
