package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ServiceTypeDto {

    @NotNull
    @Size(min = 2, max = 40, message = "Service type name must be between 2 and 40 symbols long")
    private String name;

    @Positive
    private double singlePrice;

    public ServiceTypeDto() {
    }

    public String getName() {
        return name;
    }

    public double getSinglePrice() {
        return singlePrice;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSinglePrice(double singlePrice) {
        this.singlePrice = singlePrice;
    }
}
