package com.telerikacademy.mechanicum.models.dto;


public class ServiceTypeFilterDto {

    private String name;
    private String singlePrice;

    public ServiceTypeFilterDto() {
    }

    public ServiceTypeFilterDto(String name, String singlePrice) {
        this.name = name;
        this.singlePrice = singlePrice;
    }

    public String getName() {
        return name;
    }

    public String getSinglePrice() {
        return singlePrice;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSinglePrice(String singlePrice) {
        this.singlePrice = singlePrice;
    }
}
