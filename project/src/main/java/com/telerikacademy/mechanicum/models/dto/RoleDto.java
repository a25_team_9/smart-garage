package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.Positive;
import java.util.Objects;

public class RoleDto {
    @Positive
    private int roleId;

    public RoleDto() {
    }

    public RoleDto(int roleId) {
        this.roleId = roleId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleDto roleDto = (RoleDto) o;
        return roleId == roleDto.roleId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId);
    }
}
