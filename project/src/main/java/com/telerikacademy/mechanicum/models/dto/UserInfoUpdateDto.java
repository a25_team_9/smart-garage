package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.NotNull;

public class UserInfoUpdateDto {

    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private String password;

    @NotNull
    private String repeatPassword;

    @NotNull
    private String phone;
    //optional
    private String address;


    public UserInfoUpdateDto() {
    }

    public UserInfoUpdateDto(String firstName, String lastName, String password, String repeatPassword, String phone, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.repeatPassword = repeatPassword;
        this.phone = phone;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
