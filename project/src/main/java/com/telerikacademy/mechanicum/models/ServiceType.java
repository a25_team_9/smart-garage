package com.telerikacademy.mechanicum.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "service_types")
public class ServiceType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "single_price", precision = 2)
    private double singlePrice;

    public ServiceType() {
    }

    public ServiceType(int id,
                       String name,
                       double singlePrice) {
        this.id = id;
        this.name = name;
        this.singlePrice = singlePrice;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getSinglePrice() {
        return singlePrice;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSinglePrice(double singlePrice) {
        this.singlePrice = singlePrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceType that = (ServiceType) o;
        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
