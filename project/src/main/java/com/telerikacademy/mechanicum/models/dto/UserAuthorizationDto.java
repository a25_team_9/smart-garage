package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserAuthorizationDto {

    @Email
    private String email;

    @Size(min = 8)
    private String password;

    public UserAuthorizationDto() {
    }

    public UserAuthorizationDto(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
