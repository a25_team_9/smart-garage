package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class VehicleModelDto {

    @NotNull
    private String model;

    @Positive
    private int manufacturerId;

    public VehicleModelDto() {
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }
}
