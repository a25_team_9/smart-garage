package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

public class UserInfoDto {

    @NotNull
    private UserAuthorizationDto userAuthorizationDto;
    @NotNull
    @Size(min = 2, max = 20)
    private String firstName;
    @NotNull
    @Size(min = 2, max = 20)
    private String lastName;
    @NotNull
    @Size(min = 10, max = 10)
    private String phone;
    //optional
    private String address;
    @NotNull
    private Set<RoleDto> rolesDto;

    public UserInfoDto() {
        this.userAuthorizationDto = new UserAuthorizationDto();
        this.rolesDto = new HashSet<>();
    }

    public UserAuthorizationDto getUserAuthorizationDto() {
        return userAuthorizationDto;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public Set<RoleDto> getRolesDto() {
        return rolesDto;
    }

    public void setUserAuthorizationDto(UserAuthorizationDto userAuthorizationDto) {
        this.userAuthorizationDto = userAuthorizationDto;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setRolesDto(Set<RoleDto> rolesDto) {
        this.rolesDto = rolesDto;
    }
}
