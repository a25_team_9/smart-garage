package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.Positive;

public class VisitUpdateDto {

    private Boolean paid;

    @Positive
    private int currencyId;

    public VisitUpdateDto() {
    }

    public VisitUpdateDto(boolean paid, int currencyId) {
        this.paid = paid;
        this.currencyId = currencyId;
    }

    public Boolean isPaid() {
        return paid;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }
}
