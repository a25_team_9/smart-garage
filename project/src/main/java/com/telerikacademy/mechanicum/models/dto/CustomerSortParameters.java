package com.telerikacademy.mechanicum.models.dto;


public class CustomerSortParameters {

    private String name;

    private String visitDate;

    public CustomerSortParameters() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }
}
