package com.telerikacademy.mechanicum.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

public class ServiceDto {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, fallbackPatterns = "dd-MM-yyyy")
    @FutureOrPresent(message = "Start date must be a future date")
    private LocalDate startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, fallbackPatterns = "dd-MM-yyyy")
    @FutureOrPresent(message = "End date must be a future date")
    private LocalDate endDate;

    @Positive
    private int quantity;

    @Positive
    private int serviceTypeId;

    @Positive
    private int visitId;

    public ServiceDto() {
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getServiceTypeId() {
        return serviceTypeId;
    }

    public int getVisitId() {
        return visitId;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setServiceTypeId(int serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public void setVisitId(int visitId) {
        this.visitId = visitId;
    }
}
