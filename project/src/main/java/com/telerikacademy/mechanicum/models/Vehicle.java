package com.telerikacademy.mechanicum.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "vehicles")
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vehicle_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "model_id")
    private VehicleModel model;

    @Column(name = "year")
    private int year;

    @Column(name = "vin")
    private String vin;

    @Column(name = "licence_plate")
    private String licencePlate;

    @ManyToOne
    @JoinColumn(name = "owner")
    private UserInfo owner;

    @JsonIgnore
    @Column(name = "vehicle_url")
    private String vehicleUrl;

    public Vehicle() {
    }

    public Vehicle(int id,
                   VehicleModel model,
                   int year,
                   String vin,
                   String licencePlate,
                   UserInfo owner,
                   String vehicleUrl) {
        this.id = id;
        this.model = model;
        this.year = year;
        this.vin = vin;
        this.licencePlate = licencePlate;
        this.owner = owner;
        this.vehicleUrl = vehicleUrl;
    }

    public int getId() {
        return id;
    }

    public VehicleModel getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public String getVin() {
        return vin;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public UserInfo getOwner() {
        return owner;
    }

    public String getVehicleUrl() {
        return vehicleUrl;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setModel(VehicleModel model) {
        this.model = model;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public void setOwner(UserInfo owner) {
        this.owner = owner;
    }

    public void setVehicleUrl(String vehicleUrl) {
        this.vehicleUrl = vehicleUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle)) return false;
        Vehicle vehicle = (Vehicle) o;
        return getId() == vehicle.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
