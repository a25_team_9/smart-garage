package com.telerikacademy.mechanicum.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserInfoMvcCreateDto {
    @Email
    String email;
    @NotNull
    @Size(min = 2, max = 20)
    private String firstName;
    @NotNull
    @Size(min = 2, max = 20)
    private String lastName;
    @NotNull
    @Size(min = 10, max = 10)
    private String phone;
    //optional
    private String address;

    private boolean isEmployee;
    private boolean isCustomer;

    public UserInfoMvcCreateDto() {
    }

    public UserInfoMvcCreateDto(String email, String firstName, String lastName, String phone, String address, boolean isEmployee, boolean isCustomer) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.address = address;
        this.isEmployee = isEmployee;
        this.isCustomer = isCustomer;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public boolean isEmployee() {
        return isEmployee;
    }

    public boolean isCustomer() {
        return isCustomer;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmployee(boolean employee) {
        isEmployee = employee;
    }

    public void setCustomer(boolean customer) {
        isCustomer = customer;
    }
}
