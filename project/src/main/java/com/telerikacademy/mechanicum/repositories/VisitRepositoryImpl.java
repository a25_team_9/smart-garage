package com.telerikacademy.mechanicum.repositories;

import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.repositories.contracts.VisitRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class VisitRepositoryImpl implements VisitRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public VisitRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Visit> getAll() {
        try (Session session = this.sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery("from Visit ", Visit.class);
            return query.getResultList();
        }
    }

    @Override
    public List<Visit> getCustomerVisits(int customerId,
                                         int numberOfVisits) {
        Query<Visit> queryVisits;
        try (Session session = this.sessionFactory.openSession()) {
            queryVisits = session.createQuery(
                    "from Visit v where v.vehicle.owner.id = :customerId order by v.arrivalDate desc",
                    Visit.class);
            queryVisits.setParameter("customerId", customerId);
            queryVisits.setMaxResults(numberOfVisits);
            return queryVisits.getResultList();
        }
    }

    @Override
    public Visit getByID(int id) {
        try (Session session = this.sessionFactory.openSession()) {
            Visit visit = session.get(Visit.class, id);
            if (visit == null) {
                throw new EntityNotFoundException(String.format("Visit with id %d was not found", id));
            }
            return visit;
        }
    }

    @Override
    public Visit create(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(visit);
            session.getTransaction().commit();
            return visit;
        }
    }


    @Override
    public Visit update(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(visit);
            session.getTransaction().commit();
            return visit;
        }
    }

    @Override
    public void delete(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(visit);
            session.getTransaction().commit();
        }
    }


    @Override
    public List<Visit> getCustomerLinkedServices(int customerID, Optional<Integer> vehicleId, Optional<String> date) {
        try (Session session = sessionFactory.openSession()) {
            String finalResult = generateQuery(customerID, vehicleId, date);
            Query<Visit> query = session.createQuery(finalResult, Visit.class);
            List<Visit> queryResultList = query.getResultList();
            if (queryResultList.isEmpty()) {
                throw new EntityNotFoundException("No visits matching those parameters in our app");
            }
            return queryResultList;
        }
    }

    private String generateQuery(int customerID, Optional<Integer> vehicleId, Optional<String> date) {
        List<String> myQuery = new ArrayList<>();

        vehicleId.ifPresent(integer -> myQuery.add(String.format("and v.vehicle.id = '%d' ", integer)));
        try {
            date.ifPresent(localDate -> myQuery.add(String.format("and v.arrivalDate = '%s' ",
                    LocalDate.parse(localDate, DateTimeFormatter.ofPattern("dd-MM-yyyy")))));
        } catch (DateTimeParseException ex) {
            throw new InvalidFieldException("Please specify date in format dd-MM-yyyy");
        }
        String result = String.format("from Visit v where v.vehicle.owner.id = %d ", customerID);
        myQuery.add(0, result);
        StringBuilder finalResult = new StringBuilder();
        for (String param : myQuery) {
            finalResult.append(param);
        }
        return finalResult.toString();
    }
}
