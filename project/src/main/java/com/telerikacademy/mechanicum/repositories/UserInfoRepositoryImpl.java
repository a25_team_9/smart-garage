package com.telerikacademy.mechanicum.repositories;

import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.models.UserAuthorization;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.repositories.contracts.RoleRepository;
import com.telerikacademy.mechanicum.repositories.contracts.UserInfoRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class UserInfoRepositoryImpl implements UserInfoRepository {
    private final SessionFactory sessionFactory;
    private final RoleRepository roleRepository;

    @Autowired
    public UserInfoRepositoryImpl(SessionFactory sessionFactory, RoleRepository roleRepository) {
        this.sessionFactory = sessionFactory;
        this.roleRepository = roleRepository;
    }

    @Override
    public List<UserInfo> getAll() {
        try (Session session = this.sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery("from UserInfo ", UserInfo.class);
            return query.getResultList();
        }
    }

    @Override
    public List<UserInfo> getAllCustomers() {
        try (Session session = this.sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery("from UserInfo u where :role member of u.userAuthorization.roles", UserInfo.class);
            query.setParameter("role", roleRepository.getByID(1));
            return query.getResultList();
        }
    }

    @Override
    public UserInfo getByID(int id) {
        try (Session session = this.sessionFactory.openSession()) {
            UserInfo customer = session.get(UserInfo.class, id);
            if (customer == null) {
                throw new EntityNotFoundException(String.format("Customer with id %d was not found", id));
            }
            return customer;
        }
    }

    @Override
    public UserInfo create(UserInfo userInfo) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(userInfo.getUserAuthorization());
            session.save(userInfo);

            session.getTransaction().commit();
            return userInfo;
        }
    }

    @Override
    public UserInfo update(UserInfo userInfo) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userInfo.getUserAuthorization());
            session.update(userInfo);
            session.getTransaction().commit();
            return userInfo;
        }
    }

    @Override
    public void delete(UserInfo userInfo) {
        UserAuthorization userAuthorization = userInfo.getUserAuthorization();
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userInfo);
            session.delete(userAuthorization);
            session.getTransaction().commit();
        }
    }

    @Override
    public UserInfo getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery(
                    "from UserInfo u where u.userAuthorization.email like :email", UserInfo.class);
            query.setParameter("email", email);
            List<UserInfo> userInfoList = query.getResultList();
            if (userInfoList.isEmpty()) {
                throw new EntityNotFoundException(String.format("Customer with this email not found %s", email));
            }
            return userInfoList.get(0);
        }
    }

    @Override
    public List<UserInfo> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery(
                    "from UserInfo u where u.firstName like :name " +
                            "or u.lastName like :name " +
                            "or u.userAuthorization.email like :name order by u.firstName desc", UserInfo.class);
            query.setParameter("name", String.format("'%%%s%%'", name));
            return query.getResultList();
        }
    }

    @Override
    public UserInfo getByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery(
                    "from UserInfo u where u.userAuthorization.resetToken like :token ", UserInfo.class);
            query.setParameter("token", token);
            List<UserInfo> userInfoList = query.getResultList();
            if (userInfoList.isEmpty()) {
                throw new EntityNotFoundException("Token does not exist or expired.");
            }
            return userInfoList.get(0);
        }
    }

    @Override
    public List<Visit> filter(Map<String, String> filterMap) {
        String baseQuery = fromMapToQuery(filterMap);

        try (Session session = this.sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery(baseQuery, Visit.class);
            List<Visit> queryResultList = query.getResultList();
            if (queryResultList.isEmpty()) {
                throw new EntityNotFoundException("No customers matching those parameters in our app");
            }
            return queryResultList;
        }
    }

    @Override
    public List<Visit> sort(Map<String, String> parameters) {
        String baseQuery = "from Visit v ";

        boolean isValidKeys = parameters.keySet()
                .stream()
                .noneMatch(k -> k.equalsIgnoreCase("name") || k.equalsIgnoreCase("visitDate"));

        if (isValidKeys) {
            throw new InvalidFieldException("Please sort by 'name' or 'visitDate'");
        }

        if (parameters.isEmpty() || parameters.size() == 2) {
            throw new InvalidFieldException("You can sort only by one parameter(firstName or Date)");
        }
        if (parameters.get("name") != null) {
            baseQuery = baseQuery + String.format(" order by v.vehicle.owner.firstName %s ", parameters.get("name"));
        }
        if (parameters.get("visitDate") != null) {
            baseQuery = baseQuery + String.format(" order by v.arrivalDate %s ", parameters.get("visitDate"));
        }

        try (Session session = this.sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery(baseQuery, Visit.class);
            return query.getResultList();
        }
    }

    @Override
    public int getAllCustomersCount() {
        return getAllCustomers().size();
    }

    private String fromMapToQuery(Map<String, String> filterMap) {

        String email = filterMap.getOrDefault("email", "");
        String firstName = filterMap.getOrDefault("firstName", "");
        String phone = filterMap.getOrDefault("phone", "");
        String model = filterMap.getOrDefault("model", "");
        String make = filterMap.getOrDefault("make", "");
        String from = filterMap.getOrDefault("from", "");
        String to = filterMap.getOrDefault("to", "");

        if (!model.isBlank()) {
            try {
                Integer.parseInt(model);
            } catch (NumberFormatException e) {
                throw new InvalidFieldException("Model must be number - id");
            }
        }
        if (!make.isBlank()) {
            try {
                Integer.parseInt(make);
            } catch (NumberFormatException e) {
                throw new InvalidFieldException("Manufacturer must be number - id");
            }
        }

        List<String> myQuery = new ArrayList<>();

        if (!email.isEmpty()) {
            myQuery.add(String.format(" v.vehicle.owner.userAuthorization.email = '%s' ", email));
        }
        if (!firstName.isEmpty()) {
            myQuery.add(String.format(" v.vehicle.owner.firstName like '%s' ", firstName));
        }
        if (!phone.isEmpty()) {
            myQuery.add(String.format(" v.vehicle.owner.phone like '%s' ", phone));
        }
        if (!model.isEmpty()) {
            myQuery.add(String.format(" v.vehicle.model.id = '%s' ", model));
        }
        if (!make.isEmpty()) {
            myQuery.add(String.format(" v.vehicle.model.manufacturer.id = '%s' ", make));
        }
        //This will filter only by visit arrival date
        if (myQuery.isEmpty()) {
            if (!from.isEmpty() || !to.isEmpty()) {
                if (!from.isEmpty() && !to.isEmpty()) {
                    try {
                        return String.format(" from Visit v  where v.arrivalDate between '%s' and '%s' ", LocalDate.parse(from, DateTimeFormatter.ofPattern("dd-MM-yyyy"))
                                , LocalDate.parse(to, DateTimeFormatter.ofPattern("dd-MM-yyyy")));
                    } catch (DateTimeParseException ex) {
                        throw new InvalidFieldException("Please specify date in format dd-MM-yyyy");
                    }
                }
            } else {
                throw new InvalidFieldException("Make sure that both FROM and TO are specified !");
            }
            return " from Visit v ";
        }
        if (!from.isEmpty() || !to.isEmpty()) {
            if (!from.isEmpty() && !to.isEmpty()) {
                myQuery.add(0, " from Visit v where ( ");
                for (int i = 1; i < myQuery.size() - 1; ++i) {
                    myQuery.set(i, myQuery.get(i) + " or ");
                }
                try {
                    myQuery.add(String.format(") and v.arrivalDate between '%s' and '%s' ", LocalDate.parse(from, DateTimeFormatter.ofPattern("dd-MM-yyyy"))
                            , LocalDate.parse(to, DateTimeFormatter.ofPattern("dd-MM-yyyy"))));
                } catch (DateTimeParseException ex) {
                    throw new InvalidFieldException("Please specify date in format dd-MM-yyyy");
                }
            } else {
                throw new InvalidFieldException("Make sure that both FROM and TO are specified !");
            }
        } else {
            //this one will be done only if we do not have specified date period !!
            myQuery.add(0, " from Visit v where ");
            if (myQuery.size() > 1) {
                for (int i = 1; i < myQuery.size() - 1; ++i) {
                    myQuery.set(i, myQuery.get(i) + " or ");
                }
            }
        }

        StringBuilder finalResult = new StringBuilder();
        for (String param : myQuery) {
            finalResult.append(param);
        }
        return finalResult.toString();
    }

}
