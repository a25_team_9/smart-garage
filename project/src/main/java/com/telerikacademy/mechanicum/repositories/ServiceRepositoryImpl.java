package com.telerikacademy.mechanicum.repositories;

import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ServiceRepositoryImpl implements ServiceRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ServiceRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Service> getAll() {
        try (Session session = this.sessionFactory.openSession()) {
            Query<Service> query = session.createQuery("from Service ", Service.class);
            return query.getResultList();
        }
    }

    @Override
    public Service getById(int serviceId) {
        try (Session session = this.sessionFactory.openSession()) {
            Service service = session.get(Service.class, serviceId);
            if (service == null) {
                throw new EntityNotFoundException("Service", "id", String.valueOf(serviceId));
            }
            return service;
        }
    }

    @Override
    public Service create(Service service) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(service);
            session.getTransaction().commit();
        }
        return service;
    }

    @Override
    public Service update(Service service) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(service);
            session.getTransaction().commit();
        }
        return service;
    }

    @Override
    public void delete(Service service) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(service);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean checkVisitCompletionAllServices(int visitId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Service> query = session.createQuery(
                    "from Service s where s.visit.id = :visitId and s.serviceStatus.id != 3",
                    Service.class);
            query.setParameter("visitId", visitId);
            List<Service> result = query.getResultList();
            return result.isEmpty();
        }
    }

    @Override
    public List<Service> getAllNotFinishedServices() {
        try (Session session = sessionFactory.openSession()) {
            Query<Service> query = session.createQuery("from Service s where s.serviceStatus.id != 3",
                    Service.class);
            return query.getResultList();
        }
    }
}
