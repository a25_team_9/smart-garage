package com.telerikacademy.mechanicum.repositories;

import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.models.Role;
import com.telerikacademy.mechanicum.repositories.contracts.RoleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleRepositoryImpl implements RoleRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Role> getAll() {
        try (Session session = this.sessionFactory.openSession()) {
            Query<Role> query = session.createQuery("from Role", Role.class);
            return query.getResultList();
        }
    }

    @Override
    public Role getByID(int id) {
        try (Session session = this.sessionFactory.openSession()) {
            Role role = session.get(Role.class, id);
            if (role == null) {
                throw new EntityNotFoundException(String.format("Role with ID %d not found", id));
            }
            return role;
        }
    }
}
