package com.telerikacademy.mechanicum.repositories.contracts;

import com.telerikacademy.mechanicum.models.ServiceStatus;

public interface ServiceStatusRepository {

    ServiceStatus getById(int statusId);

}
