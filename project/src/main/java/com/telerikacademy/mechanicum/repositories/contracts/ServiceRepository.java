package com.telerikacademy.mechanicum.repositories.contracts;

import com.telerikacademy.mechanicum.models.Service;

import java.util.List;


public interface ServiceRepository {
    List<Service> getAll();

    Service getById(int serviceId);

    Service create(Service service);

    Service update(Service service);

    void delete(Service service);

    boolean checkVisitCompletionAllServices(int visitId);

    List<Service> getAllNotFinishedServices();
}
