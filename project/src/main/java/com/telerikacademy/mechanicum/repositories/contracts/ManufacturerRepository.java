package com.telerikacademy.mechanicum.repositories.contracts;

import com.telerikacademy.mechanicum.models.Manufacturer;

import java.util.List;

public interface ManufacturerRepository {

    List<Manufacturer> getAll();

    Manufacturer getById(int id);

    Manufacturer getByName(String name);

    void create(Manufacturer make);
}
