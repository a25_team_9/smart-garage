package com.telerikacademy.mechanicum.repositories.contracts;

import com.telerikacademy.mechanicum.models.Currency;
import com.telerikacademy.mechanicum.models.Visit;

import java.util.List;
import java.util.Optional;

public interface VisitRepository {
    List<Visit> getAll();

    Visit getByID(int id);

    Visit create(Visit visit);

    Visit update(Visit visit);

    void delete(Visit visit);

    List<Visit> getCustomerVisits(int customerId,
                                  int numberOfVisits);

    List<Visit> getCustomerLinkedServices(int customerID, Optional<Integer> vehicleId, Optional<String> date);
}
