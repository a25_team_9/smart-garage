package com.telerikacademy.mechanicum.repositories;

import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.models.Currency;
import com.telerikacademy.mechanicum.repositories.contracts.CurrencyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Locale;

@Repository
public class CurrencyRepositoryImpl implements CurrencyRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CurrencyRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Currency> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Currency> query = session.createQuery(" from Currency ", Currency.class);
            return query.getResultList();
        }
    }

    @Override
    public Currency getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Currency currency = session.get(Currency.class, id);
            if (currency == null) {
                throw new EntityNotFoundException("Currency", "id", String.valueOf(id));
            }
            return currency;
        }
    }

    @Override
    public Currency getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Currency> query = session.createQuery(" from Currency c where c.name = :name", Currency.class);
            query.setParameter("name", name.toUpperCase(Locale.ROOT));
            if (query.getResultList().size() == 0) {
                throw new EntityNotFoundException("Currency", "name", name);
            }
            return query.getResultList().get(0);
        }
    }

    @Override
    public Currency create(Currency currency) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(currency);
            session.getTransaction().commit();
            return currency;
        }
    }

    @Override
    public Currency update(Currency currency) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(currency);
            session.getTransaction().commit();
            return currency;
        }
    }

    @Override
    public void delete(Currency currency) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(currency);
            session.getTransaction().commit();
        }
    }
}
