package com.telerikacademy.mechanicum.repositories;

import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.Vehicle;
import com.telerikacademy.mechanicum.repositories.contracts.VehicleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class VehicleRepositoryImpl implements VehicleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public VehicleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Vehicle> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery("from Vehicle", Vehicle.class);
            return query.getResultList();
        }
    }

    @Override
    public Vehicle getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Vehicle vehicle = session.get(Vehicle.class, id);
            if (vehicle == null) {
                throw new EntityNotFoundException("Vehicle", id);
            }
            return vehicle;
        }
    }

    @Override
    public Vehicle getByVin(String vin) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery(
                    "from Vehicle where vin = :vin", Vehicle.class);
            query.setParameter("vin", vin);

            List<Vehicle> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Vehicle", "vin", vin);
            }
            return result.get(0);
        }
    }

    @Override
    public Vehicle getByLicencePlate(String licencePlate) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery(
                    "from Vehicle where licencePlate = :licencePlate", Vehicle.class);
            query.setParameter("licencePlate", licencePlate);

            List<Vehicle> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Vehicle", "licence plate", licencePlate);
            }
            return result.get(0);
        }
    }


    @Override
    public Vehicle create(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            session.save(vehicle);
            return vehicle;
        }
    }

    @Override
    public Vehicle update(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(vehicle);
            session.getTransaction().commit();
            return vehicle;
        }
    }

    @Override
    public void delete(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(vehicle);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Vehicle> filter(Map<String, String> parameters) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery("from Vehicle v where v.owner.id = :ownerId");
            query.setParameter("ownerId", Integer.parseInt(parameters.get("ownerId")));
            List<Vehicle> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Vehicle", "owner", parameters.get("ownerId"));
            }
            return query.getResultList();
        }
    }

    @Override
    public List<Service> getServicesByCarId(int carId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Service> query = session.createQuery("from Service s where s.visit.vehicle.id = :carId");
            query.setParameter("carId", carId);
            return query.getResultList();
        }
    }

    @Override
    public int getAllVehiclesCount() {
        return getAll().size();
    }
}
