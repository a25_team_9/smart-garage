package com.telerikacademy.mechanicum.repositories.contracts;

import com.telerikacademy.mechanicum.models.Currency;

import java.util.List;

public interface CurrencyRepository {
    List<Currency> getAll();

    Currency getById(int id);

    Currency getByName(String name);

    Currency create(Currency currency);

    Currency update(Currency currency);

    void delete(Currency currency);
}
