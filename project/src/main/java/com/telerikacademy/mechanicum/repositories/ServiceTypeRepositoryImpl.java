package com.telerikacademy.mechanicum.repositories;

import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.models.ServiceType;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceTypeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class ServiceTypeRepositoryImpl implements ServiceTypeRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ServiceTypeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ServiceType> getAll() {
        try (Session session = this.sessionFactory.openSession()) {
            Query<ServiceType> query = session.createQuery("from ServiceType", ServiceType.class);
            return query.getResultList();
        }
    }

    @Override
    public ServiceType getById(int id) {
        try (Session session = this.sessionFactory.openSession()) {
            ServiceType serviceType = session.get(ServiceType.class, id);
            if (serviceType == null) {
                throw new EntityNotFoundException("Service Type", id);
            }
            return serviceType;
        }
    }

    @Override
    public ServiceType getByName(String name) {
        try (Session session = this.sessionFactory.openSession()) {
            Query<ServiceType> query = session.createQuery("from ServiceType where name = :name", ServiceType.class);
            query.setParameter("name", name);
            List<ServiceType> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Service Type", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public ServiceType create(ServiceType serviceType) {
        try (Session session = sessionFactory.openSession()) {
            session.save(serviceType);
            return serviceType;
        }
    }

    @Override
    public ServiceType update(ServiceType serviceType) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(serviceType);
            session.getTransaction().commit();
            return serviceType;
        }
    }

    @Override
    public List<ServiceType> filter(Map<String, String> filterMap) {
        String queryString = mapToQuery(filterMap);
        try (Session session = sessionFactory.openSession()) {
            Query<ServiceType> query = session.createQuery(queryString, ServiceType.class);
            return query.getResultList();
        }
    }

    @Override
    public void delete(ServiceType serviceTypeToDelete) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(serviceTypeToDelete);
            session.getTransaction().commit();
        }
    }

    private String mapToQuery(Map<String, String> filterMap) {
        String name = filterMap.getOrDefault("name", "");
        String singlePrice = filterMap.getOrDefault("singlePrice", "");
        if (name.isEmpty() && singlePrice.isEmpty()) {
            throw new InvalidFieldException("Please specify at least one filter parameter");
        }
        double currentPrice = 0;
        double priceMin = 0;
        double priceMax = 0;
        if (!singlePrice.isBlank() && !singlePrice.isEmpty()) {
            try {
                currentPrice = Double.parseDouble(singlePrice);
            } catch (NumberFormatException e) {
                throw new InvalidFieldException("Single price must be a number");
            }
            priceMax = currentPrice + 0.01;
            priceMin = currentPrice - 0.01;
        }
        if (currentPrice < 0) {
            throw new InvalidFieldException("Single price must be a positive number");
        }

        List<String> myQuery = new ArrayList<>();
        if (!name.isEmpty()) {
            myQuery.add(String.format(" s.name like '%%%s%%' ", name));
        }
        if (!singlePrice.isEmpty()) {
            myQuery.add(String.format(" s.singlePrice > '%s' ", String.valueOf(priceMin)));
            myQuery.add(String.format(" s.singlePrice < '%s' ", String.valueOf(priceMax)));
        }
        myQuery.add(0, " from ServiceType s where ");
        if (myQuery.size() > 1) {
            for (int i = 1; i < myQuery.size() - 1; i++) {
                myQuery.set(i, myQuery.get(i) + "and");
            }
        }
        StringBuilder result = new StringBuilder();
        for (String param : myQuery) {
            result.append(param);
        }

        return result.toString();
    }
}
