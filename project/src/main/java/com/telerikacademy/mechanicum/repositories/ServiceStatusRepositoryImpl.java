package com.telerikacademy.mechanicum.repositories;

import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.models.ServiceStatus;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceStatusRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ServiceStatusRepositoryImpl implements ServiceStatusRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ServiceStatusRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public ServiceStatus getById(int statusId) {
        try (Session session = sessionFactory.openSession()) {
            ServiceStatus status = session.get(ServiceStatus.class, statusId);
            if (status == null) {
                throw new EntityNotFoundException("Status", "id", "statusId");
            }
            return status;
        }
    }
}
