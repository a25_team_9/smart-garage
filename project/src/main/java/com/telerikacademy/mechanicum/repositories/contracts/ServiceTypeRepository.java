package com.telerikacademy.mechanicum.repositories.contracts;

import com.telerikacademy.mechanicum.models.ServiceType;

import java.util.List;
import java.util.Map;

public interface ServiceTypeRepository {
    List<ServiceType> getAll();

    ServiceType getById(int id);

    ServiceType getByName(String name);

    ServiceType create(ServiceType serviceType);

    ServiceType update(ServiceType serviceType);

    List<ServiceType> filter(Map<String, String> filterMap);

    void delete(ServiceType serviceTypeToDelete);
}
