package com.telerikacademy.mechanicum.repositories.contracts;

import com.telerikacademy.mechanicum.models.Role;

import java.util.List;

public interface RoleRepository {
    List<Role> getAll();

    Role getByID(int id);
}
