package com.telerikacademy.mechanicum.repositories.contracts;

import com.telerikacademy.mechanicum.models.VehicleModel;

import java.util.List;

public interface VehicleModelRepository {

    List<VehicleModel> getAll();

    VehicleModel getById(int id);

    VehicleModel getByName(String model);

    void create(VehicleModel model);
}
