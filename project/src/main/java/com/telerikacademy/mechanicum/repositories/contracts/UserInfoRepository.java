package com.telerikacademy.mechanicum.repositories.contracts;

import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Visit;

import java.util.List;
import java.util.Map;

public interface UserInfoRepository {

    List<UserInfo> getAll();

    List<UserInfo> getAllCustomers();

    UserInfo getByID(int id);

    UserInfo create(UserInfo customer);

    UserInfo update(UserInfo customer);

    void delete(UserInfo id);

    UserInfo getByEmail(String email);

    List<UserInfo> getByName(String name);

    UserInfo getByToken(String token);

    List<Visit> filter(Map<String, String> filterMap);

    List<Visit> sort(Map<String, String> filterMap);

    int getAllCustomersCount();
}
