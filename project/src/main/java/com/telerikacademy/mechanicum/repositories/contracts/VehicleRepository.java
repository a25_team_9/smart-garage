package com.telerikacademy.mechanicum.repositories.contracts;

import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.Vehicle;

import java.util.List;
import java.util.Map;

public interface VehicleRepository {

    List<Vehicle> getAll();

    Vehicle getById(int id);

    Vehicle getByVin(String vin);

    Vehicle getByLicencePlate(String licencePlate);

    Vehicle create(Vehicle vehicle);

    Vehicle update(Vehicle vehicle);

    void delete(Vehicle vehicle);

    List<Vehicle> filter(Map<String, String> parameters);

    List<Service> getServicesByCarId(int carId);

    int getAllVehiclesCount();
}
