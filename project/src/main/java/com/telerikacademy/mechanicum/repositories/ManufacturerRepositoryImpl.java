package com.telerikacademy.mechanicum.repositories;

import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.models.Manufacturer;
import com.telerikacademy.mechanicum.repositories.contracts.ManufacturerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ManufacturerRepositoryImpl implements ManufacturerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ManufacturerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Manufacturer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Manufacturer> query = session.createQuery("from Manufacturer", Manufacturer.class);
            return query.getResultList();
        }
    }

    @Override
    public Manufacturer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Manufacturer make = session.get(Manufacturer.class, id);
            if (make == null) {
                throw new EntityNotFoundException("Manufacturer", id);
            }
            return make;
        }
    }

    @Override
    public Manufacturer getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Manufacturer> query = session.createQuery(
                    "from Manufacturer where name = :name", Manufacturer.class);
            query.setParameter("name", name);

            List<Manufacturer> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Manufacturer", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public void create(Manufacturer make) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(make);
            session.getTransaction().commit();
        }
    }
}
