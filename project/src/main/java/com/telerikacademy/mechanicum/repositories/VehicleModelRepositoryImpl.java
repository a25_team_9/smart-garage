package com.telerikacademy.mechanicum.repositories;

import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.models.VehicleModel;
import com.telerikacademy.mechanicum.repositories.contracts.VehicleModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleModelRepositoryImpl implements VehicleModelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public VehicleModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<VehicleModel> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<VehicleModel> query = session.createQuery("from VehicleModel", VehicleModel.class);
            return query.getResultList();
        }
    }

    @Override
    public VehicleModel getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            VehicleModel vehicleModel = session.get(VehicleModel.class, id);
            if (vehicleModel == null) {
                throw new EntityNotFoundException("Model", id);
            }
            return vehicleModel;
        }
    }

    @Override
    public VehicleModel getByName(String model) {
        try (Session session = sessionFactory.openSession()) {
            Query<VehicleModel> query = session.createQuery(
                    "from VehicleModel where model = :model", VehicleModel.class);
            query.setParameter("model", model);

            List<VehicleModel> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Model", "name", model);
            }
            return result.get(0);
        }
    }

    @Override
    public void create(VehicleModel model) {
        try (Session session = sessionFactory.openSession()) {
            session.save(model);
        }
    }
}
