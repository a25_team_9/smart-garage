package com.telerikacademy.mechanicum.exceptions;

public class UnauthorizedOperationException extends RuntimeException{
    public UnauthorizedOperationException(String error) {
        super(error);
    }
}
