package com.telerikacademy.mechanicum.exceptions;

public class InvalidFieldException extends RuntimeException{
    public InvalidFieldException(String error) {
        super(error);
    }
}
