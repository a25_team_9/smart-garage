package com.telerikacademy.mechanicum.exceptions;

public class AuthenticationFailureException extends RuntimeException{
    public AuthenticationFailureException(String error) {
        super(error);
    }
}
