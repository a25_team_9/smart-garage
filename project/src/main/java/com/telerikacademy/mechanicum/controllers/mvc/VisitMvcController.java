package com.telerikacademy.mechanicum.controllers.mvc;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.GeneratePdfReport;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.*;
import com.telerikacademy.mechanicum.models.dto.*;
import com.telerikacademy.mechanicum.services.contracts.*;
import com.telerikacademy.mechanicum.utils.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/visits")
public class VisitMvcController {
    private final VisitService visitService;
    private final UserInfoService userInfoService;
    private final VehicleService vehicleService;
    private final EmailService emailService;
    private final CurrencyService currencyService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final VehicleModelService vehicleModelService;
    private final ManufacturerService manufacturerService;
    private final ServiceTypeService serviceTypeService;

    @Autowired
    public VisitMvcController(VisitService visitService,
                              UserInfoService userInfoService,
                              VehicleService vehicleService,
                              EmailService emailService,
                              CurrencyService currencyService,
                              ModelMapper modelMapper,
                              AuthenticationHelper authenticationHelper,
                              VehicleModelService vehicleModelService,
                              ManufacturerService manufacturerService,
                              ServiceTypeService serviceTypeService) {
        this.visitService = visitService;
        this.userInfoService = userInfoService;
        this.vehicleService = vehicleService;
        this.emailService = emailService;
        this.currencyService = currencyService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
        this.vehicleModelService = vehicleModelService;
        this.manufacturerService = manufacturerService;
        this.serviceTypeService = serviceTypeService;
    }

    @GetMapping
    public String showAllVisits(Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        modelsForAllVisits(new VisitDto(), model, personCalledMethod);

        return "visits";
    }

    @GetMapping("/{id}")
    public String showSingleVisit(@PathVariable Integer id, Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        if (personCalledMethod.getUserAuthorization().isEmployee()) {
            modelsForAllVisits(new VisitDto(), model, personCalledMethod);
        } else {
            modelsForAllVisitsCustomer(new VisitDto(), model, personCalledMethod);
        }
        try {
            Visit visit = visitService.getById(id, personCalledMethod);
            model.addAttribute("singleVisit", visit);
            model.addAttribute("services", visit.getServiceList());
        } catch (EntityNotFoundException e) {
            return "redirect:/visits";
        } catch (NumberFormatException num) {
            model.addAttribute("error", "Page not found");
            return "errors/not-found";
        }
        return "visit";
    }

    @PostMapping("/create")
    public String createVisit(@Valid @ModelAttribute("visitDto") VisitDto visitDto,
                              BindingResult bindingResult, Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        if (bindingResult.hasErrors()) {
            modelsForAllVisits(visitDto, model, personCalledMethod);
            return "visits";
        }

        Visit visit = modelMapper.fromDtoToVisit(visitDto);
        visitService.create(visit, personCalledMethod);
        return String.format("redirect:/visits/%d", visit.getId());

    }

    @PostMapping("/update/{id}")
    public String updateVisit(@Valid @ModelAttribute("visitUpdateDto") VisitUpdateDto visitUpdateDto,
                              BindingResult bindingResult,
                              @PathVariable Integer id, Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        if (bindingResult.hasErrors()) {
            modelsForAllVisits(new VisitDto(), model, personCalledMethod);
            model.addAttribute("visitUpdateDto", visitUpdateDto);
            return "visits";
        }
        Visit visit = modelMapper.fromDtoToVisit(visitUpdateDto, id);
        visitService.update(visit, personCalledMethod);
        return String.format("redirect:/visits/%d", visit.getId());
    }

    @PostMapping("/delete/{id}")
    public String deleteShipment(@PathVariable Integer id, Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        modelsForAllVisits(new VisitDto(), model, personCalledMethod);
        Visit visit = visitService.getById(id, personCalledMethod);
        model.addAttribute("singleVisit", visit);
        model.addAttribute("services", visit.getServiceList());

        try {
            visitService.delete(id, personCalledMethod);
            return "redirect:/visits";
        } catch (InvalidFieldException e) {
            model.addAttribute("deleteError", e.getMessage());
            return "visit";
        }
    }

    @PostMapping("/search")
    public String multiSearch(@Valid @ModelAttribute("userInfoFilterDto") UserInfoFilterDto userInfoFilterDto,
                              BindingResult error,
                              Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        List<Visit> visitListResult;
        Map<String, String> searchParams = modelMapper.fromDtoToMap(userInfoFilterDto);
        try {
            modelsForAllVisits(new VisitDto(), model, personCalledMethod);
            visitListResult = userInfoService.filter(searchParams, personCalledMethod);
            model.addAttribute("visits", visitListResult);
        } catch (EntityNotFoundException | InvalidFieldException e) {
            model.addAttribute("visits", new ArrayList<Visit>());
            model.addAttribute("searchError", e.getMessage());
            return "visits";
        }

        return "visits";
    }

    //Frpom TODO to DONE in Profile-> tomorrow (report for multiple visits) , maybe use parameter to specify max reports.
    @GetMapping(value = "/{id}/reports", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> getVisitReport(HttpSession session, @PathVariable int id) {

        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);

        ByteArrayInputStream bis = generateBis(id, personCalledMethod);
        var header = new HttpHeaders();
        header.add("Content-Disposition", "inline; filename=visitReport.pdf");

        return ResponseEntity
                .ok()
                .headers(header)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));

    }


    @PostMapping(value = "/{id}/reports", produces = MediaType.APPLICATION_PDF_VALUE)
    public String sendVisitReport(HttpSession session, @PathVariable int id) {
        //Tested and working
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        Visit visit = visitService.getById(id, personCalledMethod);
        ByteArrayInputStream bis = generateBis(id, personCalledMethod);

        try {
            emailService.sendReportEmail(
                    visit.getVehicle().getOwner().getUserAuthorization().getEmail(),
                    "Visit report",
                    "Report attached",
                    new ByteArrayDataSource(bis, MediaType.APPLICATION_PDF_VALUE));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return String.format("redirect:/visits/%d", visit.getId());
    }


    // Going to reset page without a token redirects to login page


    private Model modelsForAllVisits(VisitDto visitDto, Model model, UserInfo userInfo) {
        model.addAttribute("visitDto", visitDto);
        model.addAttribute("visitUpdateDto", new VisitUpdateDto());
        model.addAttribute("deleteServiceDto", new DeleteServiceDto());
        model.addAttribute("currentUserInfo", userInfo);
        model.addAttribute("currentUser", userInfo);
        model.addAttribute("serviceCreateDto", new ServiceDto());
        model.addAttribute("serviceUpdateDto", new ServiceDto());
        model.addAttribute("serviceTypes", serviceTypeService.getAll());
        model.addAttribute("visits", visitService.getAll(userInfo));
        model.addAttribute("vehicles", vehicleService.getAll(userInfo));
        model.addAttribute("currencies", currencyService.getAll());
        model.addAttribute("userInfoFilterDto", new UserInfoFilterDto());
        model.addAttribute("models", vehicleModelService.getAll(userInfo));
        model.addAttribute("makes", manufacturerService.getAll(userInfo));
        return model;
    }

    private Model modelsForAllVisitsCustomer(VisitDto visitDto, Model model, UserInfo userInfo) {
        model.addAttribute("visitDto", visitDto);
        model.addAttribute("visitUpdateDto", new VisitUpdateDto());
        model.addAttribute("currentUserInfo", userInfo);
        model.addAttribute("currentUser", userInfo);
        model.addAttribute("visits", new ArrayList<Visit>());
        model.addAttribute("vehicles", new ArrayList<Vehicle>());
        model.addAttribute("deleteServiceDto", new DeleteServiceDto());
        model.addAttribute("visitUpdateDto", new VisitUpdateDto());
        model.addAttribute("serviceCreateDto", new ServiceDto());
        model.addAttribute("serviceUpdateDto", new ServiceDto());
        model.addAttribute("serviceTypes", serviceTypeService.getAll());
        model.addAttribute("currencies", currencyService.getAll());
        model.addAttribute("userInfoFilterDto", new UserInfoFilterDto());
        model.addAttribute("models", new ArrayList<VehicleModel>());
        model.addAttribute("makes", new ArrayList<Manufacturer>());
        return model;
    }


    private ByteArrayInputStream generateBis(int id, UserInfo personCalledMethod) {
        Visit visit = visitService.getById(id, personCalledMethod);
        List<Visit> visitList = new ArrayList<>();
        visitList.add(visit);
        return GeneratePdfReport.visitReport(visitList);
    }
}
