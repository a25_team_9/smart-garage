package com.telerikacademy.mechanicum.controllers.rest;

import com.telerikacademy.mechanicum.exceptions.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice                               //This one is responsible for making it in JSON format
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private String message;

    private int httpStatus;

    public RestResponseEntityExceptionHandler() {
    }

    public RestResponseEntityExceptionHandler(String message, int httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    @ExceptionHandler(value = {EntityNotFoundException.class})
    protected ResponseEntity<Object> handleNotFound(RuntimeException ex) {
        return new ResponseEntity<>
                (new RestResponseEntityExceptionHandler(ex.getMessage(), 404),
                        new HttpHeaders(),
                        HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {DuplicateEntityException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex) {
        return new ResponseEntity<>
                (new RestResponseEntityExceptionHandler(ex.getMessage(), 409),
                        new HttpHeaders(),
                        HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = {InvalidFieldException.class})
    protected ResponseEntity<Object> handleBadRequest(RuntimeException ex) {
        return new ResponseEntity<>
                (new RestResponseEntityExceptionHandler(ex.getMessage(), 400),
                        new HttpHeaders(),
                        HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {UnauthorizedOperationException.class, AuthenticationFailureException.class})
    protected ResponseEntity<Object> handleUnAuthorized(RuntimeException ex) {
        return new ResponseEntity<>
                (new RestResponseEntityExceptionHandler(ex.getMessage(), 401),
                        new HttpHeaders(),
                        HttpStatus.UNAUTHORIZED);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        String resultMessage;
        try {
            resultMessage = ex.getBindingResult().getFieldError().getDefaultMessage();
        } catch (NullPointerException e) {
            resultMessage = "VALIDATION FAILED";
        }
        return new ResponseEntity<>
                (new RestResponseEntityExceptionHandler(resultMessage, 400),
                        new HttpHeaders(),
                        HttpStatus.BAD_REQUEST);
    }

}
