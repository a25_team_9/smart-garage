package com.telerikacademy.mechanicum.controllers.rest;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.VehicleModel;
import com.telerikacademy.mechanicum.models.dto.VehicleModelDto;
import com.telerikacademy.mechanicum.services.contracts.VehicleModelService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "Car models")
@RestController
@RequestMapping("/api/mechanicum/models")
public class VehicleModelController {

    private final VehicleModelService modelService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;

    @Autowired
    public VehicleModelController(VehicleModelService modelService,
                                  AuthenticationHelper authenticationHelper,
                                  ModelMapper modelMapper) {
        this.modelService = modelService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @ApiOperation(value = "Get all car models in our database",
            notes = "Only accessible from employees",
            response = VehicleModel.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<VehicleModel> getAll(@RequestHeader HttpHeaders headers) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return modelService.getAll(personCalledMethod);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get model by ID",
            notes = "Accessible from customer that is in the current visit or employee",
            response = VehicleModel.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public VehicleModel getById(@RequestHeader HttpHeaders headers,
                                @ApiParam(value = "ID of the model you are looking for", example = "83")
                                @Valid @PathVariable int id) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return modelService.getById(id, personCalledMethod);
    }

    @PostMapping
    @ApiOperation(value = "Create model if not exist in the database",
            notes = "Accessible only for employee",
            response = VehicleModel.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization",
                    dataTypeClass = HttpHeaders.class,
                    required = true,
                    paramType = "header",
                    example = "ggbg@gmail.com",
                    value = "See example below"),
            @ApiImplicitParam(name = "Parameters",
                    dataTypeClass = VehicleModelDto.class,
                    required = true,
                    paramType = "body",
                    value = "Send in JSON format. Create first brand if not exist")
    })
    public VehicleModel create(@RequestHeader HttpHeaders headers,
                               @ApiIgnore @Valid @RequestBody VehicleModelDto dto) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        VehicleModel model = modelMapper.fromDtoToModel(dto);
        return modelService.create(model, personCalledMethod);
    }

}
