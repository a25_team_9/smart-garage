package com.telerikacademy.mechanicum.controllers.utilities;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.Visit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

public class GeneratePdfReport {
    private static final Logger logger = LoggerFactory.getLogger(GeneratePdfReport.class);

    public static ByteArrayInputStream visitReport(List<Visit> visits) {

        Document document = new Document();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(document, out);
            document.open();
        } catch (DocumentException ex) {
            logger.error("Error occurred: {0}", ex);
        }
        for (Visit visit : visits) {
            PdfPTable table = new PdfPTable(5);
            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

            try {
                generateTable(visit, table);
                Paragraph titleParagraph = new Paragraph();
                titleParagraph.add(String.format("Report for visit: %d", visit.getId()));
                titleParagraph.setFont(headFont);
                titleParagraph.setExtraParagraphSpace(20f);
                titleParagraph.setSpacingAfter(10f);
                titleParagraph.setAlignment(Element.ALIGN_CENTER);

                Paragraph paragraph = new Paragraph();
                paragraph.add("All services done:");
                paragraph.setExtraParagraphSpace(20f);
                paragraph.setSpacingAfter(10f);
                paragraph.setAlignment(Element.ALIGN_LEFT);

                Paragraph totalPrice = new Paragraph();
                totalPrice.add(String.format("Total price: %.2f%s", visit.getTotalPrice(), visit.getCurrency().getName()));
                totalPrice.setExtraParagraphSpace(20f);
                totalPrice.setSpacingAfter(10f);
                totalPrice.setAlignment(Element.ALIGN_RIGHT);

                document.add(titleParagraph);
                document.add(paragraph);
                document.add(table);
                document.addTitle("Visit Report");
                document.add(totalPrice);

            } catch (DocumentException ex) {
                logger.error("Error occurred: {0}", ex);
            }
        }
        document.close();
        return new ByteArrayInputStream(out.toByteArray());
    }


    private static void generateTable(Visit visit, PdfPTable table) throws DocumentException {
        table.setWidthPercentage(100);
        table.setWidths(new int[]{4, 4, 4, 4, 4});

        Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

        PdfPCell hcell;
        hcell = new PdfPCell(new Phrase("Name", headFont));
        hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(hcell);

        hcell = new PdfPCell(new Phrase("Start Date", headFont));
        hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(hcell);

        hcell = new PdfPCell(new Phrase("End Date", headFont));
        hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(hcell);

        hcell = new PdfPCell(new Phrase("Quantity", headFont));
        hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(hcell);

        hcell = new PdfPCell(new Phrase("Single quantity price", headFont));
        hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(hcell);

        for (Service service : visit.getServiceList()) {

            PdfPCell cell;

            cell = new PdfPCell(new Phrase(service.getServiceType().getName()));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(service.getStartDate().toString()));
            cell.setPaddingLeft(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(service.getEndDate().toString()));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(service.getQuantity())));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(service.getServiceType().getSinglePrice()) + "BGN"));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setPaddingRight(5);
            table.addCell(cell);

        }
    }
}
