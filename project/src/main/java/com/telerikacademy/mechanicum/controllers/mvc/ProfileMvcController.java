package com.telerikacademy.mechanicum.controllers.mvc;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.GeneratePdfReport;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.models.Manufacturer;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.VehicleModel;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.models.dto.*;
import com.telerikacademy.mechanicum.services.contracts.*;
import com.telerikacademy.mechanicum.utils.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/profile")
public class ProfileMvcController {
    private final UserInfoService userInfoService;
    private final EmailService emailService;
    private final AuthenticationHelper authenticationHelper;
    private final VehicleModelService vehicleModelService;
    private final VisitService visitService;
    private final ManufacturerService manufacturerService;
    private final ModelMapper modelMapper;
    private final CurrencyService currencyService;

    @Autowired
    public ProfileMvcController(UserInfoService userInfoService,
                                EmailService emailService,
                                AuthenticationHelper authenticationHelper,
                                VehicleModelService vehicleModelService,
                                VisitService visitService,
                                ManufacturerService manufacturerService,
                                ModelMapper modelMapper,
                                CurrencyService currencyService) {
        this.userInfoService = userInfoService;
        this.emailService = emailService;
        this.authenticationHelper = authenticationHelper;
        this.vehicleModelService = vehicleModelService;
        this.visitService = visitService;
        this.manufacturerService = manufacturerService;
        this.modelMapper = modelMapper;
        this.currencyService = currencyService;
    }

    @GetMapping("/{id}")
    public String getProfile(Model model, HttpSession session, @PathVariable Integer id) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        UserInfo userInfo = userInfoService.getByID(id, personCalledMethod);
        modelsForSingleView(model, personCalledMethod, userInfo);
        return "profile";
    }

    @PostMapping("/{id}/reports")
    public ResponseEntity<InputStreamResource> getReports(Model model, HttpSession session, @PathVariable Integer id, @Valid @ModelAttribute("reportDto") VisitReportDto visitReportDto) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        ByteArrayInputStream bis = generateBis(id, personCalledMethod, visitReportDto);
        var header = new HttpHeaders();
        header.add("Content-Disposition", "inline; filename=visitReport.pdf");

        return ResponseEntity
                .ok()
                .headers(header)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    @PostMapping(value = "/{id}/reports/send", produces = MediaType.APPLICATION_PDF_VALUE)
    public String sendVisitReport(Model model, HttpSession session,
                                  @PathVariable Integer id,
                                  @ModelAttribute("reportDto") VisitReportDto visitReportDto) {
        //Tested and working
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        UserInfo userInfo = userInfoService.getByID(id, personCalledMethod);
        ByteArrayInputStream bis = generateBis(id, personCalledMethod, visitReportDto);

        try {
            emailService.sendReportEmail(
                    userInfo.getUserAuthorization().getEmail(),
                    "Visit report",
                    "Report attached",
                    new ByteArrayDataSource(bis, MediaType.APPLICATION_PDF_VALUE));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return String.format("redirect:/profile/%d", userInfo.getUserID());
    }

    @PostMapping("/update/{id}")
    public String updateProfile(@Valid @ModelAttribute("userInfoUpdateDto") UserInfoUpdateDto userInfoUpdateDto,
                                BindingResult bindingResult,
                                @PathVariable Integer id, Model model, HttpSession session) {

        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        UserInfo userInfo = userInfoService.getByID(id, personCalledMethod);
        modelsForSingleView(model, personCalledMethod, userInfo);

        if (bindingResult.hasErrors()) {
            return String.format("redirect:/profile/%d",id);
        }
        try {
            userInfo = modelMapper.fromDtoToCustomer(userInfoUpdateDto, id);
            userInfoService.update(userInfo, personCalledMethod);
        } catch (InvalidFieldException ife) {
            model.addAttribute("updateError", ife.getMessage());
            return "profile";
        }
        return String.format("redirect:/profile/%d", userInfo.getUserID());
    }

    @PostMapping("/delete/{id}")
    public String deleteProfile(@PathVariable Integer id, Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        UserInfo userInfo = userInfoService.getByID(id, personCalledMethod);
        modelsForSingleView(model, personCalledMethod, userInfo);

        try {
            userInfoService.delete(id, personCalledMethod);
            return "redirect:/";
        } catch (InvalidFieldException e) {
            model.addAttribute("deleteError", e.getMessage());
            return "profile";
        }
    }

    private void modelsForSingleView(Model model, UserInfo personCalledMethod, UserInfo userInfo) {
        ForgottenPasswordDto forgottenPasswordDto = new ForgottenPasswordDto();
        forgottenPasswordDto.setEmail(userInfo.getUserAuthorization().getEmail());
        model.addAttribute("currentUser", personCalledMethod);
        model.addAttribute("forgottenPassword", forgottenPasswordDto);
        model.addAttribute("currencies", currencyService.getAll());
        model.addAttribute("userInfoFilterDto", new UserInfoFilterDto());
        UserInfoUpdateDto updateDto = new UserInfoUpdateDto();
        model.addAttribute("userInfoUpdateDto", new UserInfoUpdateDto());
        model.addAttribute("reportDto", new VisitReportDto());
        model.addAttribute("visitDto", new VisitDto());

        List<Visit> visitList;
        try {
            visitList = visitService.getLinkedServices(userInfo.getUserID(), personCalledMethod, Optional.empty(), Optional.empty());
        } catch (EntityNotFoundException e) {
            visitList = new ArrayList<>();
        }
        model.addAttribute("visits", visitList);
        model.addAttribute("singleUser", userInfo);

        if (personCalledMethod.getUserAuthorization().isEmployee()) {
            model.addAttribute("models", vehicleModelService.getAll(personCalledMethod));
            model.addAttribute("makes", manufacturerService.getAll(personCalledMethod));
        } else {
            model.addAttribute("models", new ArrayList<VehicleModel>());
            model.addAttribute("makes", new ArrayList<Manufacturer>());
        }
    }


    private ByteArrayInputStream generateBis(int id, UserInfo personCalledMethod, VisitReportDto visitReportDto) {
        List<Visit> visitList = visitService.getCustomerVisits(id, Optional.of(visitReportDto.getNumberOfVisits()), Optional.of(visitReportDto.getCurrency()), personCalledMethod);
        if(visitList.isEmpty()){
            throw new EntityNotFoundException("No visits for which we can generate report");
        }
        return GeneratePdfReport.visitReport(visitList);
    }


}
