package com.telerikacademy.mechanicum.controllers.mvc;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.controllers.utilities.TokenScheduler;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.models.dto.*;
import com.telerikacademy.mechanicum.services.contracts.*;
import com.telerikacademy.mechanicum.utils.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
public class UserInfoMvcController {
    private final UserInfoService userInfoService;
    private final EmailService emailService;
    private final AuthenticationHelper authenticationHelper;
    private final VehicleModelService vehicleModelService;
    private final ManufacturerService manufacturerService;
    private final ModelMapper modelMapper;
    private final VehicleService vehicleService;

    @Autowired
    public UserInfoMvcController(UserInfoService userInfoService,
                                 EmailService emailService,
                                 AuthenticationHelper authenticationHelper,
                                 VehicleModelService vehicleModelService,
                                 ManufacturerService manufacturerService,
                                 ModelMapper modelMapper,
                                 VehicleService vehicleService) {
        this.userInfoService = userInfoService;
        this.emailService = emailService;
        this.authenticationHelper = authenticationHelper;
        this.vehicleModelService = vehicleModelService;
        this.manufacturerService = manufacturerService;
        this.modelMapper = modelMapper;
        this.vehicleService = vehicleService;
    }

    @GetMapping
    public String getAllUsers(Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        getAllAttributes(model, personCalledMethod);
        return "users";
    }


    // Process form submission from forgotPassword page
    @RequestMapping(value = "/forgot", method = RequestMethod.POST)
    public String processForgotPasswordForm(Model model, @Valid @ModelAttribute ForgottenPasswordDto forgottenPassword,
                                            HttpServletRequest request) {
        UserInfo userInfo = userInfoService.getByEmail(forgottenPassword.getEmail());
        if (userInfo.getUserAuthorization().getResetToken() != null) {
            throw new UnauthorizedOperationException("You can reset your password once per hour");
        }
        // Generate random 36-character string token for reset password

        userInfo.getUserAuthorization().setResetToken(UUID.randomUUID().toString());


        userInfoService.update(userInfo, userInfo);

        String appUrl = "http://" + request.getServerName() + ":8080";
        String text = "To reset your password, click the link below:" + appUrl
                + "/users/reset?token=" + userInfo.getUserAuthorization().getResetToken();

        String to = userInfo.getUserAuthorization().getEmail();
        String subject = ("Password Reset Request");
        emailService.sendMail(to, subject, text);
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.schedule(new TokenScheduler(userInfoService, userInfo), 60, TimeUnit.MINUTES);
        //Add success message to view

        return "redirect:/";

    }

    // Display form to reset password
    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public String displayResetPasswordPage(Model model, @RequestParam("token") String token, HttpSession session) {
        //Todo Add HttpSession
        UserInfo user = userInfoService.getByResetToken(token, new UserInfo());
        user.getUserAuthorization().setResetToken(null);
        userInfoService.update(user, user);
        model.addAttribute("userId", user.getUserID());
        model.addAttribute("passwords", new ResetPasswordDto());
        session.setAttribute("loggedUserEmail", user.getUserAuthorization().getEmail());
        return "reset";
    }

    @RequestMapping(value = "/reset/{id}", method = RequestMethod.POST)
    public String updatePassword(Model model, @PathVariable Integer id, @Valid @ModelAttribute ResetPasswordDto passwords, HttpSession session) {

        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        UserInfo user = userInfoService.getByID(id, personCalledMethod);
        if (!passwords.getPassword().equals(passwords.getRepeatPassword())) {
            model.addAttribute("userId", user.getUserID());
            model.addAttribute("passwords", passwords);
            return "reset";
        }
        user.getUserAuthorization().setPassword(passwords.getPassword());
        user.getUserAuthorization().setResetToken(null);
        userInfoService.update(user, personCalledMethod);

        session.removeAttribute("loggedUserEmail");
        return "redirect:/login";
    }


    @GetMapping("/{userId}/vehicles")
    public String showUserVehicles(Model model, HttpSession session, @PathVariable int userId) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(session);
        model.addAttribute("currentUser", currentUser);

        model.addAttribute("vehicles", userInfoService.getByID(userId, currentUser).getVehicleList());
        return "vehicles";
    }

    @PostMapping("/sort")
    public String sortByNameOrVisitDate(Model model,
                                        @ModelAttribute CustomerSortParameters customerSortParameters,
                                        HttpSession session) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(session);
        model.addAttribute("currentUser", currentUser);
        Map<String, String> map = modelMapper.fromDtoToMap(customerSortParameters);
        getAllAttributes(model, currentUser);
        List<Visit> listVisit = userInfoService.sortByNameOrVisitDate(map, currentUser);
        List<UserInfo> userList = modelMapper.fromVisitsToCustomers(listVisit);
        model.addAttribute("users", userList);

        return "users";
    }

    @PostMapping("/search")
    public String multiSearch(@Valid @ModelAttribute("userInfoFilterDto") UserInfoFilterDto userInfoFilterDto,
                              BindingResult error,
                              Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        List<Visit> visitListResult;
        Map<String, String> searchParams = modelMapper.fromDtoToMap(userInfoFilterDto);
        try {
            getAllAttributes(model, personCalledMethod);
            visitListResult = userInfoService.filter(searchParams, personCalledMethod);
            model.addAttribute("users", visitListResult
                    .stream()
                    .map(vs -> vs.getVehicle().getOwner())
                    .collect(Collectors.toSet()));
        } catch (EntityNotFoundException | InvalidFieldException e) {
            model.addAttribute("users", new ArrayList<UserInfo>());
            model.addAttribute("searchError", e.getMessage());
            return "users";
        }

        return "users";
    }


    // Going to reset page without a token redirects to login page
    @ExceptionHandler(EntityNotFoundException.class)
    public String handleNotFound(Model model, EntityNotFoundException ex) {
        model.addAttribute("error", ex.getMessage());
        return "errors/not-found";
    }

    private void getAllAttributes(Model model, UserInfo personCalledMethod) {
        model.addAttribute("users", userInfoService.getAll(personCalledMethod));
        model.addAttribute("currentUser", personCalledMethod);
        model.addAttribute("userInfoFilterDto", new UserInfoFilterDto());
        model.addAttribute("asc", getListWithOptions());
        model.addAttribute("customerSortParameters", new CustomerSortParameters());
        model.addAttribute("models", vehicleModelService.getAll(personCalledMethod));
        model.addAttribute("makes", manufacturerService.getAll(personCalledMethod));
    }

    private List<String> getListWithOptions() {
        List<String> list = new ArrayList<>();
        list.add("");
        list.add("asc");
        list.add("desc");
        return list;
    }


}
