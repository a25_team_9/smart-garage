package com.telerikacademy.mechanicum.controllers.rest;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.dto.ServiceDto;
import com.telerikacademy.mechanicum.services.contracts.ServiceService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/mechanicum/services")
public class ServiceController {

    private final ServiceService service;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;

    @Autowired
    public ServiceController(ServiceService service,
                             AuthenticationHelper authenticationHelper,
                             ModelMapper modelMapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @ApiOperation(value = "Get all services",
            notes = "Only accessible from employees",
            response = Service.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<Service> getAll(@RequestHeader HttpHeaders headers) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(headers);
        return service.getAll(currentUser);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find service by ID",
            notes = "Only accessible from employees",
            response = Service.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public Service getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(headers);
        return service.getById(id, currentUser);
    }

    @PostMapping
    @ApiOperation(value = "Create new service for an existing vehicle",
            notes = "Accessible only for employee",
            response = Service.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization",
                    dataTypeClass = HttpHeaders.class,
                    required = true,
                    paramType = "header",
                    example = "ggbg@gmail.com",
                    value = "See example below"),
            @ApiImplicitParam(name = "Parameters",
                    dataTypeClass = ServiceDto.class,
                    required = true,
                    paramType = "body",
                    value = "Send in JSON format")
    })
    public Service create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ServiceDto serviceDto) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(headers);
        Service serviceToCreate = modelMapper.fromDtoToService(serviceDto);
        return service.create(serviceToCreate, currentUser);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update an existing service ",
            notes = "Accessible only for employee",
            response = Service.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization",
                    dataTypeClass = HttpHeaders.class,
                    required = true,
                    paramType = "header",
                    example = "ggbg@gmail.com",
                    value = "See example below"),
            @ApiImplicitParam(name = "Parameters",
                    dataTypeClass = ServiceDto.class,
                    required = true,
                    paramType = "body",
                    value = "Send in JSON format")
    })
    public Service update(@RequestHeader HttpHeaders headers,
                          @PathVariable int id,
                          @Valid @RequestBody ServiceDto serviceDto) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(headers);
        Service serviceToUpdate = modelMapper.fromDto(serviceDto, id);
        return service.update(serviceToUpdate, currentUser);
    }

    @DeleteMapping("/{serviceId}")
    @ApiOperation(value = "Delete service by service ID",
            notes = "Accessible only for employee")
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int serviceId) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(headers);
        service.delete(serviceId, currentUser);
    }
}
