package com.telerikacademy.mechanicum.controllers.rest;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.models.Manufacturer;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.dto.ManufacturerDto;
import com.telerikacademy.mechanicum.services.contracts.ManufacturerService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "Car brands")
@RestController
@RequestMapping("/api/mechanicum/manufacturers")
public class ManufacturerController {

    private final ManufacturerService manufacturerService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;

    @Autowired
    public ManufacturerController(ManufacturerService manufacturerService,
                                  AuthenticationHelper authenticationHelper,
                                  ModelMapper modelMapper) {
        this.manufacturerService = manufacturerService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @ApiOperation(value = "Get all car brands in our database",
            notes = "Only accessible from employees",
            response = Manufacturer.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<Manufacturer> getAll(@RequestHeader HttpHeaders headers) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return manufacturerService.getAll(personCalledMethod);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get car brand by ID",
            notes = "Only accessible from employees",
            response = Manufacturer.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public Manufacturer getById(@RequestHeader HttpHeaders headers,
                                @ApiParam(value = "ID of the brand you are looking for", example = "17")
                                @Valid @PathVariable int id) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return manufacturerService.getById(id, personCalledMethod);
    }

    @PostMapping
    @ApiOperation(value = "Create brand if not exist in the database",
            notes = "Accessible only for employee",
            response = Manufacturer.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization",
                    dataTypeClass = HttpHeaders.class,
                    required = true,
                    paramType = "header",
                    example = "ggbg@gmail.com",
                    value = "See example below"),
            @ApiImplicitParam(name = "Parameters",
                    dataTypeClass = ManufacturerDto.class,
                    required = true,
                    paramType = "body",
                    value = "Send in JSON format")
    })
    public Manufacturer createManufacturer(@RequestHeader HttpHeaders headers,
                                           @ApiIgnore @Valid @RequestBody ManufacturerDto dto) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        Manufacturer make = modelMapper.fromDtoToManufacturer(dto);
        return manufacturerService.create(make, personCalledMethod);
    }

}

