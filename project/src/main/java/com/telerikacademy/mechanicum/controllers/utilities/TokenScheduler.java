package com.telerikacademy.mechanicum.controllers.utilities;

import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.services.contracts.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


public class TokenScheduler implements Runnable {
    private final UserInfoService userInfoService;
    private UserInfo userInfoToUpdate;

    public TokenScheduler(UserInfoService userInfoService, UserInfo userInfoToUpdate) {
        this.userInfoService = userInfoService;
        this.userInfoToUpdate = userInfoToUpdate;
    }

    @Override
    public void run() {
        changeToNull(userInfoToUpdate);
    }

    public void changeToNull(UserInfo userInfo) {
        userInfo.getUserAuthorization().setResetToken(null);
        userInfoService.update(userInfo, userInfo);
    }
}
