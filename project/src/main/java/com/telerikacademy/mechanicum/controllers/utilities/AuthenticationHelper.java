package com.telerikacademy.mechanicum.controllers.utilities;

import com.telerikacademy.mechanicum.exceptions.AuthenticationFailureException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.services.contracts.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String WRONG_USERNAME_AND_OR_PASSWORD = "Wrong username and/or password";
    private static final String USER_NOT_LOGGED = "No logged in user";
    private static final String AUTHORIZATION_REQUIRED = "The requested resource requires authentication";
    private final UserInfoService userInfoService;

    @Autowired
    public AuthenticationHelper(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    public UserInfo getUserByEmail(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTHORIZATION_REQUIRED);
        }
        String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);
        return userInfoService.getByEmail(email);
    }

    public UserInfo verifyAuthentication(String email, String pass) {
        try {
            UserInfo userInfo = userInfoService.getByEmail(email);
            if (!userInfo.getUserAuthorization().getPassword().equals(pass)) {
                throw new AuthenticationFailureException(WRONG_USERNAME_AND_OR_PASSWORD);
            }
            return userInfo;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(WRONG_USERNAME_AND_OR_PASSWORD);
        }
    }

    public UserInfo getUserByEmail(HttpSession session) {
        String email = (String) session.getAttribute("loggedUserEmail");
        if (email == null) {
            throw new UnauthorizedOperationException(USER_NOT_LOGGED);
        }
        try {
            return userInfoService.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException(USER_NOT_LOGGED);
        }
    }

}