package com.telerikacademy.mechanicum.controllers.rest;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.models.dto.CustomerSortParameters;
import com.telerikacademy.mechanicum.services.contracts.UserInfoService;
import com.telerikacademy.mechanicum.services.contracts.VisitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Api(tags = "Customers")
@RestController
@RequestMapping("/api/mechanicum/customers")
public class CustomerController {

    private final UserInfoService userInfoService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;
    private final VisitService visitService;

    @Autowired
    public CustomerController(UserInfoService userInfoService,
                              AuthenticationHelper authenticationHelper,
                              ModelMapper modelMapper,
                              VisitService visitService) {
        this.userInfoService = userInfoService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.visitService = visitService;
    }

    @GetMapping
    @ApiOperation(value = "Get all customers",
            notes = "Only accessible from employees",
            response = UserInfo.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<UserInfo> getAll(@RequestHeader HttpHeaders headers) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return userInfoService.getAllCustomers(personCalledMethod);
    }

    @PostMapping("/filter")
    @ApiOperation(value = "Filters all customers by name, email, phone number,\n" +
            "vehicle (model or make) and/or visits in range",
            notes = "Only accessible from employees",
            response = UserInfo.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<UserInfo> filter(@RequestHeader HttpHeaders headers,
                                 @RequestBody Map<String, String> userInfoFilterDtoMap) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        List<Visit> visitList = userInfoService.filter(userInfoFilterDtoMap, personCalledMethod);
        return modelMapper.fromVisitsToCustomers(visitList);
    }

    @PostMapping("/sort")
    @ApiOperation(value = "Sorts visits/customers??? by first name or arrival date of visit ",
            notes = "Only accessible from employees",
            response = UserInfo.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<UserInfo> sort(@RequestHeader HttpHeaders headers,
                               @Valid @RequestBody CustomerSortParameters customerSortParameters) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        Map<String, String> parameters = modelMapper.fromDtoToMap(customerSortParameters);
        List<Visit> visitList = userInfoService.sortByNameOrVisitDate(parameters, personCalledMethod);
        return modelMapper.fromVisitsToCustomers(visitList);
    }

    @GetMapping("/{customerID}/filter")
    @ApiOperation(value = "Finds all customer's visits",
            notes = "Only accessible from employees or specific customer",
            response = Visit.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<Visit> getLinkedVisits(@RequestHeader HttpHeaders headers,
                                       @Valid @PathVariable int customerID,
                                       @RequestParam Optional<Integer> vehicleId,
                                       @RequestParam Optional<String> date) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return visitService.getLinkedServices(customerID, personCalledMethod, vehicleId, date);
    }

    @GetMapping("/search")
    @ApiOperation(value = "Search customers by first or last name or email ",
            notes = "Only accessible from employees",
            response = UserInfo.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<UserInfo> search(@RequestHeader HttpHeaders headers,
                                 @RequestParam String name) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return userInfoService.getByName(name, personCalledMethod);
    }
}
