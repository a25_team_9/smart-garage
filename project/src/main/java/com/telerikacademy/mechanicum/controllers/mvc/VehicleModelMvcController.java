package com.telerikacademy.mechanicum.controllers.mvc;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.models.Manufacturer;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.VehicleModel;
import com.telerikacademy.mechanicum.models.dto.VehicleModelDto;
import com.telerikacademy.mechanicum.services.contracts.VehicleModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/model")
public class VehicleModelMvcController {
    private final VehicleModelService modelService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public VehicleModelMvcController(VehicleModelService modelService,
                                     ModelMapper modelMapper,
                                     AuthenticationHelper authenticationHelper) {
        this.modelService = modelService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping("/create")
    public String createModel(@Valid @ModelAttribute("modelDto") VehicleModelDto modelDto,
                              BindingResult bindingResult,
                              Model model,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "redirect:/vehicles";
        }
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        VehicleModel newModel = modelMapper.fromDtoToModel(modelDto);
        try {
            modelService.create(newModel, personCalledMethod);
        } catch (DuplicateEntityException ignored) {
        }
        return "redirect:/vehicles";
    }

}
