package com.telerikacademy.mechanicum.controllers.rest;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.dto.UserInfoDto;
import com.telerikacademy.mechanicum.models.dto.UserInfoUpdateDto;
import com.telerikacademy.mechanicum.services.contracts.UserInfoService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "Users")
@RestController
@RequestMapping("/api/mechanicum/users")
public class UserInfoController {
    private final UserInfoService userInfoService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserInfoController(UserInfoService userInfoService,
                              ModelMapper modelMapper,
                              AuthenticationHelper authenticationHelper) {
        this.userInfoService = userInfoService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Get all users",
            notes = "Only accessible from employees",
            response = UserInfo.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<UserInfo> getAll(@RequestHeader HttpHeaders headers) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return userInfoService.getAll(personCalledMethod);
    }

    @GetMapping("/{userId}")
    @ApiOperation(value = "Get user by ID",
            notes = "Only accessible from employees",
            response = UserInfo.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public UserInfo getById(@RequestHeader HttpHeaders headers,
                            @ApiParam(value = "ID of the user you want to get")
                            @Valid @PathVariable int userId) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return userInfoService.getByID(userId, personCalledMethod);
    }

    @PostMapping
    @ApiOperation(value = "Register user as a customer or employee.",
            notes = "Employee can register users",
            response = UserInfo.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization",
                    dataTypeClass = HttpHeaders.class,
                    required = true,
                    paramType = "header",
                    example = "ggbg@gmail.com",
                    value = "See example below"),
            @ApiImplicitParam(name = "Parameters",
                    dataTypeClass = UserInfoDto.class,
                    required = true,
                    paramType = "body",
                    value = "Send in JSON format")
    })
    public UserInfo create(@RequestHeader HttpHeaders headers,
                           @ApiIgnore @Valid @RequestBody UserInfoDto userInfoDto) {
        UserInfo personCalledCreate = authenticationHelper.getUserByEmail(headers);
        UserInfo userToCreate = modelMapper.fromDtoToUser(userInfoDto);
        return userInfoService.create(userToCreate, personCalledCreate);
    }

    @PutMapping("/{userInfoId}")
    @ApiOperation(value = "Update users information",
            notes = "Accessible from user with that ID and employee",
            response = UserInfo.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization",
                    dataTypeClass = HttpHeaders.class,
                    required = true,
                    paramType = "header",
                    example = "ggbg@gmail.com",
                    value = "See example below"),
            @ApiImplicitParam(name = "Parameters",
                    dataTypeClass = UserInfoUpdateDto.class,
                    required = true,
                    paramType = "body",
                    value = "Send in JSON format")
    })
    public UserInfo update(@RequestHeader HttpHeaders headers,
                           @ApiParam(value = "ID of the user to update", example = "3")
                           @PathVariable int userInfoId,
                           @ApiIgnore @Valid @RequestBody UserInfoUpdateDto userInfoUpdateDto) {
        UserInfo personCalledUpdate = authenticationHelper.getUserByEmail(headers);
        UserInfo customer = modelMapper.fromDtoToCustomer(userInfoUpdateDto, userInfoId);
        return userInfoService.update(customer, personCalledUpdate);

    }

    @DeleteMapping("/{userInfoID}")
    @ApiOperation(value = "Delete user",
            notes = "Accessible from user with that ID and employee")
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public void delete(@RequestHeader HttpHeaders headers,
                       @ApiParam(value = "ID of the user to delete", example = "3")
                       @PathVariable int userInfoID) {
        UserInfo personCalledDelete = authenticationHelper.getUserByEmail(headers);
        userInfoService.delete(userInfoID, personCalledDelete);
    }

}
