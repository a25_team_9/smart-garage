package com.telerikacademy.mechanicum.controllers.mvc;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.ServiceType;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.dto.DeleteServiceDto;
import com.telerikacademy.mechanicum.models.dto.ServiceDto;
import com.telerikacademy.mechanicum.models.dto.ServiceTypeDto;
import com.telerikacademy.mechanicum.models.dto.ServiceTypeFilterDto;
import com.telerikacademy.mechanicum.services.contracts.ServiceService;
import com.telerikacademy.mechanicum.services.contracts.ServiceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Map;

@Controller
@RequestMapping("/services")
public class ServiceMvcController {
    private final ServiceTypeService serviceTypeService;
    private final ServiceService serviceService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;

    @Autowired
    public ServiceMvcController(ServiceTypeService serviceTypeService,
                                ServiceService serviceService,
                                AuthenticationHelper authenticationHelper,
                                ModelMapper modelMapper) {
        this.serviceTypeService = serviceTypeService;
        this.serviceService = serviceService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public String showAllTypes(Model model, HttpSession session) {
        try {
            UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
            model.addAttribute("currentUser", personCalledMethod);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }
        model.addAttribute("serviceTypes", serviceTypeService.getAll());
        model.addAttribute("serviceTypeFilterDto", new ServiceTypeFilterDto());
        model.addAttribute("dto", new ServiceTypeDto());
        model.addAttribute("id", serviceTypeService.getById(1));
        return "services";
    }

    @PostMapping("/create")
    public String createServiceType(@Valid @ModelAttribute("dto") ServiceTypeDto dto,
                                    BindingResult bindingResult,
                                    Model model,
                                    HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        if (bindingResult.hasErrors()) {
            return "errors/bad-request";
        }
        model.addAttribute("dto", new ServiceTypeDto());
        ServiceType serviceTypeToCreate = modelMapper.fromDtoToServiceType(dto);
        serviceTypeService.create(serviceTypeToCreate, personCalledMethod);
        return "redirect:/services";
    }

    @PostMapping("/update")
    public String updateServiceType(@ModelAttribute("dto") ServiceTypeDto dto,
                                    BindingResult bindingResult,
                                    Model model,
                                    HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        if (bindingResult.hasErrors()) {
            return "errors/bad-request";
        }
        model.addAttribute("dto", new ServiceTypeDto());
        ServiceType serviceTypeToUpdate = serviceTypeService.getByName(dto.getName());
        serviceTypeToUpdate.setSinglePrice(dto.getSinglePrice());
        serviceTypeService.update(serviceTypeToUpdate, personCalledMethod);
        return "redirect:/services";
    }

    @PostMapping("/delete")
    public String deleteServiceType(@ModelAttribute("id") ServiceType id, Model model, HttpSession session,
                                    BindingResult bindingResult) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        if (bindingResult.hasErrors()) {
            return "errors/bad-request";
        }
        serviceTypeService.delete(id.getId(), personCalledMethod);
        return "redirect:/services";
    }

    @PostMapping("/visit/delete/{id}")
    public String deleteServiceFromVisit(@ModelAttribute("deleteServiceDto") DeleteServiceDto deleteServiceDto, BindingResult bindingResult,
                                         @PathVariable Integer id,
                                         Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        if (bindingResult.hasErrors()) {
            return String.format("redirect:/visits/%d", id);
        }
        serviceService.delete(deleteServiceDto.getId(), personCalledMethod);
        return String.format("redirect:/visits/%d", id);
    }


    @PostMapping("/create/{id}")
    public String createServiceForVisit(@ModelAttribute("serviceCreateDto") ServiceDto serviceCreateDto,
                                        BindingResult bindingResult, @PathVariable Integer id,
                                        HttpSession session, Model model) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        if (bindingResult.hasErrors()) {
            return String.format("redirect:/visits/%d", id);
        }
        Service serviceToCreate = modelMapper.fromDtoToService(serviceCreateDto);
        try {
            serviceService.create(serviceToCreate, personCalledMethod);
        } catch (DuplicateEntityException dex) {
            return String.format("redirect:/visits/%d", id);
        }
        return String.format("redirect:/visits/%d", id);
    }

    @PostMapping("/filter")
    public String filterServicesOffered(@ModelAttribute("serviceTypeFilterDto") ServiceTypeFilterDto dto, Model model,
                                        BindingResult bindingResult, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        if (bindingResult.hasErrors()) {
            return "errors/bad-request";
        }
        genereateModels(model, personCalledMethod, new ServiceTypeDto(), dto);
        Map<String, String> serviceTypeToFilter = modelMapper.fromDtoToMap(dto);
        try {
            model.addAttribute("serviceTypes", serviceTypeService.filter(serviceTypeToFilter));
            model.addAttribute("serviceTypeFilterDto", new ServiceTypeFilterDto());
        } catch (InvalidFieldException ife) {
            model.addAttribute("filterError", ife.getMessage());
            return "services";
        }

        return "services";
    }

    private void genereateModels(Model model,
                                 UserInfo personCalledMethod,
                                 ServiceTypeDto dto,
                                 ServiceTypeFilterDto serviceTypeFilterDto) {
        model.addAttribute("currentUser", personCalledMethod);
        model.addAttribute("serviceTypeFilterDto", dto);
        model.addAttribute("dto", new ServiceTypeDto());
        model.addAttribute("id", serviceTypeService.getById(1));

    }
}
