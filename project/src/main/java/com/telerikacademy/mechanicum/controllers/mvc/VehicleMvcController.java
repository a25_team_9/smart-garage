package com.telerikacademy.mechanicum.controllers.mvc;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Vehicle;
import com.telerikacademy.mechanicum.models.dto.*;
import com.telerikacademy.mechanicum.services.contracts.ManufacturerService;
import com.telerikacademy.mechanicum.services.contracts.UserInfoService;
import com.telerikacademy.mechanicum.services.contracts.VehicleModelService;
import com.telerikacademy.mechanicum.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Map;

@Controller
@RequestMapping("/vehicles")
public class VehicleMvcController {

    private final VehicleService vehicleService;
    private final ManufacturerService manufacturerService;
    private final VehicleModelService modelService;
    private final UserInfoService userInfoService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;

    @Autowired
    public VehicleMvcController(VehicleService vehicleService,
                                ManufacturerService manufacturerService,
                                VehicleModelService modelService,
                                UserInfoService userInfoService,
                                AuthenticationHelper authenticationHelper,
                                ModelMapper modelMapper) {
        this.vehicleService = vehicleService;
        this.manufacturerService = manufacturerService;
        this.modelService = modelService;
        this.userInfoService = userInfoService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showVehicles(Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        getAllModelsForVehicles(model, personCalledMethod);
        return "vehicles";
    }

    @GetMapping("/{vehicleId}")
    public String showSingleVehicle(@PathVariable Integer vehicleId, Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        try {
            Vehicle singleVehicle = vehicleService.getById(vehicleId, personCalledMethod);
            getSingleViewModels(singleVehicle, personCalledMethod, model);
        } catch (NumberFormatException nfe) {
            model.addAttribute("error", nfe.getMessage());
            return "errors/bad-request";
        }
        return "vehicle";
    }

    @PostMapping("/create")
    public String createPersonalVehicle(@Valid @ModelAttribute("vehicleDto") VehicleDto dto,
                                        BindingResult bindingResult, Model model, HttpSession session) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(session);
        if (bindingResult.hasErrors()) {
            getAllModelsForVehicles(model, currentUser);
            model.addAttribute("vehicleDto", dto);
            return "vehicles";
        }
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        Vehicle newVehicle = modelMapper.fromDtoToVehicle(dto);
        vehicleService.create(newVehicle, personCalledMethod);
        return String.format("redirect:/vehicles/%d", newVehicle.getId());
    }

    @PostMapping("/update/{id}")
    public String updateVehicle(@PathVariable int id, @Valid @ModelAttribute("updateDto") VehicleUpdateDto dto,
                                BindingResult bindingResult, Model model, HttpSession session) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(session);
        Vehicle vehicleToUpdate = modelMapper.fromUpdateDto(dto, id);
        vehicleService.update(vehicleToUpdate, currentUser);
        return String.format("redirect:/profile/%d", vehicleToUpdate.getOwner().getUserID());
    }

    @PostMapping("/delete/{id}")
    public String deleteVehicles(@PathVariable Integer id, Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        Vehicle vehicle = vehicleService.getById(id, personCalledMethod);

        try {
            vehicleService.delete(id, personCalledMethod);
        } catch (InvalidFieldException ife) {
            model.addAttribute("deleteError", ife);
            getSingleViewModels(vehicle, personCalledMethod, model);
            return "vehicles";
        }
        getAllModelsForVehicles(model, personCalledMethod);
        return "redirect:/vehicles";
    }

    @PostMapping("/filter")
    public String filterByOwner(@ModelAttribute VehicleSearchParameters ownerId,
                                Model model, HttpSession session) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(session);
        model.addAttribute("currentUser", currentUser);
        Map<String, String> map = modelMapper.fromDtoToMap(ownerId);
        getAllModelsForVehicles(model, currentUser);
        if (map.containsValue("0")) {
            model.addAttribute("vehicles", vehicleService.getAll(currentUser));
        } else {
            model.addAttribute("vehicles", vehicleService.filter(map, currentUser));
        }
        return "vehicles";
    }

    private void getAllModelsForVehicles(Model model, UserInfo personCalledMethod) {
        model.addAttribute("vehicles", vehicleService.getAll(personCalledMethod));
        model.addAttribute("customers", userInfoService.getAllCustomers(personCalledMethod));
        model.addAttribute("vehicleCatalogue", modelService.getAll(personCalledMethod));
        model.addAttribute("modelDto", new VehicleModelDto());
        model.addAttribute("makeDto", new ManufacturerDto());
        model.addAttribute("filterDto", new VehicleSearchParameters());
        model.addAttribute("vehicleDto", new VehicleDto());
        model.addAttribute("manufacturers", manufacturerService.getAll(personCalledMethod));
        model.addAttribute("currentUser", personCalledMethod);
    }

    private void getSingleViewModels(Vehicle vehicle, UserInfo personCalledMethod, Model model) {
        model.addAttribute("vehicle", vehicle);
        model.addAttribute("currentUser", personCalledMethod);
        model.addAttribute("updateDto", new VehicleUpdateDto());
        if(personCalledMethod.getUserAuthorization().isEmployee()) {
            model.addAttribute("customers", userInfoService.getAllCustomers(personCalledMethod));
        }else {
            model.addAttribute("customers",new ArrayList<>());
        }
        model.addAttribute("services", vehicleService.getServicesByCarId(vehicle.getId(), personCalledMethod));
    }



}
