package com.telerikacademy.mechanicum.controllers.rest;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Vehicle;
import com.telerikacademy.mechanicum.models.dto.VehicleDto;
import com.telerikacademy.mechanicum.models.dto.VehicleSearchParameters;
import com.telerikacademy.mechanicum.services.contracts.VehicleService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Api(tags = "Personal vehicles")
@RestController
@RequestMapping("/api/mechanicum/vehicles")
public class VehicleController {

    private final VehicleService vehicleService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;

    @Autowired
    public VehicleController(VehicleService vehicleService,
                             AuthenticationHelper authenticationHelper,
                             ModelMapper modelMapper) {
        this.vehicleService = vehicleService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @ApiOperation(value = "Get all vehicles",
            notes = "Only accessible from employees",
            response = Vehicle.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<Vehicle> getAll(@RequestHeader HttpHeaders headers) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(headers);
        return vehicleService.getAll(currentUser);
    }

    @GetMapping("/{vehicleId}")
    @ApiOperation(value = "Get vehicle by ID",
            notes = "Accessible from customer owns vehicle with that ID and employee",
            response = Vehicle.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public Vehicle getById(@RequestHeader HttpHeaders headers,
                           @ApiParam(value = "ID of the vehicle you are looking for", example = "3")
                           @PathVariable int vehicleId) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(headers);
        return vehicleService.getById(vehicleId, currentUser);
    }

    @PostMapping
    @ApiOperation(value = "Register vehicle for a specific customer",
            notes = "Employee can register vehicles",
            response = Vehicle.class)
    @ApiImplicitParam(name = "Parameters",
            dataTypeClass = VehicleDto.class,
            required = true,
            paramType = "body",
            value = "Send in JSON format")
    public Vehicle create(@RequestHeader HttpHeaders headers,
                          @ApiIgnore @Valid @RequestBody VehicleDto dto) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(headers);
        Vehicle vehicle = modelMapper.fromDtoToVehicle(dto);
        return vehicleService.create(vehicle, currentUser);
    }

    @PutMapping("/{vehicleId}")
    @ApiOperation(value = "Update vehicle",
            notes = "Accessible from customer owns vehicle with that ID and employee",
            response = Vehicle.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization",
                    dataTypeClass = HttpHeaders.class,
                    required = true,
                    paramType = "header",
                    example = "ggbg@gmail.com",
                    value = "See example below"),
            @ApiImplicitParam(name = "Parameters",
                    dataTypeClass = VehicleDto.class,
                    required = true,
                    paramType = "body",
                    value = "Send in JSON format")
    })
    public Vehicle update(@RequestHeader HttpHeaders headers,
                          @ApiParam(value = "ID of the vehicle to update", example = "2")
                          @PathVariable int vehicleId,
                          @ApiIgnore @Valid @RequestBody VehicleDto dto) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(headers);
        Vehicle vehicle = modelMapper.fromDto(dto, vehicleId);
        vehicleService.update(vehicle, currentUser);
        return vehicle;
    }

    @DeleteMapping("/{vehicleId}")
    @ApiOperation(value = "Delete visit by visit ID",
            notes = "Accessible only for employee")
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public void delete(@RequestHeader HttpHeaders headers,
                       @ApiParam(value = "ID of the vehicle to delete", example = "3")
                       @PathVariable int vehicleId) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(headers);
        vehicleService.delete(vehicleId, currentUser);
    }

    @PostMapping("/filter")
    @ApiOperation(value = "Filter vehicle by given customer ID",
            notes = "Only accessible from employees or specific customer",
            response = Vehicle.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<Vehicle> filter(@RequestHeader HttpHeaders headers,
                                @ApiParam(value = "Give customer ID")
                                @Valid @RequestBody VehicleSearchParameters parameters) {
        UserInfo currentUser = authenticationHelper.getUserByEmail(headers);
        Map<String, String> filterMap = modelMapper.fromDtoToMap(parameters);
        return vehicleService.filter(filterMap, currentUser);
    }
}
