package com.telerikacademy.mechanicum.controllers.mvc;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.exceptions.AuthenticationFailureException;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.dto.ForgottenPasswordDto;
import com.telerikacademy.mechanicum.models.dto.UserAuthorizationDto;
import com.telerikacademy.mechanicum.models.dto.UserInfoMvcCreateDto;
import com.telerikacademy.mechanicum.services.contracts.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping
public class AuthenticationMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final UserInfoService userInfoService;
    private final ModelMapper modelMapper;

    @Autowired
    public AuthenticationMvcController(AuthenticationHelper authenticationHelper,
                                       UserInfoService userInfoService,
                                       ModelMapper modelMapper) {
        this.authenticationHelper = authenticationHelper;
        this.userInfoService = userInfoService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model, HttpSession session) {
        if (session.getAttribute("loggedUserEmail") != null) {
            return "redirect:/";
        }
        model.addAttribute("loginDto", new UserAuthorizationDto());
        model.addAttribute("forgottenPassword", new ForgottenPasswordDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("loginDto") UserAuthorizationDto loginDto,
                              BindingResult bindingResult,
                              Model model,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("loginDto", loginDto);
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(loginDto.getEmail(), loginDto.getPassword());
            session.setAttribute("loggedUserEmail", loginDto.getEmail());
            return "redirect:/";
        } catch (AuthenticationFailureException afe) {
            model.addAttribute("loginError", afe.getMessage());
            model.addAttribute("loginDto", loginDto);
            model.addAttribute("forgottenPassword", new ForgottenPasswordDto());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("loggedUserEmail");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model, HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        if (!personCalledMethod.getUserAuthorization().isEmployee()) {
            return "redirect:/";
        }
        model.addAttribute("registerDto", new UserInfoMvcCreateDto());
        model.addAttribute("currentUser", personCalledMethod);
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("registerDto") UserInfoMvcCreateDto registerDto,
                                 BindingResult bindingResult,
                                 Model model,
                                 HttpSession session) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        model.addAttribute("registerDto", registerDto);
        model.addAttribute("currentUser", personCalledMethod);
        if (bindingResult.hasErrors()) {
            return "register";
        }
        UserInfo userInfo = null;
        try {
            userInfo = modelMapper.fromDtoToUser(registerDto);
            userInfoService.create(userInfo, personCalledMethod);
        } catch (Exception e) {
            model.addAttribute("errors", e.getMessage());
            return "register";
        }

        return String.format("redirect:/profile/%d", userInfo.getUserID());
    }

}
