package com.telerikacademy.mechanicum.controllers.mvc;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.services.contracts.ServiceTypeService;
import com.telerikacademy.mechanicum.services.contracts.UserInfoService;
import com.telerikacademy.mechanicum.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {
    public static final int ALL_EMPLOYEES_OF_COMPANY = 130;
    private final ServiceTypeService serviceTypeService;
    private final UserInfoService userInfoService;
    private final AuthenticationHelper authenticationHelper;
    private final VehicleService vehicleService;


    @Autowired
    public HomeMvcController(ServiceTypeService serviceTypeService,
                             UserInfoService userInfoService,
                             AuthenticationHelper authenticationHelper,
                             VehicleService vehicleService) {
        this.serviceTypeService = serviceTypeService;
        this.userInfoService = userInfoService;
        this.authenticationHelper = authenticationHelper;
        this.vehicleService = vehicleService;
    }

    @GetMapping
    public String showHome(Model model, HttpSession session) {
        try {
            UserInfo currentUser = authenticationHelper.getUserByEmail(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }
        model.addAttribute("allEmployeesCount", ALL_EMPLOYEES_OF_COMPANY);//Change if appropriate
        model.addAttribute("customersCount", userInfoService.getAllCustomersCount());
        model.addAttribute("serviceTypes", serviceTypeService.getAll().size());
        model.addAttribute("vehicleCount", vehicleService.getAllVehiclesCount());
        return "index";
    }

    @GetMapping("/navbar")
    private String putUserIntoNavBar(Model model, HttpSession session, UserInfo currentUser) {
        model.addAttribute("currentUser", currentUser);
        return "fragments/navigation";
    }


}
