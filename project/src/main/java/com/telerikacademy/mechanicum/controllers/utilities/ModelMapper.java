package com.telerikacademy.mechanicum.controllers.utilities;

import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.models.Manufacturer;
import com.telerikacademy.mechanicum.models.Role;
import com.telerikacademy.mechanicum.models.UserAuthorization;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.*;
import com.telerikacademy.mechanicum.models.dto.*;
import com.telerikacademy.mechanicum.repositories.contracts.*;
import com.telerikacademy.mechanicum.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.mechanicum.repositories.contracts.RoleRepository;
import com.telerikacademy.mechanicum.repositories.contracts.UserInfoRepository;
import com.telerikacademy.mechanicum.services.contracts.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.*;

@Component
public class ModelMapper {

    private final UserInfoRepository userInfoRepository;
    private final ManufacturerRepository manufacturerRepository;
    private final RoleRepository roleRepository;
    private final VehicleModelRepository vehicleModelRepository;
    private final VehicleRepository vehicleRepository;
    private final AuthenticationHelper authenticationHelper;
    private final CurrencyRepository currencyRepository;
    private final VisitRepository visitRepository;
    private final ServiceStatusRepository statusRepository;
    private final ServiceTypeRepository serviceTypeRepository;
    private final ServiceRepository serviceRepository;
    private final GenerateSecurePassword password;

    @Autowired
    public ModelMapper(UserInfoRepository userInfoRepository,
                       ManufacturerRepository manufacturerRepository,
                       RoleRepository roleRepository,
                       VehicleModelRepository vehicleModelRepository,
                       VehicleRepository vehicleRepository,
                       AuthenticationHelper authenticationHelper,
                       CurrencyRepository currencyRepository,
                       VisitRepository visitRepository,
                       ServiceStatusRepository statusRepository,
                       ServiceTypeRepository serviceTypeRepository,
                       ServiceRepository serviceRepository,
                       GenerateSecurePassword password) {
        this.userInfoRepository = userInfoRepository;
        this.manufacturerRepository = manufacturerRepository;
        this.roleRepository = roleRepository;
        this.vehicleModelRepository = vehicleModelRepository;
        this.vehicleRepository = vehicleRepository;
        this.authenticationHelper = authenticationHelper;
        this.currencyRepository = currencyRepository;
        this.visitRepository = visitRepository;
        this.statusRepository = statusRepository;
        this.serviceTypeRepository = serviceTypeRepository;
        this.serviceRepository = serviceRepository;
        this.password = password;
    }

    public Visit fromDtoToVisit(VisitDto visitDto) {
        Visit visit = new Visit();
        visit.setArrivalDate(LocalDate.now());
        visit.setCurrency(currencyRepository.getById(visitDto.getCurrencyId()));
        visit.setVehicle(vehicleRepository.getById(visitDto.getVehicleId()));
        return visit;
    }

    public Visit fromDtoToVisit(VisitDto visitDto, int visitId) {
        Visit visitToUpdate = visitRepository.getByID(visitId);
        visitToUpdate.setCurrency(currencyRepository.getById(visitDto.getCurrencyId()));
        visitToUpdate.setPaid(visitDto.isPaid());
        visitToUpdate.setVehicle(vehicleRepository.getById(visitDto.getVehicleId()));
        return visitToUpdate;
    }

    public Visit fromDtoToVisit(VisitUpdateDto visitUpdateDto, int visitId) {
        Visit visitToUpdate = visitRepository.getByID(visitId);
        if (!(visitUpdateDto.isPaid() == null)) {
            visitToUpdate.setPaid(visitUpdateDto.isPaid());
        }
        visitToUpdate.setCurrency(currencyRepository.getById(visitUpdateDto.getCurrencyId()));

        return visitToUpdate;
    }

    public UserInfo fromDtoToUser(UserInfoDto userInfoDto) {
        UserInfo userInfo = new UserInfo();
        Set<Role> roles = new HashSet<>();
        if (userInfoDto.getRolesDto().isEmpty()) {
            throw new InvalidFieldException("Please add roles to user");
        }
        for (RoleDto dtoRole : userInfoDto.getRolesDto()) {
            Role role = roleRepository.getByID(dtoRole.getRoleId());
            roles.add(role);
        }
        var generatePassword = password.returnSecuredPassword();
        UserAuthorization userAuthorization = new UserAuthorization(userInfoDto.getUserAuthorizationDto().getEmail(),
                generatePassword, roles);
        userInfo.setUserAuthorization(userAuthorization);
        userInfo.setAddress(userInfoDto.getAddress());
        userInfo.setFirstName(userInfoDto.getFirstName());
        userInfo.setLastName(userInfoDto.getLastName());
        userInfo.setPhone(checkNumber(userInfoDto.getPhone()));
        return userInfo;
    }

    public UserInfo fromDtoToCustomer(UserInfoUpdateDto userInfoUpdateDto, int userInfoID) {
        UserInfo userToUpdate = userInfoRepository.getByID(userInfoID);
        //check passwords match !!
        checkPasswordsMatch(userInfoUpdateDto.getPassword(), userInfoUpdateDto.getRepeatPassword());
        if (userInfoUpdateDto.getAddress() != null) {
            userToUpdate.setAddress(userInfoUpdateDto.getAddress());
        }
        userToUpdate.getUserAuthorization().setPassword(userInfoUpdateDto.getPassword());
        userToUpdate.setPhone(checkNumber(userInfoUpdateDto.getPhone()));
        userToUpdate.setFirstName(userInfoUpdateDto.getFirstName());
        userToUpdate.setLastName(userInfoUpdateDto.getLastName());

        return userToUpdate;
    }

    public UserInfo fromDtoToUser(UserInfoMvcCreateDto registerDto) {
        UserInfo userInfo = new UserInfo();
        Set<Role> roles = new HashSet<>();
        if (!registerDto.isCustomer() && !registerDto.isEmployee()) {
            throw new InvalidFieldException("Choose atleast one role!");
        }
        if (registerDto.isCustomer()) {
            roles.add(roleRepository.getByID(1));
        }
        if (registerDto.isEmployee()) {
            roles.add(roleRepository.getByID(2));
        }
        UserAuthorization userAuthorization = new UserAuthorization(registerDto.getEmail(),
                password.returnSecuredPassword(), roles);
        userInfo.setUserAuthorization(userAuthorization);
        if (registerDto.getAddress().isEmpty()) {
            userInfo.setAddress(null);
        } else {
            userInfo.setAddress(registerDto.getAddress());
        }
        userInfo.setFirstName(registerDto.getFirstName());
        userInfo.setLastName(registerDto.getLastName());
        userInfo.setPhone(checkNumber(registerDto.getPhone()));
        return userInfo;

    }

    public Manufacturer fromDtoToManufacturer(ManufacturerDto dto) {
        Manufacturer make = new Manufacturer();
        make.setName(dto.getName());
        return make;
    }

    public VehicleModel fromDtoToModel(VehicleModelDto dto) {
        VehicleModel model = new VehicleModel();
        Manufacturer manufacturer = manufacturerRepository.getById(dto.getManufacturerId());
        model.setModel(dto.getModel());
        model.setManufacturer(manufacturer);
        return model;
    }

    public Service fromDtoToService(ServiceDto serviceDto) {
        Service service = new Service();
        dtoToObject(serviceDto, service);
        return service;
    }

    public Vehicle fromDtoToVehicle(VehicleDto dto) {
        Vehicle vehicle = new Vehicle();
        dtoToObject(dto, vehicle);
        return vehicle;
    }

    public Vehicle fromDto(VehicleDto dto, int id) {
        Vehicle vehicle = vehicleRepository.getById(id);
        dtoToObject(dto, vehicle);
        return vehicle;
    }

    public Vehicle fromUpdateDto(VehicleUpdateDto dto, int id) {
        Vehicle vehicle = vehicleRepository.getById(id);
        dtoToObject(dto, vehicle);
        return vehicle;
    }

    public Service fromDto(ServiceDto dto, int serviceId) {
        Service service = serviceRepository.getById(serviceId);
        if (service.getServiceStatus().getId() == 3) {
            throw new InvalidFieldException("Cannot update finished service");
        }
        if (service.getVisit().getId() != dto.getVisitId()) {
            throw new InvalidFieldException("Service with id does not match expected visit id ");
        }
        dtoToObject(dto, service);
        return service;
    }

    public ServiceType fromDtoToServiceType(ServiceTypeDto serviceTypeDto) {
        ServiceType serviceType = new ServiceType();
        dtoToServiceType(serviceType, serviceTypeDto);
        return serviceType;
    }

    public Map<String, String> fromDtoToMap(UserInfoFilterDto userInfoFilterDto) {
        Map<String, String> result = new HashMap<>();
        result.put("email", userInfoFilterDto.getEmail() == null ? "" : userInfoFilterDto.getEmail());
        result.put("firstName", userInfoFilterDto.getFirstName() == null ? "" : userInfoFilterDto.getFirstName());
        result.put("phone", userInfoFilterDto.getPhone() == null ? "" : userInfoFilterDto.getPhone());
        result.put("from", userInfoFilterDto.getFrom() == null ? "" : userInfoFilterDto.getFrom());
        result.put("to", userInfoFilterDto.getTo() == null ? "" : userInfoFilterDto.getTo());
        result.put("model", userInfoFilterDto.getModelId());
        result.put("make", userInfoFilterDto.getMakeId());
        return result;
    }

    public Map<String, String> fromDtoToMap(ServiceTypeDto serviceTypeDto) {
        Map<String, String> result = new HashMap<>();
        result.put("name", serviceTypeDto.getName());
        result.put("singlePrice", String.valueOf(serviceTypeDto.getSinglePrice()));
        return result;
    }

    public Map<String, String> fromDtoToMap(ServiceTypeFilterDto serviceTypeDto) {
        Map<String, String> result = new HashMap<>();
        result.put("name", serviceTypeDto.getName());
        result.put("singlePrice", serviceTypeDto.getSinglePrice());
        return result;
    }

    public Map<String, String> fromDtoToMap(VehicleSearchParameters parameters) {
        Map<String, String> result = new HashMap<>();
        result.put("ownerId", parameters.getOwnerId().toString());
        return result;
    }

    public List<UserInfo> fromVisitsToCustomers(List<Visit> sortByNameOrVisitDate) {
        List<UserInfo> result = new ArrayList<>();
        for (Visit visit : sortByNameOrVisitDate) {
            if (!result.contains(visit.getVehicle().getOwner())) {
                result.add(visit.getVehicle().getOwner());
            }
        }
        return result;
    }

    public Map<String, String> fromDtoToMap(CustomerSortParameters parameters) {
        Map<String, String> result = new HashMap<>();
        if (parameters.getName() != null && !parameters.getName().isEmpty()) {
            if (!parameters.getName().equalsIgnoreCase("asc") && !parameters.getName().equalsIgnoreCase("desc")) {
                throw new InvalidFieldException("name value should be asc or desc");
            }
            result.put("name", parameters.getName());
        }
        if (parameters.getVisitDate() != null && !parameters.getVisitDate().isEmpty()) {
            if (!parameters.getVisitDate().equalsIgnoreCase("asc") && !parameters.getVisitDate().equalsIgnoreCase("desc")) {
                throw new InvalidFieldException("Date value should be asc or desc");
            }
            result.put("visitDate", parameters.getVisitDate());
        }
        return result;
    }

    private void dtoToObject(VehicleDto vehicleDto, Vehicle vehicle) {
        VehicleModel model = vehicleModelRepository.getById(vehicleDto.getModelId());
        UserInfo owner = userInfoRepository.getByID(vehicleDto.getOwnerId());
        vehicle.setModel(model);
        vehicle.setYear(vehicleDto.getYear());
        vehicle.setVin(vehicleDto.getVin());
        vehicle.setLicencePlate(vehicleDto.getLicencePlate());
        vehicle.setOwner(owner);
        if (vehicleDto.getVehicleUrl() != null) {
            vehicle.setVehicleUrl(vehicleDto.getVehicleUrl());
        }
    }

    private void dtoToObject(VehicleUpdateDto vehicleUpdateDto, Vehicle vehicle) {
        UserInfo newOwner = userInfoRepository.getByID(vehicleUpdateDto.getOwnerId());
        vehicle.setYear(vehicleUpdateDto.getYear());
        vehicle.setLicencePlate(vehicleUpdateDto.getLicencePlate());
        vehicle.setOwner(newOwner);
        if (vehicleUpdateDto.getVehicleUrl() != null) {
            vehicle.setVehicleUrl(vehicleUpdateDto.getVehicleUrl());
        }
    }

    private void dtoToObject(ServiceDto serviceDto, Service service) {
        ServiceStatus status;
        if (LocalDate.now().isAfter(serviceDto.getStartDate()) || serviceDto.getStartDate().isEqual(LocalDate.now())) {
            status = statusRepository.getById(2);
        } else {
            status = statusRepository.getById(1);
        }
        ServiceType serviceType = serviceTypeRepository.getById(serviceDto.getServiceTypeId());
        Visit visit = visitRepository.getByID(serviceDto.getVisitId());
        if (serviceDto.getStartDate().isBefore(visit.getArrivalDate())) {
            throw new InvalidFieldException("Service start date must be on or after the Visit arrival date!");
        }
        service.setStartDate(serviceDto.getStartDate());
        if (serviceDto.getEndDate().isBefore(serviceDto.getStartDate())) {
            throw new InvalidFieldException("Service end date must be after the start date");
        }
        service.setEndDate(serviceDto.getEndDate());
        service.setQuantity(serviceDto.getQuantity());
        service.setServiceStatus(status);
        service.setServiceType(serviceType);
        service.setVisit(visit);
    }

    private void checkPasswordsMatch(String password, String repeatedPassword) {
        if (!password.equals(repeatedPassword)) {
            throw new InvalidFieldException("WRONG_USERNAME_AND_OR_PASSWORD");
        }
    }

    private void dtoToServiceType(ServiceType serviceType, ServiceTypeDto serviceTypeDto) {
        serviceType.setName(serviceTypeDto.getName());
        serviceType.setSinglePrice(serviceTypeDto.getSinglePrice());
    }

    private String checkNumber(String number) {
        if (number.matches("0\\d+")) {
            return number;
        }
        throw new InvalidFieldException("Wrong format of number. Make sure it is starting with 0 ex:(0123456789)");
    }

}
