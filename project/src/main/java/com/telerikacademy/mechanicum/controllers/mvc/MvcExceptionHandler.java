package com.telerikacademy.mechanicum.controllers.mvc;

import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class MvcExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(UnauthorizedOperationException.class)
    public ModelAndView handleUnauthorizedOperationException(UnauthorizedOperationException ex) {
        ModelAndView modelAndView = new ModelAndView("errors/not-authorized");
        modelAndView.addObject("error", ex.getMessage());
        return modelAndView;
    }

    // Going to reset page without a token redirects to login page
    @ExceptionHandler(EntityNotFoundException.class)
    public ModelAndView handleNotFound( EntityNotFoundException ex) {
        ModelAndView modelAndView = new ModelAndView("errors/not-found");
        modelAndView.addObject("error", ex.getMessage());
        return modelAndView;
    }

}
