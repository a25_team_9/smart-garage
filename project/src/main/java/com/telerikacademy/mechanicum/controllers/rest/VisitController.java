package com.telerikacademy.mechanicum.controllers.rest;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.models.dto.VisitDto;
import com.telerikacademy.mechanicum.services.contracts.VisitService;
import com.telerikacademy.mechanicum.services.contracts.UserInfoService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Api(tags = "Visits")
@RestController
@RequestMapping("/api/mechanicum/visits")
public class VisitController {
    private final UserInfoService userInfoService;
    private final VisitService visitService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;

    @Autowired
    public VisitController(UserInfoService userInfoService, ModelMapper modelMapper, AuthenticationHelper authenticationHelper, VisitService visitService) {
        this.userInfoService = userInfoService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
        this.visitService = visitService;
    }

    @GetMapping
    @ApiOperation(value = "Get all visits",
            notes = "Only accessible from employees",
            response = Visit.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<Visit> getAll(@RequestHeader HttpHeaders headers) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return visitService.getAll(personCalledMethod);
    }

    @GetMapping("/{visitId}")
    @ApiOperation(value = "Find visit by ID",
            notes = "Accessible from customer that is in the current visit or employee",
            response = Visit.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public Visit getById(@RequestHeader HttpHeaders headers,
                         @ApiParam(value = "ID of the visit you are looking for")
                         @Valid @PathVariable int visitId) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return visitService.getById(visitId, personCalledMethod);
    }

    @PostMapping
    @ApiOperation(value = "Create visit for new service about existing customer",
            notes = "Accessible only for employee",
            response = Visit.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization",
                    dataTypeClass = HttpHeaders.class,
                    required = true,
                    paramType = "header",
                    example = "ggbg@gmail.com",
                    value = "See example below"),
            @ApiImplicitParam(name = "Parameters",
                    dataTypeClass = VisitDto.class,
                    required = true,
                    paramType = "body",
                    value = "Send in JSON format")
    })
    public Visit create(@RequestHeader HttpHeaders headers,
                        @ApiIgnore @Valid @RequestBody VisitDto visitDto) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        Visit visitToCreate = modelMapper.fromDtoToVisit(visitDto);
        return visitService.create(visitToCreate, personCalledMethod);
    }

    @PutMapping("/{visitId}")
    @ApiOperation(value = "Update visit for existing service",
            notes = "Accessible only for employee",
            response = Visit.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization",
                    dataTypeClass = HttpHeaders.class,
                    required = true,
                    paramType = "header",
                    example = "ggbg@gmail.com",
                    value = "See example below"),
            @ApiImplicitParam(name = "Parameters",
                    dataTypeClass = VisitDto.class,
                    required = true,
                    paramType = "body",
                    value = "Send in JSON format")
    })
    public Visit update(@RequestHeader HttpHeaders headers,
                        @ApiParam(value = "ID of the visit to update", example = "3")
                        @PathVariable int visitId,
                        @ApiIgnore @Valid @RequestBody VisitDto visitDto) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        Visit visitToCreate = modelMapper.fromDtoToVisit(visitDto, visitId);
        return visitService.update(visitToCreate, personCalledMethod);
    }

    @DeleteMapping("/{visitId}")
    @ApiOperation(value = "Delete visit by visit ID",
            notes = "Accessible only for employee")
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public void delete(@RequestHeader HttpHeaders headers,
                       @ApiParam(value = "ID of the visit to delete", example = "3")
                       @PathVariable int visitId) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        visitService.delete(visitId, personCalledMethod);
    }

    @GetMapping("/{customerId}/history")
    @ApiOperation(value = "Show visits in defined currency for specific customer filter by number of visits",
            notes = "Only accessible from employees or specific customer",
            response = Visit.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<Visit> getCustomerVisits(@RequestHeader HttpHeaders headers,
                                         @ApiParam(value = "ID of the customer linked to the visits", example = "3")
                                         @PathVariable int customerId,
                                         @ApiParam(value = "Number of visit order by arrival date descending", example = "3")
                                         @RequestParam Optional<Integer> numberOfVisits,
                                         @ApiParam(value = "Currency type to display", example = "EUR")
                                         @RequestParam Optional<String> currency) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return visitService.getCustomerVisits(customerId, numberOfVisits, currency, personCalledMethod);
    }

    @PutMapping("/payment/{visitId}")
    @ApiOperation(value = "Update when customer paid his visit",
            notes = "Only accessible from employees",
            response = Visit.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public void updatePayment(@RequestHeader HttpHeaders headers,
                              @ApiParam(value = "ID of the visit to pay", example = "3")
                              @Valid @PathVariable int visitId) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        visitService.getPayment(visitId, personCalledMethod);
    }

    @GetMapping("/{customerID}/filter")
    @ApiOperation(value = "Filter visits by given customer ID, vehicle ID and date of arrival",
            notes = "Only accessible from employees or specific customer",
            response = Visit.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<Visit> getLinkedVisits(@RequestHeader HttpHeaders headers,
                                       @ApiParam(value = "ID of the customer linked to the visits", example = "3")
                                       @Valid @PathVariable int customerID,
                                       @ApiParam(value = "ID of the vehicle linked to the customer", example = "2")
                                       @RequestParam Optional<Integer> vehicleId,
                                       @ApiParam(value = "Date of arrival", example = "dd-MM-yyyy")
                                       @RequestParam Optional<String> arrivalDate) {
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(headers);
        return visitService.getLinkedServices(customerID, personCalledMethod, vehicleId, arrivalDate);
    }
}
