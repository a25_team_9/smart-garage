package com.telerikacademy.mechanicum.controllers.rest;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.ServiceType;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.dto.ServiceTypeDto;
import com.telerikacademy.mechanicum.services.contracts.ServiceTypeService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/mechanicum/servicetypes")
public class ServiceTypeController {
    private final ServiceTypeService serviceTypeService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ServiceTypeController(ServiceTypeService serviceService,
                                 ModelMapper modelMapper,
                                 AuthenticationHelper authenticationHelper) {
        this.serviceTypeService = serviceService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Get all services available in the shop",
            notes = "Everyone can access",
            response = Service.class)
    public List<ServiceType> getAll() {
        return serviceTypeService.getAll();
    }

    @GetMapping("/{serviceTypeId}")
    @ApiOperation(value = "Get all services available in the shop",
            notes = "Everyone can access",
            response = Service.class)
    public ServiceType getById(@PathVariable int serviceTypeId) {
        return serviceTypeService.getById(serviceTypeId);
    }

    @PostMapping
    @ApiOperation(value = "Create a new service type offered by the shop",
            notes = "Only accessible from employees",
            response = ServiceType.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public ServiceType create(@RequestHeader HttpHeaders headers,
                              @Valid @RequestBody ServiceTypeDto serviceTypeDto) {
        UserInfo personCalledCreate = getUserInfoByEmail(headers);
        ServiceType serviceType = modelMapper.fromDtoToServiceType(serviceTypeDto);
        return serviceTypeService.create(serviceType, personCalledCreate);
    }

    @PutMapping("/{serviceTypeId}")
    @ApiOperation(value = "Update an existing service type",
            notes = "Only accessible from employees",
            response = ServiceType.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public ServiceType update(@RequestHeader HttpHeaders headers,
                              @PathVariable int serviceTypeId,
                              @Valid @RequestBody ServiceTypeDto serviceTypeDto) {
        UserInfo personCalledUpdate = getUserInfoByEmail(headers);
        ServiceType serviceType = modelMapper.fromDtoToServiceType(serviceTypeDto);
        return serviceTypeService.update(serviceType, personCalledUpdate);
    }

    @PostMapping("/filter")
    @ApiOperation(value = "Create a new service type offered by the shop",
            notes = "Only accessible from employees",
            response = ServiceType.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true, paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public List<ServiceType> filter(@Valid @RequestBody ServiceTypeDto serviceTypeDto) {
        Map<String, String> filterMap = modelMapper.fromDtoToMap(serviceTypeDto);
        return serviceTypeService.filter(filterMap);
    }

    @DeleteMapping("/{serviceTypeId}")
    @ApiOperation(value = "Delete service type by service type ID",
            notes = "Accessible only for employee")
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ggbg@gmail.com",
            value = "See example below")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int serviceTypeId) {
        UserInfo personCalledDelete = getUserInfoByEmail(headers);
        serviceTypeService.delete(serviceTypeId, personCalledDelete);
    }

    private UserInfo getUserInfoByEmail(HttpHeaders headers) {
        return authenticationHelper.getUserByEmail(headers);
    }
}
