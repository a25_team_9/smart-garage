package com.telerikacademy.mechanicum.controllers.mvc;

import com.telerikacademy.mechanicum.controllers.utilities.AuthenticationHelper;
import com.telerikacademy.mechanicum.controllers.utilities.ModelMapper;
import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.models.Manufacturer;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.dto.ManufacturerDto;
import com.telerikacademy.mechanicum.models.dto.VehicleModelDto;
import com.telerikacademy.mechanicum.services.contracts.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/manufacturer")
public class ManufacturerMvcController {
    private final ManufacturerService manufacturerService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;

    @Autowired
    public ManufacturerMvcController(ManufacturerService manufacturerService,
                                     ModelMapper modelMapper,
                                     AuthenticationHelper authenticationHelper) {
        this.manufacturerService = manufacturerService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping("/create")
    public String createModel(@ModelAttribute("makeDto") ManufacturerDto makeDto, BindingResult bindingResult,
                              Model model, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "redirect:/vehicles";
        }
        UserInfo personCalledMethod = authenticationHelper.getUserByEmail(session);
        Manufacturer newManufacturer = modelMapper.fromDtoToManufacturer(makeDto);
        try {
            manufacturerService.create(newManufacturer, personCalledMethod);
        } catch (DuplicateEntityException de) {
        }
        return "redirect:/vehicles";
    }

}
