package com.telerikacademy.mechanicum.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;


import java.util.Collections;

@Configuration
public class SwaggerConfig {
    @Bean
    public Docket swaggerConfiguration() {

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.telerikacademy.mechanicum"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiDetails());
    }

    private ApiInfo apiDetails() {
        return new ApiInfo("Mechanicum API",
                "API for auto repair shop to manage day-to-day job",
                "1.0",
                "Free to use",
                new Contact("Mechanicum", "not-realSite.bg", "not-realEmail@notrealEmail.com"),
                "API license",
                "not-realSite.bg",
                Collections.emptyList()
        );
    }
}
