package com.telerikacademy.mechanicum.config;

import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.ServiceStatus;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceRepository;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceStatusRepository;
import com.telerikacademy.mechanicum.repositories.contracts.VisitRepository;
import com.telerikacademy.mechanicum.utils.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Configuration
@Component
public class VisitAndServiceSchedulerConfig implements Runnable {
    private final VisitRepository visitRepository;
    private final ServiceRepository serviceRepository;
    private final ServiceStatusRepository serviceStatusRepository;
    private final EmailService emailService;

    @Autowired
    public VisitAndServiceSchedulerConfig(VisitRepository visitRepository,
                                          ServiceRepository serviceRepository,
                                          ServiceStatusRepository serviceStatusRepository,
                                          EmailService emailService) {
        this.visitRepository = visitRepository;
        this.serviceRepository = serviceRepository;
        this.serviceStatusRepository = serviceStatusRepository;
        this.emailService = emailService;
    }

    @Scheduled(cron = "0 0 20 * * ?")
    public void refreshDatabase() {
        List<Service> notFinishedServices = serviceRepository.getAllNotFinishedServices();
        ServiceStatus statusComplete = serviceStatusRepository.getById(3);
        ServiceStatus statusInProgress = serviceStatusRepository.getById(2);
        for (Service service : notFinishedServices) {
            if (LocalDate.now().isAfter(service.getEndDate()) || LocalDate.now().isEqual(service.getEndDate())) {
                service.setServiceStatus(statusComplete);
                serviceRepository.update(service);
                System.out.println("updating service finished");
            }
            if (serviceRepository.checkVisitCompletionAllServices(service.getVisit().getId())) {
                Visit visitToUpdate = visitRepository.getByID(service.getVisit().getId());
                visitToUpdate.setCompletionDate(LocalDate.now());
                visitRepository.update(visitToUpdate);
                //TODO Add email sending to owner
                //Tell customer that a car is ready to be pickedUp
                emailService.sendMail(
                        service.getVisit().getVehicle().getOwner().getUserAuthorization().getEmail(),
                        "Car is ready for pickup",
                        generateMessage(service));
                System.out.println("updating visit");
            }
            if ((service.getStartDate()).isEqual(LocalDate.now().plusDays(1))) {
                service.setServiceStatus(statusInProgress);
                serviceRepository.update(service);
                System.out.println("updating service with status 2");
            }
        }
    }

    @Override
    public void run() {
        refreshDatabase();
    }

    private String generateMessage(Service service) {
        return String.format("Hello %s,%n" +
                        "Your %s, model: %s is ready for pick up." +
                        "For more information about the auto-repair please check your profile at:" +
                        "http://www.mechanicum.com %n" +
                        "Best Regards %n" +
                        "Team Mechanicum%n",
                service.getVisit().getVehicle().getOwner().getFirstName(),
                service.getVisit().getVehicle().getModel().getManufacturer().getName(),
                service.getVisit().getVehicle().getModel().getModel());
    }
}
