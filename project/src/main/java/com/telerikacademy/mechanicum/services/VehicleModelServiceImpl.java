package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.VehicleModel;
import com.telerikacademy.mechanicum.repositories.contracts.VehicleModelRepository;
import com.telerikacademy.mechanicum.services.contracts.VehicleModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleModelServiceImpl implements VehicleModelService {

    public static final String MODIFY_MODEL_ERROR_MESSAGE = "Only employee can modify a model.";
    private final VehicleModelRepository modelRepository;

    @Autowired
    public VehicleModelServiceImpl(VehicleModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    @Override
    public List<VehicleModel> getAll(UserInfo personCalledMethod) {
        verifyCanModify(personCalledMethod);
        return modelRepository.getAll();
    }

    @Override
    public VehicleModel getById(int id, UserInfo personCalledMethod) {
        verifyCanModify(personCalledMethod);
        return modelRepository.getById(id);
    }

    @Override
    public VehicleModel create(VehicleModel model, UserInfo personCalledMethod) {
        verifyCanModify(personCalledMethod);
        boolean duplicateExists = true;
        try {
            modelRepository.getByName(model.getModel());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Model", "name", model.getModel());
        }
        modelRepository.create(model);
        return model;
    }

    private void verifyCanModify(UserInfo user) {
        var isEmployee = user.getUserAuthorization().isEmployee();
        if (!isEmployee) {
            throw new UnauthorizedOperationException(MODIFY_MODEL_ERROR_MESSAGE);
        }
    }
}
