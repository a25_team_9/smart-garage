package com.telerikacademy.mechanicum.services.contracts;

import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Visit;


import java.util.List;
import java.util.Map;


public interface UserInfoService {
    List<UserInfo> getAll(UserInfo personCalledMethod) ;

    List<UserInfo> getAllCustomers(UserInfo personCalledMethod);

    int getAllCustomersCount();

    UserInfo getByID(int customerID, UserInfo personCalledMethod);

    UserInfo create(UserInfo user,UserInfo personCalledMethod);

    UserInfo getByEmail(String email);

    UserInfo update(UserInfo customer, UserInfo userInfo);

    void delete(int userInfoID, UserInfo personCalledDelete);

    List<Visit> filter(Map<String, String> filterMap, UserInfo personCalledMethod);

    List<Visit> sortByNameOrVisitDate(Map<String, String> filterMap, UserInfo personCalledMethod);

    List<UserInfo> getByName(String name, UserInfo personCalledMethod);

    UserInfo getByResetToken(String token, UserInfo personCalledMethod);
}
