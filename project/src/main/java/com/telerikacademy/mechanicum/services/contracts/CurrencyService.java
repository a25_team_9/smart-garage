package com.telerikacademy.mechanicum.services.contracts;

import com.telerikacademy.mechanicum.models.Currency;
import com.telerikacademy.mechanicum.models.UserInfo;

import java.util.List;

public interface CurrencyService {
    List<Currency> getAll();

    Currency getById(int id);

    Currency create(Currency currency, UserInfo personCalledMethod);

    Currency update(Currency currency, UserInfo personCalledMethod);

    void delete(int currencyId, UserInfo personCalledMethod);
}
