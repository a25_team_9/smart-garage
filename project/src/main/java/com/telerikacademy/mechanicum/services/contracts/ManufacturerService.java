package com.telerikacademy.mechanicum.services.contracts;

import com.telerikacademy.mechanicum.models.Manufacturer;
import com.telerikacademy.mechanicum.models.UserInfo;

import java.util.List;

public interface ManufacturerService {

    List<Manufacturer> getAll(UserInfo personCalledMethod);

    Manufacturer getById(int makeId, UserInfo personCalledMethod);

    Manufacturer create(Manufacturer make, UserInfo personCalledMethod);
}
