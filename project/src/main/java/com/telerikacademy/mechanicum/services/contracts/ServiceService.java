package com.telerikacademy.mechanicum.services.contracts;

import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.UserInfo;

import java.util.List;

public interface ServiceService {
    List<Service> getAll(UserInfo currentUser);

    Service getById(int serviceId, UserInfo currentUser);

    Service create(Service service, UserInfo currentUser);

    Service update(Service service, UserInfo currentUser);

    void delete(int serviceId, UserInfo currentUser);
}
