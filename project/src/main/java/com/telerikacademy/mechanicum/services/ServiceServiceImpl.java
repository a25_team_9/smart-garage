package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceRepository;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceStatusRepository;
import com.telerikacademy.mechanicum.repositories.contracts.VisitRepository;
import com.telerikacademy.mechanicum.services.contracts.ServiceService;
import com.telerikacademy.mechanicum.utils.ExchangeRateService;
import com.telerikacademy.mechanicum.utils.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {

    public static final String MODIFY_SERVICE_ERROR_MESSAGE = "Only employee can modify service.";
    private final ServiceRepository serviceRepository;
    private final VisitRepository visitRepository;
    private final ServiceStatusRepository serviceStatusRepository;
    private final ExchangeRateService exchangeRateService;
    private final EmailService emailService;

    @Autowired
    public ServiceServiceImpl(ServiceRepository serviceRepository,
                              VisitRepository visitRepository,
                              ServiceStatusRepository serviceStatusService,
                              ExchangeRateService exchangeRateService,
                              EmailService emailService) {
        this.serviceRepository = serviceRepository;
        this.visitRepository = visitRepository;
        this.serviceStatusRepository = serviceStatusService;
        this.exchangeRateService = exchangeRateService;
        this.emailService = emailService;
    }

    @Override
    public List<Service> getAll(UserInfo currentUser) {
        verifyIsEmployee(currentUser);
        return serviceRepository.getAll();
    }

    @Override
    public Service getById(int serviceId, UserInfo currentUser) {
        verifyIsEmployee(currentUser);
        return serviceRepository.getById(serviceId);
    }

    @Override
    public Service create(Service service, UserInfo currentUser) {
        verifyIsEmployee(currentUser);

        if (service.getVisit().getCompletionDate() != null) {
            throw new UnauthorizedOperationException("Cannot create a service in a completed visit");
        }
        if(service.getVisit().getServiceList()
                .stream()
                .anyMatch(service1 ->
                        service1.getServiceType().equals(service.getServiceType())
                )
        )
        {
            throw new DuplicateEntityException(String.format("Visit already contains this service %s",service.getServiceType()));
        }
        serviceRepository.create(service);
        Visit visitToUpdate = getVisitToUpdate(service);
        visitRepository.update(visitToUpdate);
        return service;
    }


    @Override
    public Service update(Service service, UserInfo currentUser) {
        verifyIsEmployee(currentUser);
        if (LocalDate.now().isAfter(service.getEndDate())) {
            service.setServiceStatus(serviceStatusRepository.getById(3));
        }
        serviceRepository.update(service);
        boolean checkVisitFinish = serviceRepository.checkVisitCompletionAllServices(service.getVisit().getId());
        Visit visitToUpdate = getVisitToUpdate(service);
        if (checkVisitFinish) {
            visitToUpdate.setCompletionDate(LocalDate.now());
            emailService.sendMail(
                    service.getVisit().getVehicle().getOwner().getUserAuthorization().getEmail(),
                    "Car is ready for pickup",
                    generateMessage(service));
        }
        visitRepository.update(visitToUpdate);
        return service;
    }

    @Override
    public void delete(int serviceId, UserInfo currentUser) {
        verifyIsEmployee(currentUser);
        Service serviceToDelete = serviceRepository.getById(serviceId);
        Visit visitToUpdate = visitRepository.getByID(serviceToDelete.getVisit().getId());
        double convertRate = exchangeRateService.getConverter("BGN",visitToUpdate.getCurrency().getName()).getConvertionRate();
        double totalPrice = visitToUpdate.getTotalPrice() != null ? visitToUpdate.getTotalPrice() : 0.0;
        totalPrice-=serviceToDelete.getQuantity()*serviceToDelete.getServiceType().getSinglePrice()*convertRate;
        visitToUpdate.setTotalPrice(Double.parseDouble(
                        String.format(Locale.US, "%.2f", totalPrice)));
        visitRepository.update(visitToUpdate);
        serviceRepository.delete(serviceToDelete);
    }

    private void verifyIsEmployee(UserInfo currentUser) {
        boolean isEmployee = currentUser.getUserAuthorization().isEmployee();
        if (!isEmployee) {
            throw new UnauthorizedOperationException(MODIFY_SERVICE_ERROR_MESSAGE);
        }
    }

    private void calculateTotalPrice(Visit visit, Service service) {
        double convertRate = exchangeRateService.getConverter("BGN",visit.getCurrency().getName()).getConvertionRate();
        double totalPrice = visit.getTotalPrice() != null ? visit.getTotalPrice() : 0.0;
        totalPrice+=service.getQuantity()*service.getServiceType().getSinglePrice()*convertRate;
        visit.setTotalPrice( Double.parseDouble(
                String.format(Locale.US, "%.2f", totalPrice)));
    }

    private Visit getVisitToUpdate(Service service) {
        Visit visitToUpdate = visitRepository.getByID(service.getVisit().getId());
        calculateTotalPrice(visitToUpdate,service);
        return visitToUpdate;
    }

    private String generateMessage(Service service) {
        return String.format("Hello %s,%n" +
                        "Your %s, model: %s is ready for pick up." +
                        "For more information about the auto-repair please check your profile at:" +
                        "http://ec2-3-16-25-97.us-east-2.compute.amazonaws.com:8080  %n" +
                        "Best Regards %n" +
                        "Team Mechanicum%n",
                service.getVisit().getVehicle().getOwner().getFirstName(),
                service.getVisit().getVehicle().getModel().getManufacturer().getName(),
                service.getVisit().getVehicle().getModel().getModel());
    }

}
