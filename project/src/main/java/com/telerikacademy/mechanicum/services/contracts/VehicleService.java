package com.telerikacademy.mechanicum.services.contracts;

import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Vehicle;

import java.util.List;
import java.util.Map;

public interface VehicleService {

    List<Vehicle> getAll(UserInfo currentUser);

    Vehicle getById(int id, UserInfo currentUser);

    Vehicle create(Vehicle vehicle, UserInfo currentUser);

    Vehicle update(Vehicle vehicle, UserInfo currentUser);

    void delete(int id, UserInfo currentUser);

    List<Vehicle> filter(Map<String, String> parameters, UserInfo currentUser);

    List<Service> getServicesByCarId(int carId, UserInfo currentUser);

    int getAllVehiclesCount();
}
