package com.telerikacademy.mechanicum.services.contracts;

import com.telerikacademy.mechanicum.models.ServiceStatus;
import com.telerikacademy.mechanicum.models.UserInfo;

public interface ServiceStatusService {

    ServiceStatus getById(int statusId, UserInfo currentUser);

}
