package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.ServiceStatus;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceStatusRepository;
import com.telerikacademy.mechanicum.services.contracts.ServiceStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceStatusServiceImpl implements ServiceStatusService {

    private static final String RESOURCES_NEEDS_AUTHORIZATION_MESSAGE = "Resources needs authorization";
    private final ServiceStatusRepository statusRepository;

    @Autowired
    public ServiceStatusServiceImpl(ServiceStatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public ServiceStatus getById(int statusId, UserInfo currentUser) {
        isEmployee(currentUser);
        return statusRepository.getById(statusId);
    }

    private void isEmployee(UserInfo currentUser) {
        var isEmployee = currentUser.getUserAuthorization().isEmployee();
        if (!isEmployee) {
            throw new UnauthorizedOperationException(RESOURCES_NEEDS_AUTHORIZATION_MESSAGE);
        }
    }
}
