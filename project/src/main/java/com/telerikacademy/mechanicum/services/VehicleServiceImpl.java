package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Vehicle;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.repositories.contracts.VehicleRepository;
import com.telerikacademy.mechanicum.repositories.contracts.VisitRepository;
import com.telerikacademy.mechanicum.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@org.springframework.stereotype.Service
public class VehicleServiceImpl implements VehicleService {

    public static final String MODIFY_VEHICLES_ERROR_MESSAGE = "Only employee can modify a vehicles.";
    public static final String MODIFY_VEHICLE_ERROR_MESSAGE = "Only employee or owner can modify a personal vehicle.";
    private final VehicleRepository vehicleRepository;
    private final VisitRepository visitRepository;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository,
                              VisitRepository visitRepository) {
        this.vehicleRepository = vehicleRepository;
        this.visitRepository = visitRepository;
    }

    @Override
    public List<Vehicle> getAll(UserInfo currentUser) {
        verifyIsEmployee(currentUser);
        return vehicleRepository.getAll();
    }

    @Override
    public Vehicle getById(int id, UserInfo currentUser) {
        verifyIsEmployeeOrOwner(currentUser, id);
        return vehicleRepository.getById(id);
    }

    @Override
    public Vehicle create(Vehicle vehicle, UserInfo currentUser) {
        verifyIsEmployee(currentUser);
        boolean duplicateExists = true;
        try {
            vehicleRepository.getByVin(vehicle.getVin());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Vehicle", "vin", vehicle.getVin());
        }

        duplicateExists = true;
        try {
            vehicleRepository.getByLicencePlate(vehicle.getLicencePlate());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Vehicle", "licence plate", vehicle.getLicencePlate());
        }

        vehicleRepository.create(vehicle);
        return vehicle;
    }

    @Override
    public Vehicle update(Vehicle vehicle, UserInfo currentUser) {
        verifyIsEmployeeOrOwner(currentUser, vehicle.getId());
        return vehicleRepository.update(vehicle);
    }

    @Override
    public void delete(int id, UserInfo currentUser) {
        verifyIsEmployee(currentUser);
        Vehicle vehicleToDelete = vehicleRepository.getById(id);
        try {
            visitRepository.getCustomerLinkedServices(vehicleToDelete.getOwner().getUserID(),
                    Optional.of(vehicleToDelete.getId()),
                    Optional.empty());
        } catch (EntityNotFoundException enf) {
            vehicleRepository.delete(vehicleToDelete);
            return;
        }
        throw new InvalidFieldException("You can`t delete vehicles that have Visits");
    }

    @Override
    public List<Vehicle> filter(Map<String, String> parameters, UserInfo currentUser) {
        verifyIsEmployee(currentUser);
        return vehicleRepository.filter(parameters);
    }

    @Override
    public List<Service> getServicesByCarId(int carId, UserInfo currentUser) {
        verifyIsEmployeeOrOwner(currentUser, carId);
        return vehicleRepository.getServicesByCarId(carId);
    }


    @Override
    public int getAllVehiclesCount() {
        return vehicleRepository.getAllVehiclesCount();
    }

    private void verifyIsEmployee(UserInfo currentUser) {
        boolean isEmployee = currentUser.getUserAuthorization().isEmployee();
        if (!isEmployee) {
            throw new UnauthorizedOperationException(MODIFY_VEHICLES_ERROR_MESSAGE);
        }
    }

    private void verifyIsEmployeeOrOwner(UserInfo currentUser, int vehicleId) {
        var isEmployee = currentUser.getUserAuthorization().isEmployee();
        var isOwner = vehicleRepository.getById(vehicleId).getOwner().getUserID().equals(currentUser.getUserID());
        if (!isOwner && !isEmployee) {
            throw new UnauthorizedOperationException(MODIFY_VEHICLE_ERROR_MESSAGE);
        }
    }
}
