package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Vehicle;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceRepository;
import com.telerikacademy.mechanicum.repositories.contracts.VisitRepository;
import com.telerikacademy.mechanicum.services.contracts.VehicleService;
import com.telerikacademy.mechanicum.utils.contracts.EmailService;
import com.telerikacademy.mechanicum.repositories.contracts.UserInfoRepository;
import com.telerikacademy.mechanicum.services.contracts.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class UserInfoServiceImpl implements UserInfoService {
    private static final String RESOURCES_NEEDS_AUTHORIZATION_MESSAGE = "Resources needs authorization";

    private final UserInfoRepository userInfoRepository;
    private final ServiceRepository serviceRepository;
    private final VehicleService vehicleService;

    private final VisitRepository visitRepository;
    private final EmailService emailService;

    @Autowired
    public UserInfoServiceImpl(UserInfoRepository userInfoRepository,
                               ServiceRepository serviceRepository,
                               VehicleService vehicleService,
                               VisitRepository visitRepository,
                               EmailService emailService) {
        this.userInfoRepository = userInfoRepository;
        this.serviceRepository = serviceRepository;
        this.vehicleService = vehicleService;
        this.visitRepository = visitRepository;
        this.emailService = emailService;
    }

    @Override
    public List<UserInfo> getAll(UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        return userInfoRepository.getAll();
    }

    @Override
    public List<UserInfo> getAllCustomers(UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        return userInfoRepository.getAllCustomers();
    }

    @Override
    public int getAllCustomersCount() {
        return userInfoRepository.getAllCustomersCount();
    }

    @Override
    public UserInfo getByID(int customerID, UserInfo personCalledMethod) {
        UserInfo userInfo = userInfoRepository.getByID(customerID);
        checkIfAuthorized(userInfo,personCalledMethod);
        return userInfo;
    }

    @Override
    public UserInfo create(UserInfo user, UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        checkEmailExist(user);
        userInfoRepository.create(user);
        emailService.sendMail(user.getUserAuthorization().getEmail(), "Account creation", getCreationText(user));
        return user;
    }

    @Override
    public UserInfo getByEmail(String email) {
        return userInfoRepository.getByEmail(email);
    }

    @Override
    public UserInfo update(UserInfo customer, UserInfo personCalledMethod) {
        checkIfAuthorized(customer, personCalledMethod);
        return userInfoRepository.update(customer);
    }

    @Override
    public void delete(int userInfoID, UserInfo personCalledDelete) {
        checkIfAdmin(personCalledDelete);
        UserInfo userToDelete = userInfoRepository.getByID(userInfoID);
        if(!visitRepository.getCustomerVisits(userInfoID,1).isEmpty()){
            throw new InvalidFieldException("Can`t delete customer that has/had visits to our shop.");
        }
        for (Vehicle v: userToDelete.getVehicleList()) {
            vehicleService.delete(v.getId(),personCalledDelete);
        }
        userInfoRepository.delete(userToDelete);
    }

    @Override
    public List<Visit> filter(Map<String, String> filterMap, UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        return userInfoRepository.filter(filterMap);
    }

    @Override
    public List<Visit> sortByNameOrVisitDate(Map<String, String> parameters, UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        return userInfoRepository.sort(parameters);
    }

    @Override
    public List<UserInfo> getByName(String name, UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        return userInfoRepository.getByName(name);
    }

    @Override
    public UserInfo getByResetToken(String token, UserInfo personCalledMethod) {
        UserInfo userInfo = userInfoRepository.getByToken(token);
        return userInfo;
    }


    private void checkEmailExist(UserInfo user) {
        boolean duplicateEmail = true;
        try {
            userInfoRepository.getByEmail(user.getUserAuthorization().getEmail());
        } catch (EntityNotFoundException e) {
            duplicateEmail = false;
        }

        if (duplicateEmail) {
            throw new DuplicateEntityException(String.format("Customer with this email %s already exist",
                    user.getUserAuthorization().getEmail()));
        }
    }

    private void checkIfAdmin(UserInfo personCalledMethod) {
        if (!personCalledMethod.getUserAuthorization().isEmployee()) {
            throw new UnauthorizedOperationException(RESOURCES_NEEDS_AUTHORIZATION_MESSAGE);
        }
    }

    private void checkIfAuthorized(UserInfo userInfo, UserInfo personCalledMethod) {
        if (!personCalledMethod.getUserAuthorization().isEmployee() &&
                !(personCalledMethod.getUserID().equals(userInfo.getUserID()))) {
            throw new UnauthorizedOperationException(RESOURCES_NEEDS_AUTHORIZATION_MESSAGE);
        }
    }

    private String getCreationText(UserInfo user) {
        return String.format("Hello %s,%n%n" +
                        "We are sending you this email to let you know that a registration to our application was successfully created.%n" +
                        "To feel the emotion please log in in our web portal:%n" +
                        " http://ec2-3-16-25-97.us-east-2.compute.amazonaws.com:8080 %n" +
                        "Your login information is right below.%n" +
                        "Email: %s%n" +
                        "Password: %s%n" +
                        "%n%n" +
                        "Best Regards %n" +
                        "Team Mechanicum%n",
                user.getFirstName(),
                user.getUserAuthorization().getEmail(),
                user.getUserAuthorization().getPassword());
    }


}
