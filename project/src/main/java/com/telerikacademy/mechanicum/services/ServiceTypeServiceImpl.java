package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.ServiceType;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceRepository;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceTypeRepository;
import com.telerikacademy.mechanicum.services.contracts.ServiceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ServiceTypeServiceImpl implements ServiceTypeService {
    private static final String RESOURCES_NEEDS_AUTHORIZATION_MESSAGE = "Resources needs authorization";

    private final ServiceTypeRepository serviceTypeRepository;
    private final ServiceRepository serviceRepository;

    @Autowired
    public ServiceTypeServiceImpl(ServiceTypeRepository serviceTypeRepository,
                                  ServiceRepository serviceRepository) {
        this.serviceTypeRepository = serviceTypeRepository;
        this.serviceRepository = serviceRepository;
    }

    @Override
    public List<ServiceType> getAll() {
        return serviceTypeRepository.getAll();
    }

    @Override
    public ServiceType getById(int id) {
        return serviceTypeRepository.getById(id);
    }

    @Override
    public ServiceType getByName(String name) {
        return serviceTypeRepository.getByName(name);
    }

    @Override
    public ServiceType create(ServiceType serviceType, UserInfo userInfo) {
        checkIfAdmin(userInfo);
        boolean duplicateExists = true;
        try {
            serviceTypeRepository.getByName(serviceType.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Service type", "name", serviceType.getName());
        }
        serviceTypeRepository.create(serviceType);
        return serviceType;
    }

    @Override
    public ServiceType update(ServiceType serviceType, UserInfo userInfo) {
        checkIfAdmin(userInfo);
        serviceTypeRepository.update(serviceType);
        return serviceType;
    }

    @Override
    public List<ServiceType> filter(Map<String, String> filterMap) {
        return serviceTypeRepository.filter(filterMap);
    }

    @Override
    public void delete(int serviceTypeId, UserInfo personCalledDelete) {
        checkIfAdmin(personCalledDelete);
        ServiceType serviceTypeToDelete = serviceTypeRepository.getById(serviceTypeId);
        boolean safeToDelete = serviceRepository.getAll().stream()
                .noneMatch(s -> s.getServiceType().getName().equals(serviceTypeToDelete.getName()));
        if (!safeToDelete) {
            throw new UnauthorizedOperationException("Cannot delete type if there are services already created with that type");
        }
        serviceTypeRepository.delete(serviceTypeToDelete);
    }

    private void checkIfAdmin(UserInfo personCalledMethod) {
        if (!personCalledMethod.getUserAuthorization().isEmployee()) {
            throw new UnauthorizedOperationException(RESOURCES_NEEDS_AUTHORIZATION_MESSAGE);
        }
    }
}
