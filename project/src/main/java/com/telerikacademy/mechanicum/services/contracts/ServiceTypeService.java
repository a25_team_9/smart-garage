package com.telerikacademy.mechanicum.services.contracts;

import com.telerikacademy.mechanicum.models.ServiceType;
import com.telerikacademy.mechanicum.models.UserInfo;

import java.util.List;
import java.util.Map;

public interface ServiceTypeService {
    List<ServiceType> getAll();

    ServiceType getById(int id);

    ServiceType getByName(String name);

    ServiceType create(ServiceType serviceType, UserInfo userInfo);

    ServiceType update(ServiceType serviceType, UserInfo userInfo);

    List<ServiceType> filter(Map<String, String> filterMap);

    void delete(int serviceTypeId, UserInfo personCalledDelete);
}
