package com.telerikacademy.mechanicum.services.contracts;

import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.VehicleModel;

import java.util.List;

public interface VehicleModelService {

    List<VehicleModel> getAll(UserInfo personCalledMethod);

    VehicleModel getById(int id, UserInfo personCalledMethod);

    VehicleModel create(VehicleModel model, UserInfo personCalledMethod);

}
