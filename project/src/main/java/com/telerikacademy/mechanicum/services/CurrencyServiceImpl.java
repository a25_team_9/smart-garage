package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Currency;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.repositories.contracts.CurrencyRepository;
import com.telerikacademy.mechanicum.services.contracts.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyServiceImpl implements CurrencyService {
    private static final String RESOURCES_NEEDS_AUTHORIZATION_MESSAGE = "Resources needs authorization";

    private final CurrencyRepository currencyRepository;

    @Autowired
    public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public List<Currency> getAll() {
        return currencyRepository.getAll();
    }

    @Override
    public Currency getById(int id) {
        return currencyRepository.getById(id);
    }

    @Override
    public Currency create(Currency currency, UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        boolean duplicateExists = true;
        try {
            currencyRepository.getByName(currency.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Currency", "name", currency.getName());
        }
        return currencyRepository.create(currency);
    }

    @Override
    public Currency update(Currency currency, UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        return currencyRepository.update(currency);
    }

    @Override
    public void delete(int currencyId, UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        Currency currencyToDelete = currencyRepository.getById(currencyId);
        currencyRepository.delete(currencyToDelete);
    }

    private void checkIfAdmin(UserInfo personCalledMethod) {
        if (!personCalledMethod.getUserAuthorization().isEmployee()) {
            throw new UnauthorizedOperationException(RESOURCES_NEEDS_AUTHORIZATION_MESSAGE);
        }
    }
}
