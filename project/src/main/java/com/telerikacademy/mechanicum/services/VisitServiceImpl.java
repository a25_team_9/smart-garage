package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.InvalidFieldException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Service;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Visit;
import com.telerikacademy.mechanicum.repositories.contracts.CurrencyRepository;
import com.telerikacademy.mechanicum.repositories.contracts.ServiceRepository;
import com.telerikacademy.mechanicum.repositories.contracts.UserInfoRepository;
import com.telerikacademy.mechanicum.repositories.contracts.VisitRepository;
import com.telerikacademy.mechanicum.services.contracts.VisitService;
import com.telerikacademy.mechanicum.utils.ExchangeRateService;
import com.telerikacademy.mechanicum.utils.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Component
public class VisitServiceImpl implements VisitService {
    private static final String RESOURCES_NEEDS_AUTHORIZATION_MESSAGE = "Resources needs authorization";

    private final VisitRepository visitRepository;
    private final CurrencyRepository currencyRepository;
    private final ServiceRepository serviceRepository;
    private final UserInfoRepository userInfoRepository;
    private final ExchangeRateService exchangeRateService;
    private final EmailService emailService;

    @Autowired
    public VisitServiceImpl(VisitRepository visitRepository,
                            CurrencyRepository currencyRepository,
                            ServiceRepository serviceRepository,
                            UserInfoRepository userInfoRepository,
                            ExchangeRateService exchangeRateService,
                            EmailService emailService) {
        this.visitRepository = visitRepository;
        this.currencyRepository = currencyRepository;
        this.serviceRepository = serviceRepository;
        this.userInfoRepository = userInfoRepository;
        this.exchangeRateService = exchangeRateService;
        this.emailService = emailService;
    }

    @Override
    public List<Visit> getAll(UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        return visitRepository.getAll();
    }

    @Override
    public Visit getById(int visitId, UserInfo personCalledMethod) {
        UserInfo userToFilterBy = userInfoRepository.getByID(personCalledMethod.getUserID());
        checkIfAuthorized(userToFilterBy, personCalledMethod);
        return visitRepository.getByID(visitId);
    }

    @Override
    public Visit create(Visit visit, UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        return visitRepository.create(visit);
    }

    @Override
    public Visit update(Visit visit, UserInfo personCalledMethod) {
        Visit visitOld = visitRepository.getByID(visit.getId());
        checkIfAuthorized(visitOld.getVehicle().getOwner(),personCalledMethod);
        boolean checkVisitFinish = serviceRepository.checkVisitCompletionAllServices(visit.getId());

        if (!visitOld.getCurrency().equals(visit.getCurrency())) {
            double convertRate = exchangeRateService.getConverter
                    (visitOld.getCurrency().getName(), visit.getCurrency().getName()).getConvertionRate();
            visit.setTotalPrice(
                    Double.parseDouble(
                            String.format(Locale.US, "%.2f", visit.getTotalPrice() * convertRate)));
        }

        if (checkVisitFinish) {
            visit.setCompletionDate(LocalDate.now());
            if(personCalledMethod.getUserAuthorization().isEmployee()) {
                emailService.sendMail(
                        visit.getVehicle().getOwner().getUserAuthorization().getEmail(),
                        "Car is ready for pickup",
                        generateMessage(visit));
            }
        }

        return visitRepository.update(visit);
    }

    @Override
    public void delete(int visitId, UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        Visit visitToDelete = visitRepository.getByID(visitId);
        if (!visitToDelete.getServiceList().isEmpty()) {
            throw new InvalidFieldException("You can`t delete Visit with services in it.");
        }
        visitRepository.delete(visitToDelete);
    }

    @Override
    public List<Visit> getCustomerVisits(int customerId,
                                         Optional<Integer> numberOfVisits,
                                         Optional<String> currency,
                                         UserInfo personCalledMethod) {
        UserInfo userToFilterBy = userInfoRepository.getByID(customerId);
        checkIfAuthorized(userToFilterBy, personCalledMethod);
        int numberOfResults = numberOfVisits.orElse(1);
        List<Visit> visits = visitRepository.getCustomerVisits(customerId, numberOfResults);
        if (currency.isPresent()) {
            for (Visit v : visits) {
                if (!v.getCurrency().getName().equals(currency.get())) {
                    double convertRate = exchangeRateService.getConverter(v.getCurrency().getName(), currency.get()).getConvertionRate();
                    v.setCurrency(currencyRepository.getByName(currency.get()));
                    double convertedPrice = v.getTotalPrice() * convertRate;
                    v.setTotalPrice(Double.parseDouble(String.format(Locale.US, "%.2f", convertedPrice)));
//                    for(Service s : v.getServiceList() ){
//                        double convertedPriceService = s.getServiceType().getSinglePrice()*convertRate;
//                        s.getServiceType().setSinglePrice(Double.parseDouble(String.format(Locale.US, "%.2f", convertedPriceService)));
//                    }
                }
            }
        }
        return visits;
    }

    @Override
    public List<Visit> getLinkedServices(int customerID, UserInfo personCalledMethod, Optional<Integer> vehicleId, Optional<String> date) {
        UserInfo userToFilterBy = userInfoRepository.getByID(customerID);
        checkIfAuthorized(userToFilterBy, personCalledMethod);
        return visitRepository.getCustomerLinkedServices(customerID, vehicleId, date);
    }

    @Override
    public void getPayment(int visitId, UserInfo personCalledMethod) {
        checkIfAdmin(personCalledMethod);
        Visit visit = visitRepository.getByID(visitId);
        if (visit.getCompletionDate() == null) {
            throw new UnauthorizedOperationException("Cannot pay for visit that has uncompleted services");
        }
        visit.setPaid(true);
        visitRepository.update(visit);
    }

    private void checkIfAdmin(UserInfo personCalledMethod) {
        if (!personCalledMethod.getUserAuthorization().isEmployee()) {
            throw new UnauthorizedOperationException(RESOURCES_NEEDS_AUTHORIZATION_MESSAGE);
        }
    }

    private void checkIfAuthorized(UserInfo userInfo, UserInfo personCalledMethod) {
        if (!personCalledMethod.getUserAuthorization().isEmployee() &&
                !(personCalledMethod.getUserID().equals(userInfo.getUserID()))) {
            throw new UnauthorizedOperationException(RESOURCES_NEEDS_AUTHORIZATION_MESSAGE);
        }
    }

    private String generateMessage(Visit visit) {
        return String.format("Hello %s,%n" +
                        "Your %s, model: %s is ready for pick up." +
                        "For more information about the auto-repair please check your profile at:" +
                        "http://ec2-3-16-25-97.us-east-2.compute.amazonaws.com:8080 %n" +
                        "Best Regards %n" +
                        "Team Mechanicum%n",
                visit.getVehicle().getOwner().getFirstName(),
                visit.getVehicle().getModel().getManufacturer().getName(),
                visit.getVehicle().getModel().getModel());
    }
}
