package com.telerikacademy.mechanicum.services.contracts;

import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.models.Visit;

import java.util.List;
import java.util.Optional;

public interface VisitService {
    List<Visit> getAll(UserInfo personCalledMethod);

    Visit getById(int visitId, UserInfo personCalledMethod);

    Visit create(Visit visit, UserInfo personCalledMethod);

    Visit update(Visit visit, UserInfo personCalledMethod);

    void delete(int visitId, UserInfo personCalledMethod);

    List<Visit> getCustomerVisits(int customerId,
                                  Optional<Integer> numberOfVisits,
                                  Optional<String> currency,
                                  UserInfo personCalledMethod);

    void getPayment(int visitId, UserInfo personCalledMethod);

    List<Visit> getLinkedServices(int customerID, UserInfo personCalledMethod, Optional<Integer> vehicleId, Optional<String> date);

}
