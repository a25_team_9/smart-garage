package com.telerikacademy.mechanicum.services;

import com.telerikacademy.mechanicum.exceptions.DuplicateEntityException;
import com.telerikacademy.mechanicum.exceptions.EntityNotFoundException;
import com.telerikacademy.mechanicum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.mechanicum.models.Manufacturer;
import com.telerikacademy.mechanicum.models.UserInfo;
import com.telerikacademy.mechanicum.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.mechanicum.services.contracts.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {

    public static final String MODIFY_MAKES_ERROR_MESSAGE = "Only employee can modify a manufacturer.";
    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerServiceImpl(ManufacturerRepository repository) {
        this.manufacturerRepository = repository;
    }

    @Override
    public List<Manufacturer> getAll(UserInfo personCalledMethod) {
        verifyCanModify(personCalledMethod);
        return manufacturerRepository.getAll();
    }

    @Override
    public Manufacturer getById(int makeId, UserInfo personCalledMethod) {
        verifyCanModify(personCalledMethod);
        return manufacturerRepository.getById(makeId);
    }

    @Override
    public Manufacturer create(Manufacturer make, UserInfo personCalledMethod) {
        verifyCanModify(personCalledMethod);
        boolean duplicateExists = true;
        try {
            manufacturerRepository.getByName(make.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Manufacturer", "name", make.getName());
        }
        manufacturerRepository.create(make);
        return make;
    }

    private void verifyCanModify(UserInfo user) {
        var isEmployee = user.getUserAuthorization().isEmployee();
        if (!isEmployee) {
            throw new UnauthorizedOperationException(MODIFY_MAKES_ERROR_MESSAGE);
        }
    }
}
