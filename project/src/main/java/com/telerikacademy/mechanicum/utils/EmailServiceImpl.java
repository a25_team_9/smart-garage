package com.telerikacademy.mechanicum.utils;

import com.telerikacademy.mechanicum.utils.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

@Service
public class EmailServiceImpl implements EmailService {
    private static final String ourEmail = "mechanicum.project@gmail.com";
    private static final String report = "report";
    private JavaMailSender mailSender;

    @Autowired
    public EmailServiceImpl(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void sendMail(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(ourEmail);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        mailSender.send(message);
    }

    @Override
    public void sendReportEmail(String to, String subject, String text, ByteArrayDataSource byteArrayDataSource) {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setTo(to);
            helper.setText(text);
            helper.setSubject(subject);
            helper.addAttachment(report, byteArrayDataSource);
        } catch (MessagingException e) {
            throw new IllegalArgumentException("Failed to send email");
        }
        mailSender.send(message);
    }

}
