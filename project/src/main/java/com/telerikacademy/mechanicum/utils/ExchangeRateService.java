package com.telerikacademy.mechanicum.utils;

import com.telerikacademy.mechanicum.models.converter.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class ExchangeRateService {

    private static final String key = "7f8ee8MPrdGnyxtKuK51ecxGaPtb0zufoP4i6jPeb0h3CDd48ct25CLv";
    private static final String customHeader = "x-happi-key";
    private RestTemplate restTemplate;

    @Autowired
    public ExchangeRateService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Converter getConverter(String from, String to) {
        String uri = String.format("https://api.happi.dev/v1/exchange/%s/%s", from, to);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set(customHeader, key);
        HttpEntity<Converter> entity = new HttpEntity<Converter>(headers);

        ResponseEntity<Converter> response = restTemplate.exchange(uri, HttpMethod.GET, entity, Converter.class);
        Converter converter = response.getBody();

        return converter;

    }
}
//https://api.happi.dev/v1/exchange/:from/:to
