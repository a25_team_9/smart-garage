package com.telerikacademy.mechanicum.utils.contracts;

import javax.mail.util.ByteArrayDataSource;

public interface EmailService {

    void sendMail(String to, String subject, String text);

    void sendReportEmail(String to, String subject, String text, ByteArrayDataSource byteArrayDataSource);
}
