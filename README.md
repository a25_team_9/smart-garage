# Smart Garage
Georgi Georgiev Ivan Hristov Ivan Perchemliev group project

# MECHANICUM

A web application that enables the employees of an auto repair shop work more efficiently. At its core it is a Business Operations Programme that manages most of the day-to-day job of the customer support staff. The systems has 2 types of users - Users and Employees. Every single service, visit and vehicle is recordered in a complex system and updated daily.

### 1 The Database

The data of the application is stored on a **MariaDB** database server. It can be recreated by running the files in the database folder inside the project:
 - **create_db_new.sql** - creating tables in the database
 - **create_user.sql** - ensures access for the database
 - **fill_db_new.sql** - fills the database with _sample_ data

Connection between the java source code and the database was done by using **Hibernate ORM** framework.
Data in the tables is not duplicated and the database itself is normalised. 

------------

### 2 Structure of application
#### Backend
We have tried to follow all principle that we know for high quality code (**OOP, KISS, SOLID** ect.).
**SpringMVC** and **SpringBoot** frameworks were used in creating the three layers:
##### Repository layer
Connection between the java code and the database is established by using **Hibernate**.
##### Service layer
The business logic of the app - service creation, visit updates, price calculations ect.
##### Controller layer
Connection with the client - branched out to **Rest controllers** and **MVC controllers**. The **Rest controllers** are connected with **Swagger** in order to improve testing quality.

#### Frontend
The entire frontend was written on **HTML** and **CSS** with **Bootstrap** framework while the Html pages connection was done via **MVC** controllers along with **Thymeleaf** template.
Small parts were complimented with **JavaScript** to make the pages more attractive and user-friendly.
 ![link to picture](/media%20files/210416%20frontpage%20not%20logged%201.jpg)<br />
 ![link to picture](/media%20files/210416%20frontpage%20not%20logged%202.jpg)<br />
 ![link to picture](/media%20files/210416%20frontpage%20not%20logged%203.jpg)<br />
 ![link to picture](/media%20files/210416%20frontpage%20not%20logged%204.jpg)<br />

#### Tests
Tests are one of the most important parts of any app, as they help greatly to reduce the time for manual testing and keep track of implemented functionalities while developing new ones.
The service layer is covered with tests for 100% with the help of **JUnit** and **Mockito**.
 ![link to picture](/media%20files/210416%20tests.jpg)<br />

##### Possible Issues / Bugs / Ideas for the future

Possible Issues<br />
 - When a customer is created without a visit or vehicle there will be inconsistency between the search by name & filter in the Customers tab.

Bugs<br />
 - No known bugs noticed as of 16.04.2021.

Ideas for the future<br />
 - Adding calendar for services - a schedule for mechanics day-to-day planning.
 - Send notifications about year maintenance services that are coming up.
 - Additional security - hashing usernames and passwords in database, using reCAPTCHA for logging.
 - Add prices with VAT and without in the single service type.
 - Change prices depending on what currency is chosen from the navbar.


#### Development information

- Link to project - <a href='ec2-3-16-25-97.us-east-2.compute.amazonaws.com:8080'>Link</a>

- Tools used: IntelliJ IDEA, JDK11 & MariaDB, Google Chrome & Mozilla Firefox.

- Link to the Trello board - <a href ='https://trello.com/b/BJZoDYA5/smart-garage'>Click here</a>

- Link to the Swagger documentation (_Please run the application, before opening the link_) - <a href ='http://localhost:8080/swagger-ui/'>Click here</a> or <a href ='http://localhost:8080/swagger-ui/index.html'>Click here</a>

- Link to pictures of database structure -  ![link to picture](/media%20files/210416%20database%20structure.jpg)<br />

- Gmail account:
 - User: mechanicum.project@gmail.com
 - Password: alpha25mechanicuM

### 3 The Fuctionality

The functionalities are split, depending on the roles: **USER** or **EMPLOYEE**.

#### Anonymous 

Unregistered users can see the home page and all available service types and their price in BGN. Only **Employees** can register users. Both customers and employees can login.
 ![link to picture](/media%20files/210416%20login.jpg)<br />

#### User

User can see the following pages: <br />
 - _Home_: Title page presenting all the information about the company
 - _Services offered_: All available service types and their price in BGN
 - _Vehicles_: List of all the user's vehicles that have been serviced in MECHANICUM
 - _Profile_: Personal information about the user, their vehicles and a history of their visits sorted by date, get a report for their visits and more
 - _Logout_: Log out of application

#### Employee

Employees have most functionalities and see the following pages:<br />
 - _Home_: Title page presenting all the information about the company
 - _Customers_: List of all customers with fucntionality to filter it
 - _Register_: Create a new customer and/or employee
 - _Vehicles_: List of all vehicles regardless of owner
 - _Services offered_: All available service types and their price in BGN with the functionality to create/update and filter
 - _Services performed_: List of all visits regardless of customer id with fucntionality to filter it and create a new visit
 - _Profile_: Personal information about the user, their vehicles and a history of their visits sorted by date, get a report for their visits and more

#### Daily updates - Amazing feature

The application runs a daily update in order to keep track of which visits are complete and that their price is up to day. Each update occurs at 20:00 and can be modified in the Backend.

#### Creating a new user

Employees can register a new user from the _Register User_ menu in the navbar. 
 ![link to picture](/media%20files/210417%20register%201.jpg)<br />
They will need the following mandatory elements, marked with "*":<br />
 - Valid email
 - First name
 - Last name
 - Phone number in the format 0xxxxxxxxx
 ![link to picture](/media%20files/210417%20register%202.jpg)<br />
After clicking create an entry in the database is created (in 'user_info', 'users_authorization' & 'users_roles'). The created user can be a customer or an employee. Employees can also have their vehicles serviced in the shop.

#### Report generation
Both customers and employees can get reports for one or multiple visits with the desired currency. Employees have the additional functionality of sending it/them reports to the customer's email.
  ![link to picture](/media%20files/210416%20profile.jpg)<br />
  ![link to picture](/media%20files/210417%20report%201.jpg)<br />

#### Deleting an user

Employees can delete an user only if the user has not visits attatched to him. A user cannot delete their own profile.

#### Creating a vehicle

Employees can create a vehicle for a customer. If the model or make does not exist they can be added to the database. The following items are needed:<br />
 - Manufacturer
 - Model
 - VIN - unique to the database
 - Licence plate - unique to the database
 - Owner - registered user

#### Creating a visit 
  ![link to picture](/media%20files/210416%20Visits.jpg)<br />
Employees can create a new visit for one of the registered user's vehicles.
There is a choice between a user's vehicles, currency for the visit and state of payment (true or false). The status of the visits depends on the start and end date.

#### Creating a service in a visit 

Employees can create or delete a new service inside a visit. There are choices from the service types in the database.
  ![link to picture](/media%20files/210417%20new%20visit%201.jpg)<br />
  ![link to picture](/media%20files/210417%20new%20visit%202.jpg)<br />
  ![link to picture](/media%20files/210417%20new%20visit%203.jpg)<br />
Upon the completion of this function the total price of the visit is recalculated.

#### Service type
  ![link to picture](/media%20files/210417%20service%20type.jpg)<br />
Employees update an already existing service type or create a new service type only if there is not one with same name already in the database. Both regular users and employees can see the full list of services offered by the shop.

#### Deleting a visit 

Employees can delete a visit only if there are no services in it.

#### Deleting a service

Employees can delete services inside the menu of a visit. Upon the completion of this function the total price of the visit is recalculated.

#### Filter / sort / search functionalities
  ![link to picture](/media%20files/210417%20filter%20menu.jpg)<br />
Employees can filter / sort / search in the pages with lists - ie Customers, Vehicles in order to find the needed result.









